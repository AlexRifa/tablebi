
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/27/2017 14:32:56
-- Generated from EDMX file: c:\projects\Other\TableBI\TableBI.Data\TableBIModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [TableBI];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Account]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Account];
GO
IF OBJECT_ID(N'[dbo].[Bank]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Bank];
GO
IF OBJECT_ID(N'[dbo].[Company]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Company];
GO
IF OBJECT_ID(N'[dbo].[PPBD]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PPBD];
GO
IF OBJECT_ID(N'[dbo].[sysdiagrams]', 'U') IS NOT NULL
    DROP TABLE [dbo].[sysdiagrams];
GO
IF OBJECT_ID(N'[Хранилище TableBIModelContainer].[Company2]', 'U') IS NOT NULL
    DROP TABLE [Хранилище TableBIModelContainer].[Company2];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Account'
CREATE TABLE [dbo].[Account] (
    [AccountId] int IDENTITY(1,1) NOT NULL,
    [Account1] varchar(250)  NULL,
    [BankId] int  NULL,
    [CompanyId] int  NULL
);
GO

-- Creating table 'Bank'
CREATE TABLE [dbo].[Bank] (
    [BankId] int IDENTITY(1,1) NOT NULL,
    [BankName] varchar(250)  NULL,
    [BankAbr] varbinary(50)  NULL
);
GO

-- Creating table 'Company'
CREATE TABLE [dbo].[Company] (
    [CompanyId] int IDENTITY(1,1) NOT NULL,
    [CompanyName] varchar(250)  NULL,
    [CompanyAbr] varchar(250)  NULL,
    [CompanyOur] int  NULL,
    [CompanyGroupId] int  NULL
);
GO

-- Creating table 'PPBD'
CREATE TABLE [dbo].[PPBD] (
    [PPId] int  NOT NULL,
    [Номер] int  NOT NULL,
    [Дата] datetime  NOT NULL,
    [Сумма] decimal(18,0)  NOT NULL,
    [ПлательщикСчет] int  NOT NULL,
    [ПлательщикБанк1] int  NULL,
    [ПолучательСчет] int  NOT NULL,
    [ПолучательБанк1] int  NULL,
    [ПолучательРасчСчет] varchar(250)  NULL,
    [ДатаПоступило] datetime  NULL,
    [НазначениеПлатежа] varchar(1000)  NULL
);
GO

-- Creating table 'sysdiagrams'
CREATE TABLE [dbo].[sysdiagrams] (
    [name] nvarchar(128)  NOT NULL,
    [principal_id] int  NOT NULL,
    [diagram_id] int IDENTITY(1,1) NOT NULL,
    [version] int  NULL,
    [definition] varbinary(max)  NULL
);
GO

-- Creating table 'Company2'
CREATE TABLE [dbo].[Company2] (
    [CompanyId] int  NOT NULL,
    [CompanyName] varchar(250)  NOT NULL,
    [CompanyAbr] varchar(250)  NULL,
    [CompanyOur] int  NULL,
    [CompanyGroupId] int  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [AccountId] in table 'Account'
ALTER TABLE [dbo].[Account]
ADD CONSTRAINT [PK_Account]
    PRIMARY KEY CLUSTERED ([AccountId] ASC);
GO

-- Creating primary key on [BankId] in table 'Bank'
ALTER TABLE [dbo].[Bank]
ADD CONSTRAINT [PK_Bank]
    PRIMARY KEY CLUSTERED ([BankId] ASC);
GO

-- Creating primary key on [CompanyId] in table 'Company'
ALTER TABLE [dbo].[Company]
ADD CONSTRAINT [PK_Company]
    PRIMARY KEY CLUSTERED ([CompanyId] ASC);
GO

-- Creating primary key on [PPId] in table 'PPBD'
ALTER TABLE [dbo].[PPBD]
ADD CONSTRAINT [PK_PPBD]
    PRIMARY KEY CLUSTERED ([PPId] ASC);
GO

-- Creating primary key on [diagram_id] in table 'sysdiagrams'
ALTER TABLE [dbo].[sysdiagrams]
ADD CONSTRAINT [PK_sysdiagrams]
    PRIMARY KEY CLUSTERED ([diagram_id] ASC);
GO

-- Creating primary key on [CompanyId], [CompanyName] in table 'Company2'
ALTER TABLE [dbo].[Company2]
ADD CONSTRAINT [PK_Company2]
    PRIMARY KEY CLUSTERED ([CompanyId], [CompanyName] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------