﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableBI.Data;
namespace TableBI.BL
{
    public interface IImport
    {
        Doc GetNextPP();
        PP ParsePP(List<string> pp);
        int StartImport(string file);
        int InsertPP(PP pp);

        Company GetCompany(string name);
    }
}
