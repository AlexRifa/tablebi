﻿// Developer Express Code Central Example:
// How to preserve the XtraGrid view state in multi level master/detail
// 
// This example is an extension of example for a grid in master-detail mode. It
// stores view states of all existing nested grid views.
// 
// You can find sample updates and versions for different programming languages here:
// http://www.devexpress.com/example=E1466

using System;
using System.Collections;
using DevExpress.XtraGrid;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace DevExpress.XtraGrid.Helpers
{

    public struct InfoRow
    {
        public int handle;
        public int level;
    };

    class HelperClass
    {
        private GridView view;
        private string keyFieldName;
        int focusedRowHandle;
        private ArrayList saveExpList = new ArrayList();
        private ArrayList saveSelList = new ArrayList();

        public HelperClass(GridView view, string keyFieldName)
        {
            this.view = view;
            this.keyFieldName = keyFieldName;
        }

        void SaveExpandInfo()
        {
            InfoRow row;
            saveExpList.Clear();
            for (int i = -1; i > int.MinValue; i--)
            {
                if (!view.IsValidRowHandle(i)) break;
                if (view.GetRowExpanded(i))
                {
                    row.handle = i;
                    row.level = view.GetRowLevel(i);
                    saveExpList.Add(row);
                }
            }
        }

        void SaveSelectInfo(object val)
        {
            InfoRow row;
            saveSelList.Clear();
            int rowByValueHandle = view.LocateByValue(0, view.Columns[keyFieldName], val, null); ;
            int[] selectionArray = view.GetSelectedRows();
            if (selectionArray != null)
                for (int i = 0; i < selectionArray.Length; i++)
                {
                    int dataRowHandle = selectionArray[i];
                    if (dataRowHandle == rowByValueHandle)
                        dataRowHandle = view.GetParentRowHandle(rowByValueHandle);
                    row.handle = dataRowHandle;
                    row.level = view.GetRowLevel(i);
                    saveSelList.Add(row);
                }
        }

        void SaveFocusInfo(object val)
        {
            int rowByValueHandle = view.LocateByValue(0, view.Columns[keyFieldName], val, null);
            if (view.FocusedRowHandle == rowByValueHandle)
            {
                focusedRowHandle = view.GetParentRowHandle(view.FocusedRowHandle);
            }
            else focusedRowHandle = view.FocusedRowHandle;

        }

        void LoadFocusInfo()
        {
            if (view.IsValidRowHandle(focusedRowHandle))
                view.FocusedRowHandle = focusedRowHandle;
        }

        private void LoadSelectInfo()
        {
            view.BeginSelection();
            view.ClearSelection();
            for (int i = 0; i < saveSelList.Count; i++)
                view.SelectRow(((InfoRow)saveSelList[i]).handle);
            view.EndSelection();
        }

        void LoadExpandInfo()
        {
            view.BeginUpdate();
            view.CollapseAllGroups();
            for (int i = 0; i < saveExpList.Count; i++)
                view.ExpandGroupRow(((InfoRow)saveExpList[i]).handle, false);
            view.EndUpdate();
        }

        public void SaveViewInfo(object val)
        {
            SaveExpandInfo();
            SaveSelectInfo(val);
            SaveFocusInfo(val);
        }

        public void LoadViewInfo()
        {
            LoadExpandInfo();
            LoadSelectInfo();
            LoadFocusInfo();
        }
    }


    public class GridControlState
    {

        public struct RowInfo
        {
            public object Id;
            public int level;
        }

        public struct ViewDescriptor
        {
            public string relationName;
            public string keyFieldName;

            public ViewDescriptor(string relationName, string keyFieldName)
            {
                this.relationName = relationName;
                this.keyFieldName = keyFieldName;
            }
        }

        public class ViewState
        {
            private GridControlState gridState;
            private ViewState parent;
            private ViewDescriptor descriptor;
            private ArrayList saveExpList;

            protected ViewState(GridControlState gridState, ViewDescriptor descriptor)
            {
                this.gridState = gridState;
                this.descriptor = descriptor;
                this.parent = null;
            }

            protected ViewState(ViewState parent, ViewDescriptor descriptor)
            {
                this.parent = parent;
                this.gridState = parent.gridState;
                this.descriptor = descriptor;
            }

            public static ViewState Create(GridControlState gridState, GridView view)
            {
                if (!gridState.viewDescriptors.ContainsKey(view.LevelName)) return null;
                ViewState state = new ViewState(gridState, (ViewDescriptor)gridState.viewDescriptors[view.LevelName]);
                return state;
            }

            private static ViewState Create(ViewState parent, GridView view)
            {
                if (!parent.gridState.viewDescriptors.ContainsKey(view.LevelName)) return null;
                ViewState state = new ViewState(parent, (ViewDescriptor)parent.gridState.viewDescriptors[view.LevelName]);
                return state;
            }

            public bool IsLevel(string level)
            {
                return level == this.descriptor.relationName;
            }

            public ArrayList SaveExpList
            {
                get
                {
                    if (saveExpList == null)
                        saveExpList = new ArrayList();
                    return saveExpList;
                }
            }

            protected int FindParentRowHandle(GridView view, RowInfo rowInfo, int rowHandle)
            {
                int result = view.GetParentRowHandle(rowHandle);
                while (view.GetRowLevel(result) != rowInfo.level)
                    result = view.GetParentRowHandle(result);
                return result;
            }

            protected void ExpandRowByRowInfo(GridView view, RowInfo rowInfo)
            {
                int dataRowHandle = view.LocateByValue(0, view.GroupedColumns[rowInfo.level], rowInfo.Id);
                if (dataRowHandle != GridControl.InvalidRowHandle)
                {
                    int parentRowHandle = FindParentRowHandle(view, rowInfo, dataRowHandle);
                    view.SetRowExpanded(parentRowHandle, true, false);
                }
            }

            public void SaveExpansionViewInfo(GridView view)
            {
                if (view.GroupedColumns.Count == 0) return;
                SaveExpList.Clear();
                for (int i = -1; i > int.MinValue; i--)
                {
                    if (!view.IsValidRowHandle(i)) break;
                    if (view.GetRowExpanded(i))
                    {
                        RowInfo rowInfo;
                        int dataRowHandle = view.GetDataRowHandleByGroupRowHandle(i);
                        rowInfo.Id = view.GetGroupRowValue(i);
                        rowInfo.level = view.GetRowLevel(i);
                        SaveExpList.Add(rowInfo);
                    }
                }
            }

            public void LoadExpansionViewInfo(GridView view)
            {
                if (view.GroupedColumns.Count == 0) return;
                view.BeginUpdate();
                try
                {
                    view.CollapseAllGroups();
                    foreach (RowInfo info in SaveExpList)
                        ExpandRowByRowInfo(view, info);
                }
                finally
                {
                    view.EndUpdate();
                }
            }

            public void SaveState(GridView view)
            {
                if (ReferenceEquals(view.GridControl.FocusedView, view)) gridState.focused = this;
                SaveExpansionViewInfo(view);
            }

            public void LoadState(GridView view)
            {
                LoadExpansionViewInfo(view);
                if (ReferenceEquals(gridState.focused, this)) view.GridControl.FocusedView = view;
            }
        }

        private Hashtable viewDescriptors;
        private ViewState root;
        private ViewState focused;

        public GridControlState(ViewDescriptor[] views)
        {
            viewDescriptors = new Hashtable();
            foreach (ViewDescriptor desc in views)
            {
                viewDescriptors.Add(desc.relationName, desc);
            }
        }

        public void SaveViewInfo(GridView view)
        {
            root = ViewState.Create(this, view);
            root.SaveState(view);
        }

        public void LoadViewInfo(GridView view)
        {
            if (root == null || !root.IsLevel(view.LevelName)) return;
            root.LoadState(view);
        }

    }
}