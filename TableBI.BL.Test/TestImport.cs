﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using  TableBI.BL;
using TableBI.Data;
namespace TableBI.BL.Test
{
    [TestFixture]
    public class ImportTest
    {
        public IImport Import { get; set; }
        [SetUp]
        public void Init()
        {
            Import = new Import();
        }

        List<string> GeneratePPList()
        {
            const string ppString = @"Номер = 404
            Дата = 16.01.2017
            Сумма = 301961.00
            ПлательщикСчет = 40702810200040000128
            ПлательщикИНН = 5903046904
            ПлательщикКПП =
                Плательщик1 = Открытое Акционерное Общество ""Нью Граунд""
            ПлательщикРасчСчет = 40702810200040000128
            ПлательщикБанк1 = ФБ ФИЛИАЛ АКБ ""ФОРА-БАНК""(АО) В Г.ПЕРМИ
                ПлательщикБанк2 = Г ПЕРМЬ
                ПлательщикБИК = 045773713
            ПлательщикКорсчет = 30101810900000000713
            ПолучательСчет = 40702810901280002343
            ДатаПоступило = 16.01.2017
            ПолучательИНН = 5906143705
            ПолучательКПП =
                Получатель1 = ООО ""Амадео""
            ПолучательРасчСчет = 40702810901280002343
            ПолучательБанк1 = ФБ ФИЛИАЛ ПАО ""БАНК УРАЛСИБ"" В Г.УФА
                ПолучательБанк2 = Г УФА
                ПолучательБИК = 048073770
            ПолучательКорсчет = 30101810600000000770
            ВидПлатежа = электронно
            ВидОплаты = 01
            СтатусСоставителя =
                ПоказательКБК =
                    ОКАТО =
                        ПоказательОснования =
                            ПоказательПериода =
                                ПоказательНомера =
                                    ПоказательДаты =
                                        ПоказательТипа =
                                            СрокПлатежа =
                                                Очередность = 5
            НазначениеПлатежа = оплата по сч.№Ам17010903 от 09.01.17 за стройматериалы.Сумма 301961 - 00В т.ч.НДС(18 %) 46061 - 85
            Код =";

            List<string> lines = new List<string>();

            ppString.Split('\n').ToList().ForEach((line) =>
                {
                    if(!String.IsNullOrEmpty(line))
                        lines.Add(line.Trim());
                }
            
            );

            return lines;
        }
        [Test]
        public void ImportTest_GeneratePPList()
        {
            PP pp =Import.ParsePP(GeneratePPList());
            NUnit.Framework.Assert.That(pp, Is.Not.Null);
        }

        [Test]
        public void ImportTest_Номер()
        {

            PP pp = Import.ParsePP(this.GeneratePPList());

            NUnit.Framework.Assert.That(pp.Номер, Is.Not.Zero);
        }

        [Test]
        public void ImportTest_Дата()
        {

            PP pp = Import.ParsePP(this.GeneratePPList());

            NUnit.Framework.Assert.That(pp.Дата, Is.InRange(new DateTime(2017,1,1 ), new DateTime(2027, 1, 1)));
        }

        [Test]
        public void ImportTest_Сумма()
        {

            PP pp = Import.ParsePP(this.GeneratePPList());

            NUnit.Framework.Assert.That(pp.Сумма, Is.Not.Zero);
        }

        [Test]
        public void ImportTest_ПлатильщикСчет()
        {

            PP pp = Import.ParsePP(GeneratePPList());

            NUnit.Framework.Assert.That(pp.ПлательщикСчет, Is.Not.Empty);
        }

        [Test]
        public void ImportTest_Плательщик1()
        {

            PP pp = Import.ParsePP(GeneratePPList());

            NUnit.Framework.Assert.That(pp.Плательщик1, Is.Not.Empty);
        }

        [Test]
        public void ImportTest_ПлательщикБанк1()
        {

            PP pp = Import.ParsePP(GeneratePPList());

            NUnit.Framework.Assert.That(pp.ПлательщикБанк1, Is.Not.Empty);
        }
        [Test]
        public void ImportTest_Получатель1()
        {

            PP pp = Import.ParsePP(GeneratePPList());

            NUnit.Framework.Assert.That(pp.Получатель1, Is.Not.Empty);
        }

        [Test]
        public void ImportTest_ДатаПоступило()
        {

            PP pp = Import.ParsePP(GeneratePPList());

            NUnit.Framework.Assert.That(pp.ДатаПоступило, Is.InRange(new DateTime(2017, 1, 1), new DateTime(2027, 1, 1)));
        }

        [Test]
        public void ImportTest_НазначениеПлатежа()
        {

            PP pp = Import.ParsePP(GeneratePPList());

            NUnit.Framework.Assert.That(pp.НазначениеПлатежа, Is.Not.Empty);
        }
        [Test]
        public void ImportTest_ПолучательБанк1()
        {

            PP pp = Import.ParsePP(GeneratePPList());

            NUnit.Framework.Assert.That(pp.ПолучательБанк1, Is.Not.Empty);
        }
        [Test]
        public void ImportTest_ПолучательРасчСчет()
        {

            PP pp = Import.ParsePP(GeneratePPList());

            NUnit.Framework.Assert.That(pp.ПолучательРасчСчет, Is.Not.Empty);
        }
        
        [Test]
        public void ImportTest_StartImport()
        {

            int rez = Import.StartImport(@"c:\projects\Other\ПРОГА\01.01.2017 - 04.04.2017\Аврора\01.txt");

            NUnit.Framework.Assert.That(rez, Is.GreaterThan(-1));
        }

        [Test]
        public void ImportTest_GetCompany()
        {
            Company company = Import.GetCompany("Открытое Акционерное Общество \"Нью Граунд\"");

            NUnit.Framework.Assert.That(company.CompanyName, Is.Not.Empty);
        }

        [Test]
        public void ImportTest_GetSinonim()
        {
            //Company company = Import.GetCompany("ООО Аврора; Бэтмобиль");
            CompanyService companyService = new CompanyService();
            var companies = companyService.GetSinonim("ООО Аврора; Бэтмобиль");

            NUnit.Framework.Assert.That(companies.Count(), Is.GreaterThan(0));
        }

    }
}
