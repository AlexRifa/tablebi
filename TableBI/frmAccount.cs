﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TableBI.BL;
using System.Data.Entity;

namespace TableBI
{
    public partial class frmAccount : Form
    {
        TableBI.Data.TableBIEntities2 dbContext;
        public frmAccount(TableBI.Data.TableBIEntities2 dbContext, int CompanyId)
        {
            InitializeComponent();
            this.dbContext = dbContext;
            
            this.dbContext.Account.Load();            
            
            bindingSource1.DataSource = this.dbContext.Account.Where(x => x.CompanyId == CompanyId).ToList();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                Log.AddToLog("frmAccount btnOk_Click " + ex.Message);
                MessageBox.Show(ex.Message);
            }

            Close();
            
        }

        private void gridViewAccount_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            dbContext.SaveChanges();
        }
    }
}
