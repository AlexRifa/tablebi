﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Data.SqlClient;

namespace TableBI.Data
{
    public class PPView: View_PP, IPPBD
    {
        
        public static List<View_PP> GetByPeriod(TableBIEntities2 dbContext, DateTime begin, DateTime end)
        {
            var beginParameter = new SqlParameter("@begin", begin);
            var endParameter = new SqlParameter("@end", end);

            var ostatki = dbContext.Database
                .SqlQuery<View_PP>("spComing @begin, @end", beginParameter, endParameter)
                .ToList();
            return ostatki;
        }

        public static double GetSum(TableBIEntities2 dbContext, DateTime begin)
        {            
            var param = new SqlParameter("@begin", begin);

            double sum = dbContext.Database
                .SqlQuery<Double>("select sum(Сумма) from View_PP where ДатаВыполнено < @begin", param)
                .FirstOrDefault();
                
            return sum;
        }

        public  static List<PPView>  GetPPViewList(List<View_PP> list, double beginSum, TableBIEntities2 dbContext)
        {
            List<PPView> newlist = new List<PPView>();

            list = list.OrderBy(t => t.Дата).ToList();

            double? sum = beginSum;
            int i = 0;
            foreach (View_PP pp in list)
            {
                sum += pp.Сумма;

                PPView ppView = new PPView();

                foreach (PropertyInfo property in typeof(View_PP).GetProperties())
                {
                    property.SetValue(ppView, property.GetValue(pp, null), null);
                }
                ppView.ОбщийПриход = sum;
                ppView.Index = i++;
                

                //если тип универсал, процент меняется на тот который у приемника
                TypeCompany type = dbContext.TypeCompany.Where(x => x.TypeCompanyId == pp.TypeCompanyId).FirstOrDefault();
                if (type != null)
                {
                    if (type.TypeCompanyName.ToLower() == "универсал")
                    {
                        //TypeCompany typeCompany2 = dbContext.TypeCompany.Where(x=>x.TypeCompanyId == pp.TypeCompanyId2).FirstOrDefault();
                        Percent percent = (Percent)dbContext.Percent.Where(x => 
                            x.TypeCompanyId == pp.TypeCompanyId2 
                            && x.OwnerId == pp.OwnerId
                            && x.PrecentDate <= ppView.Дата
                            && x.PercentDateEnd >= ppView.Дата).FirstOrDefault();
                        if (percent != null)
                        {
                            ppView.PercentId = percent.PercentId;
                            ppView.PercentIdReal = percent.PercentId;
                            ppView.PercentDayInc = percent.PercentDayInc;
                            ppView.PercentNdsDayInc = percent.PercentNdsDayInc;
                            ppView.PercentNdsValue = percent.PercentNdsValue;
                            ppView.PercentValue = percent.PercentValue;
                        }
                    }
                }

                Company company = dbContext.Company.Where(x => x.CompanyId == pp.ПолучательКомпания).FirstOrDefault();
                if (company != null)
                {
                    if (!String.IsNullOrEmpty(pp.Получатель) && company.CompanyFizLizo == true && (company.CompanyName.ToUpper() != pp.Получатель.ToUpper()))
                    {
                        ppView.ПолучательКомпанияИмя = pp.Получатель;
                    }
                    else
                    {
                        ppView.ПолучательКомпанияИмя = pp.ПолучательКомпанияИмя;
                    }
                }
                else
                {
                    ppView.ПолучательКомпанияИмя = pp.ПолучательКомпанияИмя;
                }

                newlist.Add(ppView);

            }

            SetDebt(newlist);

            return newlist;

        }

        public static List<PPView> GetPPViewListDirect(List<View_PP> list, double beginSum, TableBIEntities2 dbContext)
        {
            List<PPView> rez = new List<PPView>();

            rez = GetPPViewList(list, beginSum, dbContext);
            rez = rez.Where(x => x.Type == "r").ToList();
            return rez;
        }

        public static List<PPView> GetPPViewListPrihod(List<View_PP> list, double beginSum, TableBIEntities2 dbContext)
        {
            List<PPView> rez = new List<PPView>();

            rez = GetPPViewList(list, beginSum, dbContext);
            rez = rez.Where(x => x.Type != "r").ToList();
            return rez;
        }        


        public new String ПолучательКомпанияИмя { get; set; }

        public static void SetDebt(List<PPView>  list)
        {
            var listOwner = list.Where(t=>t.OwnerId!=null).Select(t => t.OwnerId).Distinct().ToList();
            foreach(int ownerId in listOwner)
            {
                double? sum = list.Where(t => t.OwnerId == ownerId).Sum(t => t.Долг);
                list.Where(t => t.OwnerId == ownerId).ToList().ForEach(t => t.ТекущийДолг = Convert.ToDouble(sum));
            }
        }

        public Double ТекущийДолг { get; set; }

        public new Double? ОбщийПриход { get; set; }

        public Double Index { get; set; }

        public Double? ПланСумма { get
            {
                if (this.Процент == null) return null;
                return this.Сумма - this.Сумма / 100 * this.Процент;

            }
        }

        public Double? Долг {
            get
            {
                if (this.Процент == null) return null;
                return this.ПланСумма - Convert.ToDouble(this.ActionSum);
            }
        }

        public Double? Процент
        {
            get
            {
                if (String.IsNullOrEmpty(this.НДС))
                {
                    return this.PercentValue;
                }
                if (this.НДС.ToUpper() != "НДС")
                {
                    return this.PercentValue;
                }
                else
                {
                    return this.PercentNdsValue;
                }
            }
        }

        public  DateTime? ПланДата2
        {
            get
            {
                if (this.PercentIdReal == null)
                    return null;
                if (this.Процент == null)
                {
                    return null;
                }
                int addDays = 0;
                if (String.IsNullOrEmpty(this.НДС))
                {
                    addDays = Convert.ToInt32(this.PercentDayInc);
                }
                else
                {
                    if (this.НДС.ToUpper() != "НДС")
                    {
                        addDays = Convert.ToInt32(this.PercentDayInc);
                    }
                    else
                    { 
                        addDays = Convert.ToInt32(this.PercentNdsDayInc);
                    }
                }

                
                return AddDays(Дата, addDays);
            }
        }


        DateTime AddDays(DateTime? date, int days)
        {
            DateTime temp = Convert.ToDateTime(date);            
            for (int i = 1; i <= days;)
            {
                temp = temp.AddDays(1);
                if (temp.DayOfWeek == DayOfWeek.Sunday || temp.DayOfWeek == DayOfWeek.Saturday || IsOutput(temp))
                {
                    continue;
                }
                i++;
            }

            return temp;
        }

        public Double? ПланВыдача
        {
            get
            {
                return this.Сумма - this.Сумма / 100 * Процент;
            }
        }

        public void SelectRow(TableBI.Data.TableBIEntities2 dbContext, bool selected)
        {
            PPBD pp = dbContext.PPBD.Where(x => x.PPId == this.PPId).First();
            this.Selected = selected;
            pp.Selected = selected;
            dbContext.SaveChanges();
        }

        /// <summary>
        /// Проверка на выходной день(праздник)
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
       public static bool IsOutput(DateTime date)
        {
            if (date.DayOfYear >= 1  && date.DayOfYear <= 5)
            {
                return true;
            }

            if (date.DayOfYear == 7 )
            {
                return true;
            }

            if (date.Day == 23 && date.Month == 2)
            {
                return true;
            }

            if (date.Day == 8 && date.Month == 3)
            {
                return true;
            }

            if (date.Day == 1 && date.Month == 5)
            {
                return true;
            }
            if (date.Day == 9 && date.Month == 5)
            {
                return true;
            }

            if (date.Day == 12 && date.Month == 6)
            {
                return true;
            }

            if (date.Day == 4 && date.Month == 11)
            {
                return true;
            }

            return false;
        }
    }


}
