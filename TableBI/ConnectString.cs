﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using TableBI.Data;
using TableBI.BL;
using System.Configuration;
using System.Data.SqlClient;

namespace TableBI
{
    public partial class ConnectString : DevExpress.XtraEditors.XtraForm
    {
        public ConnectString()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            Config.Server = tbServer.Text;
            Config.User = tbUser.Text;
            Config.Password = tbPassword.Text;
            Config.Path = tbPath.Text;
            Config.Save();
            this.DialogResult = DialogResult.OK;
            Close();          
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ConnectString_Load(object sender, EventArgs e)
        {
            tbServer.Text = Config.Server;
            tbUser.Text = Config.User;
            tbPassword.Text = Config.Password;
            tbPath.Text = Config.Path;
        }

        private bool TestConn(string cs)
        {
            try
            {
                var conn = new SqlConnection(cs);
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}