alter table dbo.Company add
	BalanceDays varchar(250) null;
go;

update Company
set BalanceDays = OstatokPeriodId - 1
where OstatokPeriodId is not null;

update Company
set BalanceDays = '1, 2, 3, 4, 5'
where BalanceDays = '0';

delete from OstatokPeriod 
where OstatokPeriodDay = 0;

update OstatokPeriod 
set OstatokPeriodName = '�������'
where OstatokPeriodDay = 5;