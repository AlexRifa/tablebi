﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using TableBI.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace TableBI.BL
{
    public class Doc
    {
        public string type;
        public List<string> lines;
    }

    public class Import:IImport
    {
        List<string> linesFomFile = new List<string>();
        private int posInFile = 0;
        private static TableBIEntities2 context;
        string path;
        string currentAccount = "";

        static Import()
        {
            context = new TableBIEntities2();
        }

        public Doc GetNextPP()
        {
            bool isend = false;
            bool find = false;
            Doc doc =  new Doc();
            List<string> rez = new List<string>();
            doc.lines = rez;

            for (; posInFile < linesFomFile.Count(); posInFile++)
            {
                string line = linesFomFile[posInFile];

                string[] split = line.Split('=');

                if (split.Length!=2)
                    continue;
                split[0] = split[0].Trim();
                split[1] = split[1].Trim();
                //if ((split[0].ToLower() == "СекцияДокумент".ToLower()) &&
                //    ((split[1].ToLower() == "Платежное поручение".ToLower())
                //    || (split[1].ToLower() == "Банковский ордер".ToLower())
                //    || (split[1].ToLower() == "мемориальный ордер".ToLower())
                //    || (split[1].ToLower() == "Прочее".ToLower())
                //    || (split[1].ToLower() == "инкассовое поручение".ToLower())
                //    ))
                if ((split[0].ToLower() == "СекцияДокумент".ToLower()) )
                {
                    switch (split[1].ToLower())
                    {
                        case "платежное поручение":
                            doc.type = "PP"; break;
                        case "банковский ордер":
                            doc.type = "BO"; break;
                        case "мемориальный ордер":                            
                            doc.type = "MO"; break;
                        case "прочее":
                            doc.type = "PR"; break;
                        case "инкассовое поручение":
                            doc.type = "IP"; break;
                        default: doc.type = split[1].ToLower(); break;
                    }

                    find = true;
                    posInFile++;
                    break;
                }

            }

            if (!find)
            {
                return doc;
            }

            
            for (; posInFile < linesFomFile.Count(); posInFile++)
            {
                string line = linesFomFile[posInFile];

                string[] split = line.Split('=');

                if (split.Length == 1)
                {
                    if ((line.ToLower() == "КонецДокумента".ToLower()))
                    {
                        isend = true;
                        posInFile++;
                        break;
                    }
                }
                
                rez.Add(line);
                
            }

            if (!isend)
            {
                return new Doc();
            }

            doc.lines = rez;
            return doc;
            
        }

        public List<Ostatki> GetOstatokList()
        {
            
            List<string> list = new List<string>();

            List<Ostatki> ostatkiList = new List<Ostatki>();

            for (int i=0; i < linesFomFile.Count;)
            {
                if (linesFomFile[i].ToUpper().IndexOf("СекцияРасчСчет".ToUpper()) > -1)
                {
                    List<string> ostatokLines = new List<string>();
                    i++;
                    while (linesFomFile[i].ToUpper().IndexOf("КонецРасчСчет".ToUpper()) == -1 && i < linesFomFile.Count && (linesFomFile[i].ToUpper().IndexOf("СекцияДокумент".ToUpper()) == -1))
                    {
                        ostatokLines.Add(linesFomFile[i]);
                        i++;
                    }
                    Ostatki ostatok = ParseOstatok(ostatokLines);
                    Account account = context.Account.Where(x => x.Account1 == ostatok.РасчСчет).FirstOrDefault();
                    if (account == null)
                        continue;
                    ostatok.AccountId = account.AccountId;
                    ostatkiList.Add(ostatok);
                }

                if (linesFomFile[i].ToUpper().IndexOf("СекцияДокумент".ToUpper()) > -1)
                    break;
                i++;        
            }

            return ostatkiList;
        }

        private double _ConvertToDouble(string str)
        {
            if (String.IsNullOrEmpty(str))
                return 0;

            try
            {

                return Convert.ToDouble(str, CultureInfo.InvariantCulture);
            }
            catch (Exception ex)
            {
                return 0;
            }
            
        }
        public Ostatki ParseOstatok( List<string> list)
        {
            Ostatki ostatok = new Ostatki();
            list.ForEach(n =>
            {
                if (String.IsNullOrEmpty(n))
                {

                    Log.AddToLog(path + list[0]);
                    return;
                }
                string[] line = n.Split('=');

                if (line.Length < 2) return;

                line[1] = line[1].Trim();
                switch (line[0].Trim())
                {
                    
                    case "РасчСчет":                 
                        ostatok.РасчСчет = line[1];
                    break;

                    case "ДатаСоздания":
                        if (String.IsNullOrEmpty(line[1]))
                        {
                            ostatok.ДатаСоздания = new DateTime(1990, 1, 1);
                            break;
                        }
                        ostatok.ДатаСоздания = Convert.ToDateTime(line[1]);
                        break;
                    case "ДатаНачала":
                        if (String.IsNullOrEmpty(line[1]))
                        {
                            ostatok.ДатаНачала = new DateTime(1990, 1, 1);
                            break;
                        }
                        ostatok.ДатаНачала = Convert.ToDateTime(line[1]);
                        break;
                    case "ДатаКонца":
                        if (String.IsNullOrEmpty(line[1]))
                        {
                            ostatok.ДатаКонца = new DateTime(1990, 1, 1);
                            break;
                        }
                        ostatok.ДатаКонца = Convert.ToDateTime(line[1]);
                        break;

                    case "НачальныйОстаток":                        
                            ostatok.НачальныйОстаток = _ConvertToDouble(line[1]);                        
                        break;
                    case "ВсегоПоступило":                        
                            ostatok.ВсегоПоступило = _ConvertToDouble(line[1]);                        
                        break;
                    case "ВсегоСписано":
                        
                            ostatok.ВсегоСписано = _ConvertToDouble(line[1]);
                        
                        break;
                    case "КонечныйОстаток":                        
                            ostatok.КонечныйОстаток = _ConvertToDouble(line[1]);
                        break;
                        
                        
                }

            });

            return ostatok;
        }
        public PP ParsePP(List<string> pp)
        {
            PP outpp = new PP();
            pp.ForEach(n =>
            {
                if (String.IsNullOrEmpty(n)) {

                    Log.AddToLog(path+pp[0]);
                    return;
                }
                string[] line = n.Split('=');

                if (line.Length < 2) return;

                line[1] = line[1].Trim();
                switch (line[0].Trim())
                {
                    case "Номер":
                        outpp.Номер = Convert.ToInt32(line[1]);
                        break;
                    case "Дата":
                        outpp.Дата = Convert.ToDateTime(line[1]);
                        break;
                    case "Сумма":                       
                            outpp.Сумма = _ConvertToDouble(line[1]);                        
                        break;
                    case "ПлательщикСчет":
                        outpp.ПлательщикСчет = line[1];
                        break;
                    case "Плательщик1":
                        outpp.Плательщик1 = line[1];
                        break;
                    case "Плательщик":
                        outpp.Плательщик1 = line[1];
                        break;
                    case "ПлательщикБанк1":
                        outpp.ПлательщикБанк1 = line[1];
                        break;
                    case "ПлательщикБИК":
                        outpp.ПлательщикБИК = line[1];
                        break;
                    case "ПолучательБИК":
                        outpp.ПолучательБИК = line[1];
                        break;
                    case "Получатель1":
                        outpp.Получатель1 = line[1];
                        break;
                    case "Получатель":
                        outpp.Получатель1 = line[1];
                        break;
                    case "ПолучательРасчСчет":
                        outpp.ПолучательРасчСчет = line[1];
                        break;
                    case "ПолучательСчет":
                        outpp.ПолучательСчет = line[1];
                        break;
                    case "ДатаПоступило":
                        if (String.IsNullOrEmpty(line[1]))
                        {
                            outpp.ДатаПоступило = new DateTime(1990, 1, 1);
                            break;
                        }
                        outpp.ДатаПоступило= Convert.ToDateTime(line[1]);
                        break;
                    case "ДатаСписано":
                        if (String.IsNullOrEmpty(line[1]))
                        {
                            outpp.ДатаСписано = new DateTime(1990,1,1);
                            break;
                        }
                        outpp.ДатаСписано = Convert.ToDateTime(line[1]);
                        break;
                    case "НазначениеПлатежа":
                        outpp.НазначениеПлатежа = line[1];
                        break;
                    case "НазначениеПлатежа1":
                        outpp.НазначениеПлатежа += line[1];
                        break;
                    case "НазначениеПлатежа2":
                        outpp.НазначениеПлатежа += line[1];
                        break;
                    case "ПолучательБанк1":
                        outpp.ПолучательБанк1 = line[1];
                        break;
                    case "КвитанцияДата":
                        if (String.IsNullOrEmpty(line[1]))
                        {
                            outpp.КвитанцияДата = new DateTime(1990, 1, 1);
                            break;
                        }
                        outpp.КвитанцияДата = Convert.ToDateTime(line[1]);
                        break;

                    case "ПлательщикИНН":                        
                        outpp.ПлательщикИНН = line[1];
                        break;
                    case "ПолучательИНН":
                        outpp.ПолучательИНН = line[1];
                        break;
                }

            });

           

            if (((outpp.ДатаПоступило == new DateTime(1990, 1, 1)) 
                && (outpp.ДатаСписано == new DateTime(1990, 1, 1)) 
                && outpp.КвитанцияДата== new DateTime(1990, 1, 1))
                ||(
                (outpp.ДатаПоступило == DateTime.MinValue)
                && (outpp.ДатаСписано == DateTime.MinValue)
                && outpp.КвитанцияДата == DateTime.MinValue)                    
             )
            {                
                if (currentAccount == outpp.ПлательщикСчет)
                    outpp.ДатаСписано = outpp.Дата;
                else
                    outpp.ДатаПоступило = outpp.Дата;
            }

            //if ((outpp.ДатаПоступило == new DateTime(1990, 1, 1)) && (outpp.ДатаСписано == DateTime.MinValue) && outpp.КвитанцияДата == DateTime.MinValue)
            //{
            //    outpp.ДатаПоступило = outpp.Дата;
            //}

            if ((outpp.ДатаПоступило == new DateTime(1990, 1, 1)) && (outpp.ДатаСписано == DateTime.MinValue) && outpp.КвитанцияДата != DateTime.MinValue)
            {
                if (currentAccount == outpp.ПлательщикСчет)
                    outpp.ДатаСписано = outpp.КвитанцияДата;
                else
                    outpp.ДатаПоступило = outpp.КвитанцияДата;
            }


            if (outpp.ДатаСписано < new DateTime(1990, 1, 1))
                outpp.ДатаСписано = new DateTime(1990, 1, 1);

            if (outpp.ДатаПоступило < new DateTime(1990, 1, 1))
                outpp.ДатаПоступило = new DateTime(1990, 1, 1);

            if (outpp.ДатаПоступило == new DateTime(1990, 1, 1) && outpp.ДатаСписано == new DateTime(1990, 1, 1))
            {
                if (outpp.КвитанцияДата > new DateTime(1990, 1, 1))
                {
                    if (currentAccount == outpp.ПлательщикСчет)
                        outpp.ДатаСписано = outpp.КвитанцияДата;
                    else
                        outpp.ДатаПоступило = outpp.КвитанцияДата;
                }
                else
                {
                    if (currentAccount == outpp.ПлательщикСчет)
                        outpp.ДатаСписано = outpp.Дата;
                    else
                        outpp.ДатаПоступило = outpp.Дата;
                }
            }

            



            return outpp;
        }

        private void GetcurrentAccount()
        {
            foreach(string line in linesFomFile)
            {
                string[] split = line.Split('=');
                if(split.Count()==2 && split[0].ToUpper() == "РасчСчет".ToUpper())
                {
                    currentAccount = split[1];
                }
            }
        }

        public int StartImport(string path)
        {
            if (!File.Exists(path))
                return -1;

            this.path = path;
            linesFomFile = File.ReadAllLines(path, Encoding.GetEncoding(1251)).ToList();

            if (linesFomFile.Count == 0)
            {
                return -1;
            }

            if (linesFomFile[0].ToUpper() != "1CClientBankExchange".ToUpper())
                return 0;

            GetcurrentAccount();

            bool end = false; 
            List<PP> ppList = new List<PP>();
            List<BOImport> boList = new List<BOImport>();
            posInFile = 0;
            while (!end)
            {
                Doc doc = this.GetNextPP();
                List<string> stingList = doc.lines;
                if (stingList.Count == 0)
                {
                    end = true;
                    break;
                }

                PP pp = this.ParsePP(stingList);
                pp.Type = doc.type;
                ppList.Add(pp);
                                   
                
            }

            int count = 0;
            ppList.ForEach(pp =>
            {
                if (InsertPP(pp) > 0)
                {
                    count++;
                }
            });            

            foreach (Ostatki ostatok in GetOstatokList())
            {
                InsertOstatok(ostatok);
            }
            context.SaveChanges();
            

            return count;
        }

        //В базе возможны дубликаты при одновременном использовании импорта с разных клиентов
       public  void DeleteDuplicates()
        {
            try
            {

                var ostatki = context.Database
                    .ExecuteSqlCommand("DeleteDuplicates");

            } catch (Exception ex)
            {
                Log.AddToLog(ex.Message);
            }
            
        }

        public int InsertOstatok(Ostatki ostatok)
        {
            var exist = context.Ostatki.Where(x => x.РасчСчет == ostatok.РасчСчет && x.ДатаКонца == ostatok.ДатаКонца);
            if (exist.Count() != 0) return -1;
            context.Ostatki.Add(ostatok);
            context.SaveChanges();
            return 0;
        }

        public int InsertPP(PP pp)
        {
            InsertIfNotExistCompany(pp);            
            InsertIfNotExistBank(new Bank { BankName = pp.ПлательщикБанк1, BankBik = pp.ПлательщикБИК });
            InsertIfNotExistBank(new Bank { BankName = pp.ПолучательБанк1, BankBik = pp.ПолучательБИК });
            InsertIfNotExistAcc(pp.ПлательщикСчет, pp.ПлательщикБанк1, pp.ПлательщикБИК, pp.ПлательщикИНН);
            InsertIfNotExistAcc(pp.ПолучательСчет, pp.ПолучательБанк1, pp.ПолучательБИК, pp.ПолучательИНН);

            

            //int Получатель1 = GetCompany(pp.Получатель1).CompanyId;
            //int Плательщик1 = GetCompany(pp.Плательщик1).CompanyId;
            int ПлательщикСчет = context.Account.Where(acc => acc.Account1 == pp.ПлательщикСчет).First().AccountId;
            int ПолучательСчет = context.Account.Where(acc => acc.Account1 == pp.ПолучательСчет).First().AccountId;
            IQueryable<PPBD> qppbd = context.PPBD.Where(ppbd =>
                ppbd.Дата == pp.Дата
                && ppbd.Номер == pp.Номер
                && ppbd.ПлательщикСчет == ПлательщикСчет
                && ppbd.ПолучательСчет == ПолучательСчет
                && ppbd.Сумма == pp.Сумма
                && ppbd.ДатаПоступило == pp.ДатаПоступило);
            if (qppbd.Count() > 0)
            {
                return -2;//уже существует
            }
            context.PPBD.Add(new PPBD()
            {
                Дата = pp.Дата,
                Номер = pp.Номер,
                ДатаПоступило = pp.ДатаПоступило,
                ДатаСписано = pp.ДатаСписано,
                ПлательщикСчет = ПлательщикСчет,
                ПолучательСчет = ПолучательСчет,
                Сумма =pp.Сумма,
                НазначениеПлатежа = pp.НазначениеПлатежа,
                ПолучательРасчСчет = pp.ПолучательРасчСчет,
                Type = pp.Type,
                ДатаВыполнено = GetДатаВыполнено(pp),
                ПлательщикИНН = pp.ПлательщикИНН,
                ПлательщикБИК = pp.ПлательщикБИК,
                ПолучательБИК = pp.ПолучательБИК,
                ПолучательИНН = pp.ПолучательИНН,
                Получатель = pp.Получатель1
            });

            int id = 1;
            return id;
            
        }

        DateTime GetДатаВыполнено(PP pp)
        {
            DateTime rez = new DateTime(1995, 1, 1);
            if (pp.Type != "MO")
            {
                return (pp.ДатаПоступило > new DateTime(1995, 1, 1)) ? pp.ДатаПоступило : pp.ДатаСписано;
            }
            else {
                if (pp.КвитанцияДата > new DateTime(1995, 1, 1))
                    return pp.КвитанцияДата;

                if (pp.ДатаСписано > new DateTime(1995, 1, 1))
                    return pp.ДатаСписано;

                if (pp.ДатаСписано > new DateTime(1995, 1, 1))
                    return pp.ДатаСписано;

                if (pp.ДатаПоступило > new DateTime(1995, 1, 1))
                    return pp.ДатаПоступило;                
            }

            return pp.Дата;       
        }

        int InsertIfNotExistBank(Bank bank)
        {            
            int inserted = 0;

            var foundBank = context.Bank.ToList().Where(b => (b.BankBik == bank.BankBik)||(b.BankName.ToLower() == bank.BankName.ToLower()) );
            if (foundBank.Count() == 0)
            {
                context.Bank.Add(bank);
                context.SaveChanges();
            }
            else {
                if (String.IsNullOrEmpty(foundBank.First().BankName))
                {
                    foundBank.First().BankName = bank.BankName;
                    context.SaveChanges();
                }

                if (String.IsNullOrEmpty(foundBank.First().BankBik))
                {
                    foundBank.First().BankBik = bank.BankBik;
                    context.SaveChanges();
                }
            }
            return 0;
        }

        int InsertIfNotExistAcc(string acc, string bankName, string bankBik, string companyINN)
        {

            int inserted = 0;

            var banks = context.Bank.ToList().Where(b => (b.BankBik == bankBik) || (b.BankName.ToLower() == bankName.ToLower()));
            //var companies = context.Company.Where(b => b.CompanyName == companyName);
            var company = GetCompanyByINN(companyINN);
            int BankId = banks.Count() > 0 ? banks.First().BankId : 0;
            var accounts = context.Account.ToList().Where(b => b.Account1 == acc);
            if (accounts.Count() == 0) {
                context.Account.Add(new Account()
                {
                    Account1 = acc,
                    BankId = banks.Count()>0? banks.First().BankId:0,
                    CompanyId = company != null ? company.CompanyId : 0,

                });

                context.SaveChanges();

            }            

            return 0;
        }

        public Company GetCompany(string name)
        {
            var copanies = context.Company.Where(company => company.CompanyName.ToUpper() == name.ToUpper());
            if (copanies.Count() == 0)
            {
                copanies = context.Company.Where(company => company.CompanyAbr.ToUpper().IndexOf(name.ToUpper()) >-1);
            }

            if (copanies.Count() > 0)
            {
                return copanies.First();
            }
            

            Regex regex = new Regex(@""".+""", RegexOptions.IgnoreCase);
            var matches = regex.Matches(name);
            
            if (matches.Count == 0)
            {
                return null;
            }
  
            string clearName = matches[0].Value.Replace("\"", "").ToUpper();
            copanies = context.Company.Where(company => company.CompanyName.ToUpper().IndexOf(clearName) > -1);
  

            if (copanies.Count() == 0)
            {
                return null;
            }
            return copanies.First();

        }

        public Company GetCompanyByINN(string companyINN)
        {
            var copanies = context.Company.Where(company => company.CompanyINN.ToUpper() == companyINN.ToUpper());

            if (copanies.Count() > 0)
            {
                return copanies.First();
            }
            else
            {
                return null;
            }
        }

        int InsertIfNotExistCompany(PP pp)
        {
            
            int inserted = 0;

            var copanies = GetCompanyByINN(pp.ПолучательИНН);

            if (copanies == null)
            {
                context.Company.Add(new Company()
                {
                    CompanyName = pp.Получатель1,
                    CompanyINN = pp.ПолучательИНН


                });
                context.SaveChanges();
            }
            else
            {
                
                if (copanies.CompanyName.Length > pp.Получатель1.Length && pp.Получатель1.Length>2)
                {
                    copanies.CompanyName = pp.Получатель1;
                }
            }            
            

          

            copanies = GetCompanyByINN(pp.ПлательщикИНН);
            if (copanies == null)
            {
                context.Company.Add(new Company()
                {
                    CompanyName = pp.Плательщик1,
                    CompanyINN = pp.ПлательщикИНН


                });
                context.SaveChanges();
            }
            else {
                if (copanies.CompanyName.Length > pp.Плательщик1.Length)
                {
                    //if (copanies.CompanyFizLizo == true)
                    //{
                    //    copanies = GetFizLizo(copanies, pp);
                    //}
                    //else
                    {
                        copanies.CompanyName = pp.Плательщик1;
                    }
                    
                }
            }


            return inserted;
        }

        Company GetFizLizo(Company copanies, PP pp)
        {
            if (copanies.CompanyName.ToUpper() == pp.Плательщик1.ToUpper())
            {
                return copanies;
            }
            else
            {
                //поиск по имени
                Company company = context.Company.Where(x =>
                    x.CompanyName.ToUpper() == pp.Плательщик1.ToUpper()
                    && x.CompanyINN == pp.ПлательщикИНН).FirstOrDefault();
                if (company != null)
                {
                    return company;
                }
                else
                {
                    context.Company.Add(new Company()
                    {
                        CompanyName = pp.Плательщик1,
                        CompanyINN = pp.ПлательщикИНН
                    });
                    context.SaveChanges();
                }


            }

            return copanies;
        }
        int InsertIncoming()
        {

            return 0;
        }

        int InsertOutcoming()
        {

            return 0;
        }

        public BOImport ParseBO(List<string> pp)
        {
            BOImport outpp = new BOImport();
            pp.ForEach(n =>
            {
                string[] line = n.Split('=');
                line[1] = line[1].Trim();
                switch (line[0].Trim())
                {
                    case "Номер":
                        outpp.Номер = Convert.ToInt32(line[1]);
                        break;
                    case "Дата":
                        outpp.Дата = Convert.ToDateTime(line[1]);
                        break;
                    case "Сумма":
                        try
                        {
                            outpp.Сумма = Convert.ToDouble(line[1]);
                        }
                        catch (Exception ex)
                        {
                            outpp.Сумма = Convert.ToDouble(line[1].Replace('.', ','));
                        }

                        break;
                    case "ПлательщикСчет":
                        outpp.ПлательщикСчет = line[1];
                        break;
                    case "Плательщик1":
                        outpp.Плательщик1 = line[1];
                        break;
                    case "ПлательщикБанк1":
                        outpp.ПлательщикБанк1 = line[1];
                        break;
                    case "Получатель1":
                        outpp.Получатель1 = line[1];
                        break;
                    case "ПолучательРасчСчет":
                        outpp.ПолучательРасчСчет = line[1];
                        break;
                    case "ПолучательСчет":
                        outpp.ПолучательСчет = line[1];
                        break;
                    case "ДатаПоступило":
                        if (String.IsNullOrEmpty(line[1]))
                        {
                            outpp.ДатаПоступило = new DateTime(1990, 1, 1);
                            break;
                        }
                        outpp.ДатаПоступило = Convert.ToDateTime(line[1]);
                        break;
                    case "ДатаСписано":
                        if (String.IsNullOrEmpty(line[1]))
                        {
                            outpp.ДатаСписано = new DateTime(1990, 1, 1);
                            break;
                        }
                        outpp.ДатаСписано = Convert.ToDateTime(line[1]);
                        break;
                    case "НазначениеПлатежа":
                        outpp.НазначениеПлатежа = line[1];
                        break;
                }

            });

            if (outpp.ДатаСписано < new DateTime(1990, 1, 1))
                outpp.ДатаСписано = new DateTime(1990, 1, 1);

            if (outpp.ДатаПоступило < new DateTime(1990, 1, 1))
                outpp.ДатаПоступило = new DateTime(1990, 1, 1);

            return outpp;
        }

        //public int InsertBO(BOImport pp)
        //{
        //    InsertIfNotExistCompany((PP)pp);
        //    InsertIfNotExistBank(pp.ПлательщикБанк1);
        //    InsertIfNotExistBank(pp.ПолучательБанк1);
        //    InsertIfNotExistAcc(pp.ПлательщикСчет, pp.ПлательщикБанк1, pp.Плательщик1);
        //    InsertIfNotExistAcc(pp.ПолучательСчет, pp.ПолучательБанк1, pp.Получатель1);

        //    var context = new TableBIEntities2();

        //    int Получатель1 = GetCompany(pp.Получатель1).CompanyId;
        //    int Плательщик1 = GetCompany(pp.Плательщик1).CompanyId;
        //    int ПлательщикСчет = context.Account.Where(acc => acc.Account1 == pp.ПлательщикСчет).First().AccountId;
        //    int ПолучательСчет = context.Account.Where(acc => acc.Account1 == pp.ПолучательСчет).First().AccountId;
        //    IQueryable<BO> qppbd = context.BO.Where(ppbd =>
        //        ppbd.Дата == pp.Дата
        //        && ppbd.Номер == pp.Номер
        //        && ppbd.ПлательщикСчет == ПлательщикСчет);
        //    if (qppbd.Count() > 0)
        //    {
        //        return -2;//уже существует
        //    }
        //    context.BO.Add(new BO()
        //    {
        //        Дата = pp.Дата,
        //        Номер = pp.Номер,
        //        ДатаПоступило = pp.ДатаПоступило,
        //        ДатаСписано = pp.ДатаСписано,
        //        ПлательщикСчет = ПлательщикСчет,
        //        ПолучательСчет = ПолучательСчет,
        //        Сумма = pp.Сумма,
        //        НазначениеПлатежа = pp.НазначениеПлатежа,
        //        ПолучательРасчСчет = pp.ПолучательРасчСчет

        //    });

        //    int id = context.SaveChanges();
        //    return id;
        //    //try
        //    //{
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //    return -1;
        //    //}


        //}
    }
}
