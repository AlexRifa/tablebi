﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using TableBI.BL;
using System.Threading;
using System.Diagnostics;

namespace Update
{
    class Program
    {
        static void Main(string[] args)
        {
            //Config.Path= @"C:\projects\Other\TableBI\TableBI\bin\Debug";
            
            string tableExe = Config.Path + @"\TableBI.exe";
            string tableExeConfig = Config.Path + @"\TableBI.exe.config";
            string tableExePdb = Config.Path + @"\TableBI.pdb";
            string TableBIBLdll = Config.Path + @"\TableBI.BL.dll";
            string TableBIBLpdb = Config.Path + @"\TableBI.BL.pdb";
            string TableBIDatadll = Config.Path + @"\TableBI.Data.dll";
            string TableBIDatapdb = Config.Path + @"\TableBI.Data.pdb";
            

            List<string> files = new List<string> { tableExe, tableExeConfig, tableExePdb, TableBIBLdll, TableBIBLpdb, TableBIDatadll, TableBIDatapdb };
            Console.WriteLine("Path:"+ Config.Path);
            Thread.Sleep(3500);

            try
            {
             

                Console.WriteLine("Нужно закрыть TableBI!");
                Thread.Sleep(500);
                int j = 0;
                while (Process.GetProcessesByName("TableBI").Length > 0)
                {
                    Console.WriteLine("Terminate process! TableBI");
                    Process[] myProcesses2 = Process.GetProcessesByName("TableBI");
                    for (int i = 0; i < myProcesses2.Length; i++) {                        
                        myProcesses2[i].Kill();
                        Console.WriteLine("Close:" );
                        Thread.Sleep(1300);

                    }
                    j++;
                    Thread.Sleep(300);
                    if (j > 5)
                    {
                        break;
                    }
                }
                

                
                
            }
               catch (Exception ex) {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
            Thread.Sleep(1500);
            Console.WriteLine("Close");
    

            foreach (string file in files)
            {
                
                    
                    if (File.Exists(file))
                    {


                    //File.Move(Path.GetFileName(file), Path.GetFileName(file)+"22");
                    //File.Delete(Path.GetFileName(file) + "22");
                
                    try
                    {

                        //File.Replace(file, Path.GetFileName(file), "");
                        File.Delete(Path.GetFileName(file));
                        File.Copy(file, Path.GetFileName(file));
                        Console.WriteLine(file, Path.GetFileName(file));
                    }
                    catch (Exception ex) { Console.WriteLine(ex.Message, file); }


                }

            }
            Thread.Sleep(2500);
            Process.Start("TableBI.exe");

        }        
    }
}
