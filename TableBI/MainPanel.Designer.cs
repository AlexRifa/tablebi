﻿namespace TableBI
{
    partial class MainPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainPanel));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode2 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.Animation.PushTransition pushTransition1 = new DevExpress.Utils.Animation.PushTransition();
            this.gridView15 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn76 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.gridControlOstatki = new DevExpress.XtraGrid.GridControl();
            this.bindingSource14 = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewOstatki = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOstatokSelected = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCompanyName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBankName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colКонечныйОстаток = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOstatokProcess = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn75 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOstatokFrom = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOstatokTakeoff = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOstatokPrimichanie = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOstatokProcessSpisanie1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOstatokTo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOstatokProcessSpisanie2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOstatokTo2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOstatokProcessSpisanie3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOstatokTo3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOstatokProcessSpisanie4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOstatokTo4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barDockingMenuItem1 = new DevExpress.XtraBars.BarDockingMenuItem();
            this.bbiUsers = new DevExpress.XtraBars.BarButtonItem();
            this.bbiConnect = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemSpinEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.barStaticItem1 = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticItemSum = new DevExpress.XtraBars.BarStaticItem();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barEditItem3 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barButtonItem10 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem11 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonMiniToolbar1 = new DevExpress.XtraBars.Ribbon.RibbonMiniToolbar(this.components);
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.grid = new DevExpress.XtraGrid.GridControl();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPPId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colНомер = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colДата = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colСумма = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colНДС = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colПлатильщикБанк = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colПлатильщикКомпания = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colПлатильщикКомпанияИмя = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colПлатильщикБанкИмя = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colГруппа = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colПлательщикСчет = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colПолучательБанк = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colПолучательКомпанияИмя = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colПолучательБанкИмя = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colПолучательКомпания = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colГруппа2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colПолучательСчет = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colНазначениеПлатежа1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colНаш = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColor21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionPlanDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionPayDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionPrecent = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridPlanSum = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionDebt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionComment = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPercentValue1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionCurDebt = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerId2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerColor2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView12 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView14 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.bindingSource17 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource4 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource11 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource9 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource7 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource8 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource6 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource10 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource5 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barManager2 = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControl1 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl2 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl3 = new DevExpress.XtraBars.BarDockControl();
            this.barDockControl4 = new DevExpress.XtraBars.BarDockControl();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timerUpdateAll = new System.Windows.Forms.Timer(this.components);
            this.bindingSourceOstatkiPeriod = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource15 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourceSection = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource12 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource13 = new System.Windows.Forms.BindingSource(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridViewCompany = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCompanyName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyFizLizo1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colCompanyINN1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyOstatok1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colGroupId3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEditCompany = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colOwnerId3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEditOwner = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colTypeCompanyId3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colOstatokPeriodId1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckedComboBoxEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit();
            this.repositoryItemLookUpEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyFizLizo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyINN = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupId2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeCompanyId2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyOstatok = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOstatokPeriodId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwner1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl3 = new DevExpress.XtraGrid.GridControl();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colGroupId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage9 = new DevExpress.XtraTab.XtraTabPage();
            this.gridPercent = new DevExpress.XtraGrid.GridControl();
            this.gridViewPercent = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colPercentId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwnerId1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPrecentDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPercentDateEnd = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPercentValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPercentDayInc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOwner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPercentNdsValue = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPercentNdsDayInc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeCompanyId1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit6 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colPercentPrihod = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemColorEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.xtraTabPage10 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.gridViewPercentUhod = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn24 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn25 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn26 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn27 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn28 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn29 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn30 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn31 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn32 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn33 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit7 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridColumn34 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemColorEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOwnerName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colColor = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemColorEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.colOwnerPlusDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupId1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.xtraTabPage8 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colTypeCompanyId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTypeCompanyName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colBankName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBankAbr = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBankBik = new DevExpress.XtraGrid.Columns.GridColumn();
            this.xtraTabPage13 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlAccount = new DevExpress.XtraGrid.GridControl();
            this.gridViewAccount = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colAccount1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colCompanyId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colBankId = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colAccountActive = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit4 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.colAccountBlock = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.xtraTabPage14 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlOstatkiSectionDetail = new DevExpress.XtraGrid.GridControl();
            this.bindingSource18 = new System.Windows.Forms.BindingSource(this.components);
            this.gridViewOstatkiSectionDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumnTypeCompany = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit15 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridColumnOwner = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit14 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridColumnGroup = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit16 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemColorEdit5 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.repositoryItemLookUpEdit13 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit11 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemLookUpEdit12 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.gridControlSection = new DevExpress.XtraGrid.GridControl();
            this.gridViewSection = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colOstatkiSectionName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colGroupId4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit9 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.colOwnerId4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemLookUpEdit10 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.repositoryItemColorEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemColorEdit();
            this.repositoryItemLookUpEdit8 = new DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit();
            this.xtraTabPage12 = new DevExpress.XtraTab.XtraTabPage();
            this.sbtnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.sBtnOstatkiExcel = new DevExpress.XtraEditors.SimpleButton();
            this.dateEditOstatki = new DevExpress.XtraEditors.DateEdit();
            this.lookUpEditSection = new DevExpress.XtraEditors.LookUpEdit();
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.gridViewUhod = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn14 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colНазначениеПлатежа = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colКомиссия = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colОбщийПриход = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colГруппа21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colГруппа22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colPercentDayInc1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colПланДата = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnУходПроцент = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colДатаВыполнено = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colДатаСписано = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn36 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn37 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn38 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn39 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionComment1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActionPayDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView13 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPage11 = new DevExpress.XtraTab.XtraTabPage();
            this.sBtn = new DevExpress.XtraEditors.SimpleButton();
            this.sBtnAddDirect = new DevExpress.XtraEditors.SimpleButton();
            this.gridControlDirect = new DevExpress.XtraGrid.GridControl();
            this.gridViewDirect = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn41 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn42 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn43 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn46 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn47 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn48 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn51 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn53 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn54 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn56 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn59 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView16 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.sBtnEditDirect = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage7 = new DevExpress.XtraTab.XtraTabPage();
            this.spinEditOut = new DevExpress.XtraEditors.SpinEdit();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.spinEditPP = new DevExpress.XtraEditors.SpinEdit();
            this.mainTab = new DevExpress.XtraTab.XtraTabControl();
            this.bindingSource16 = new System.Windows.Forms.BindingSource(this.components);
            this.barWorkspaceMenuItem1 = new DevExpress.XtraBars.BarWorkspaceMenuItem();
            this.workspaceManager1 = new DevExpress.Utils.WorkspaceManager();
            this.ribbonMiniToolbar2 = new DevExpress.XtraBars.Ribbon.RibbonMiniToolbar(this.components);
            this.ribbonPageGroup5 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup6 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonGroup1 = new DevExpress.XtraBars.BarButtonGroup();
            this.ribbonPageGroup7 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.beiBegin = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.beiEnd = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemDateEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOstatki)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOstatki)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceOstatkiPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceSection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource13)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditCompany)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditOwner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPercent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit3)).BeginInit();
            this.xtraTabPage10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPercentUhod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).BeginInit();
            this.xtraTabPage8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            this.xtraTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            this.xtraTabPage13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).BeginInit();
            this.xtraTabPage14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOstatkiSectionDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOstatkiSectionDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit8)).BeginInit();
            this.xtraTabPage12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOstatki.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOstatki.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditSection.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUhod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).BeginInit();
            this.xtraTabPage11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDirect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDirect)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView16)).BeginInit();
            this.xtraTabPage7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditOut.Properties)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPP.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainTab)).BeginInit();
            this.mainTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridView15
            // 
            this.gridView15.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn76});
            this.gridView15.GridControl = this.gridControlOstatki;
            this.gridView15.Name = "gridView15";
            // 
            // gridColumn76
            // 
            this.gridColumn76.Caption = "gridColumn76";
            this.gridColumn76.ColumnEdit = this.repositoryItemImageEdit1;
            this.gridColumn76.Name = "gridColumn76";
            this.gridColumn76.Visible = true;
            this.gridColumn76.VisibleIndex = 0;
            // 
            // repositoryItemImageEdit1
            // 
            this.repositoryItemImageEdit1.AutoHeight = false;
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            this.repositoryItemImageEdit1.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.Squeeze;
            // 
            // gridControlOstatki
            // 
            this.gridControlOstatki.DataSource = this.bindingSource14;
            this.gridControlOstatki.Dock = System.Windows.Forms.DockStyle.Fill;
            gridLevelNode1.LevelTemplate = this.gridView15;
            gridLevelNode1.RelationName = "Level1";
            this.gridControlOstatki.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControlOstatki.Location = new System.Drawing.Point(0, 0);
            this.gridControlOstatki.MainView = this.gridViewOstatki;
            this.gridControlOstatki.MenuManager = this.ribbon;
            this.gridControlOstatki.Name = "gridControlOstatki";
            this.gridControlOstatki.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageEdit1,
            this.repositoryItemPictureEdit1,
            this.repositoryItemCheckEdit1});
            this.gridControlOstatki.Size = new System.Drawing.Size(1356, 333);
            this.gridControlOstatki.TabIndex = 0;
            this.gridControlOstatki.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewOstatki,
            this.gridView15});
            this.gridControlOstatki.Click += new System.EventHandler(this.gridControlOstatki_Click);
            // 
            // gridViewOstatki
            // 
            this.gridViewOstatki.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOstatokSelected,
            this.colCompanyName2,
            this.colBankName1,
            this.colКонечныйОстаток,
            this.colOstatokProcess,
            this.gridColumn75,
            this.colOstatokFrom,
            this.colOstatokTakeoff,
            this.colOstatokPrimichanie,
            this.colOstatokProcessSpisanie1,
            this.colOstatokTo1,
            this.colOstatokProcessSpisanie2,
            this.colOstatokTo2,
            this.colOstatokProcessSpisanie3,
            this.colOstatokTo3,
            this.colOstatokProcessSpisanie4,
            this.colOstatokTo4});
            this.gridViewOstatki.GridControl = this.gridControlOstatki;
            this.gridViewOstatki.Name = "gridViewOstatki";
            this.gridViewOstatki.OptionsCustomization.AllowSort = false;
            this.gridViewOstatki.OptionsFind.AlwaysVisible = true;
            this.gridViewOstatki.OptionsMenu.EnableColumnMenu = false;
            this.gridViewOstatki.OptionsView.AllowCellMerge = true;
            this.gridViewOstatki.OptionsView.ColumnAutoWidth = false;
            this.gridViewOstatki.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewOstatki.OptionsView.RowAutoHeight = true;
            this.gridViewOstatki.OptionsView.ShowGroupPanel = false;
            this.gridViewOstatki.RowHeight = 13;
            this.gridViewOstatki.ViewCaptionHeight = 0;
            this.gridViewOstatki.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridViewOstatki_CustomDrawCell);
            this.gridViewOstatki.RowCellStyle += new DevExpress.XtraGrid.Views.Grid.RowCellStyleEventHandler(this.gridViewOstatki_RowCellStyle);
            this.gridViewOstatki.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewOstatki_CustomRowCellEdit);
            this.gridViewOstatki.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewOstatki_RowUpdated);
            this.gridViewOstatki.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridViewOstatki_MouseDown);
            // 
            // colOstatokSelected
            // 
            this.colOstatokSelected.Caption = " ";
            this.colOstatokSelected.ColumnEdit = this.repositoryItemCheckEdit1;
            this.colOstatokSelected.FieldName = "OstatokSelected";
            this.colOstatokSelected.Name = "colOstatokSelected";
            this.colOstatokSelected.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokSelected.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokSelected.OptionsFilter.AllowFilter = false;
            this.colOstatokSelected.Visible = true;
            this.colOstatokSelected.VisibleIndex = 1;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // colCompanyName2
            // 
            this.colCompanyName2.Caption = "Компания";
            this.colCompanyName2.FieldName = "CompanyName";
            this.colCompanyName2.Name = "colCompanyName2";
            this.colCompanyName2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.True;
            this.colCompanyName2.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colCompanyName2.OptionsFilter.AllowFilter = false;
            this.colCompanyName2.Visible = true;
            this.colCompanyName2.VisibleIndex = 2;
            // 
            // colBankName1
            // 
            this.colBankName1.Caption = "Банк";
            this.colBankName1.FieldName = "BankName";
            this.colBankName1.Name = "colBankName1";
            this.colBankName1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colBankName1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colBankName1.OptionsFilter.AllowFilter = false;
            this.colBankName1.Visible = true;
            this.colBankName1.VisibleIndex = 3;
            // 
            // colКонечныйОстаток
            // 
            this.colКонечныйОстаток.Caption = "Остаток на счете фактический";
            this.colКонечныйОстаток.DisplayFormat.FormatString = "c2";
            this.colКонечныйОстаток.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colКонечныйОстаток.FieldName = "КонечныйОстаток";
            this.colКонечныйОстаток.Name = "colКонечныйОстаток";
            this.colКонечныйОстаток.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colКонечныйОстаток.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colКонечныйОстаток.OptionsFilter.AllowFilter = false;
            this.colКонечныйОстаток.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "КонечныйОстаток", "{0:c2}")});
            this.colКонечныйОстаток.Visible = true;
            this.colКонечныйОстаток.VisibleIndex = 5;
            this.colКонечныйОстаток.Width = 111;
            // 
            // colOstatokProcess
            // 
            this.colOstatokProcess.Caption = "В процессе зачисления без формулы";
            this.colOstatokProcess.DisplayFormat.FormatString = "c2";
            this.colOstatokProcess.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOstatokProcess.FieldName = "OstatokProcess";
            this.colOstatokProcess.Name = "colOstatokProcess";
            this.colOstatokProcess.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokProcess.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokProcess.OptionsFilter.AllowFilter = false;
            this.colOstatokProcess.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OstatokProcess", "{0:c2}")});
            this.colOstatokProcess.Visible = true;
            this.colOstatokProcess.VisibleIndex = 4;
            this.colOstatokProcess.Width = 137;
            // 
            // gridColumn75
            // 
            this.gridColumn75.Caption = "Остаток на счете рабочий";
            this.gridColumn75.DisplayFormat.FormatString = "c2";
            this.gridColumn75.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn75.FieldName = "OstatokRabochiy";
            this.gridColumn75.Name = "gridColumn75";
            this.gridColumn75.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn75.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn75.OptionsFilter.AllowFilter = false;
            this.gridColumn75.Visible = true;
            this.gridColumn75.VisibleIndex = 0;
            // 
            // colOstatokFrom
            // 
            this.colOstatokFrom.Caption = "От кого";
            this.colOstatokFrom.FieldName = "OstatokFrom";
            this.colOstatokFrom.Name = "colOstatokFrom";
            this.colOstatokFrom.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokFrom.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokFrom.OptionsFilter.AllowFilter = false;
            this.colOstatokFrom.Visible = true;
            this.colOstatokFrom.VisibleIndex = 6;
            this.colOstatokFrom.Width = 102;
            // 
            // colOstatokTakeoff
            // 
            this.colOstatokTakeoff.Caption = "Заказали на снятие";
            this.colOstatokTakeoff.FieldName = "OstatokTakeoff";
            this.colOstatokTakeoff.Name = "colOstatokTakeoff";
            this.colOstatokTakeoff.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokTakeoff.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokTakeoff.OptionsFilter.AllowFilter = false;
            this.colOstatokTakeoff.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "OstatokTakeoff", "{0:c2}")});
            this.colOstatokTakeoff.Visible = true;
            this.colOstatokTakeoff.VisibleIndex = 7;
            this.colOstatokTakeoff.Width = 119;
            // 
            // colOstatokPrimichanie
            // 
            this.colOstatokPrimichanie.Caption = "Примечание";
            this.colOstatokPrimichanie.FieldName = "OstatokPrimichanie";
            this.colOstatokPrimichanie.Name = "colOstatokPrimichanie";
            this.colOstatokPrimichanie.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokPrimichanie.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokPrimichanie.OptionsFilter.AllowFilter = false;
            this.colOstatokPrimichanie.Visible = true;
            this.colOstatokPrimichanie.VisibleIndex = 8;
            this.colOstatokPrimichanie.Width = 143;
            // 
            // colOstatokProcessSpisanie1
            // 
            this.colOstatokProcessSpisanie1.Caption = "Сумма в процессе списания";
            this.colOstatokProcessSpisanie1.DisplayFormat.FormatString = "c2";
            this.colOstatokProcessSpisanie1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOstatokProcessSpisanie1.FieldName = "OstatokProcessSpisanie1";
            this.colOstatokProcessSpisanie1.Name = "colOstatokProcessSpisanie1";
            this.colOstatokProcessSpisanie1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokProcessSpisanie1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokProcessSpisanie1.OptionsFilter.AllowFilter = false;
            this.colOstatokProcessSpisanie1.Visible = true;
            this.colOstatokProcessSpisanie1.VisibleIndex = 9;
            // 
            // colOstatokTo1
            // 
            this.colOstatokTo1.Caption = "Кому";
            this.colOstatokTo1.FieldName = "OstatokTo1";
            this.colOstatokTo1.Name = "colOstatokTo1";
            this.colOstatokTo1.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokTo1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokTo1.OptionsFilter.AllowFilter = false;
            this.colOstatokTo1.Visible = true;
            this.colOstatokTo1.VisibleIndex = 10;
            // 
            // colOstatokProcessSpisanie2
            // 
            this.colOstatokProcessSpisanie2.Caption = "Сумма в процессе списания";
            this.colOstatokProcessSpisanie2.DisplayFormat.FormatString = "c2";
            this.colOstatokProcessSpisanie2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOstatokProcessSpisanie2.FieldName = "OstatokProcessSpisanie2";
            this.colOstatokProcessSpisanie2.Name = "colOstatokProcessSpisanie2";
            this.colOstatokProcessSpisanie2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokProcessSpisanie2.OptionsFilter.AllowFilter = false;
            this.colOstatokProcessSpisanie2.Visible = true;
            this.colOstatokProcessSpisanie2.VisibleIndex = 11;
            // 
            // colOstatokTo2
            // 
            this.colOstatokTo2.Caption = "Кому";
            this.colOstatokTo2.FieldName = "OstatokTo2";
            this.colOstatokTo2.Name = "colOstatokTo2";
            this.colOstatokTo2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokTo2.OptionsFilter.AllowFilter = false;
            this.colOstatokTo2.Visible = true;
            this.colOstatokTo2.VisibleIndex = 12;
            // 
            // colOstatokProcessSpisanie3
            // 
            this.colOstatokProcessSpisanie3.Caption = "Сумма в процессе списания";
            this.colOstatokProcessSpisanie3.DisplayFormat.FormatString = "c2";
            this.colOstatokProcessSpisanie3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOstatokProcessSpisanie3.FieldName = "OstatokProcessSpisanie3";
            this.colOstatokProcessSpisanie3.Name = "colOstatokProcessSpisanie3";
            this.colOstatokProcessSpisanie3.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokProcessSpisanie3.OptionsFilter.AllowFilter = false;
            this.colOstatokProcessSpisanie3.Visible = true;
            this.colOstatokProcessSpisanie3.VisibleIndex = 13;
            // 
            // colOstatokTo3
            // 
            this.colOstatokTo3.Caption = "Кому";
            this.colOstatokTo3.FieldName = "OstatokTo3";
            this.colOstatokTo3.Name = "colOstatokTo3";
            this.colOstatokTo3.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokTo3.OptionsFilter.AllowFilter = false;
            this.colOstatokTo3.Visible = true;
            this.colOstatokTo3.VisibleIndex = 14;
            // 
            // colOstatokProcessSpisanie4
            // 
            this.colOstatokProcessSpisanie4.Caption = "Сумма в процессе списания";
            this.colOstatokProcessSpisanie4.DisplayFormat.FormatString = "c2";
            this.colOstatokProcessSpisanie4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOstatokProcessSpisanie4.FieldName = "OstatokProcessSpisanie4";
            this.colOstatokProcessSpisanie4.Name = "colOstatokProcessSpisanie4";
            this.colOstatokProcessSpisanie4.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokProcessSpisanie4.OptionsFilter.AllowFilter = false;
            this.colOstatokProcessSpisanie4.Visible = true;
            this.colOstatokProcessSpisanie4.VisibleIndex = 15;
            // 
            // colOstatokTo4
            // 
            this.colOstatokTo4.Caption = "Кому";
            this.colOstatokTo4.FieldName = "OstatokTo4";
            this.colOstatokTo4.Name = "colOstatokTo4";
            this.colOstatokTo4.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colOstatokTo4.OptionsFilter.AllowFilter = false;
            this.colOstatokTo4.Visible = true;
            this.colOstatokTo4.VisibleIndex = 16;
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barDockingMenuItem1,
            this.bbiUsers,
            this.bbiConnect,
            this.barButtonItem5,
            this.barEditItem1,
            this.barButtonItem6,
            this.barButtonItem7,
            this.barButtonItem8,
            this.barButtonItem9,
            this.barStaticItem1,
            this.barStaticItemSum,
            this.barEditItem2,
            this.barEditItem3,
            this.barButtonItem10,
            this.barButtonItem11,
            this.barWorkspaceMenuItem1,
            this.barSubItem1,
            this.barButtonGroup1,
            this.beiBegin,
            this.beiEnd});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 27;
            this.ribbon.MiniToolbars.Add(this.ribbonMiniToolbar1);
            this.ribbon.MiniToolbars.Add(this.ribbonMiniToolbar2);
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemSpinEdit1,
            this.repositoryItemTextEdit1,
            this.repositoryItemTextEdit2,
            this.repositoryItemDateEdit1,
            this.repositoryItemDateEdit2});
            this.ribbon.Size = new System.Drawing.Size(1362, 143);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Открыть папку";
            this.barButtonItem1.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.Glyph")));
            this.barButtonItem1.Id = 1;
            this.barButtonItem1.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.LargeGlyph")));
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "barButtonItem2";
            this.barButtonItem2.Id = 2;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Обновить";
            this.barButtonItem3.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.Glyph")));
            this.barButtonItem3.Id = 3;
            this.barButtonItem3.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.LargeGlyph")));
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem3_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Экспорт";
            this.barButtonItem4.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.Glyph")));
            this.barButtonItem4.Id = 4;
            this.barButtonItem4.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.LargeGlyph")));
            this.barButtonItem4.Name = "barButtonItem4";
            this.barButtonItem4.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem4_ItemClick);
            // 
            // barDockingMenuItem1
            // 
            this.barDockingMenuItem1.Caption = "barDockingMenuItem1";
            this.barDockingMenuItem1.Id = 5;
            this.barDockingMenuItem1.Name = "barDockingMenuItem1";
            // 
            // bbiUsers
            // 
            this.bbiUsers.Caption = "Пользователи";
            this.bbiUsers.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiUsers.Glyph")));
            this.bbiUsers.Id = 6;
            this.bbiUsers.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiUsers.LargeGlyph")));
            this.bbiUsers.Name = "bbiUsers";
            this.bbiUsers.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUsers_ItemClick);
            // 
            // bbiConnect
            // 
            this.bbiConnect.Caption = "Настройки";
            this.bbiConnect.Glyph = ((System.Drawing.Image)(resources.GetObject("bbiConnect.Glyph")));
            this.bbiConnect.Id = 7;
            this.bbiConnect.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("bbiConnect.LargeGlyph")));
            this.bbiConnect.Name = "bbiConnect";
            this.bbiConnect.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiConnect_ItemClick);
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "barButtonItem5";
            this.barButtonItem5.Id = 8;
            this.barButtonItem5.Name = "barButtonItem5";
            this.barButtonItem5.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem5_ItemClick);
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "Начальное значение";
            this.barEditItem1.Edit = this.repositoryItemSpinEdit1;
            this.barEditItem1.Id = 9;
            this.barEditItem1.Name = "barEditItem1";
            this.barEditItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            this.barEditItem1.Width = 150;
            this.barEditItem1.EditValueChanged += new System.EventHandler(this.barEditItem1_EditValueChanged);
            // 
            // repositoryItemSpinEdit1
            // 
            this.repositoryItemSpinEdit1.AutoHeight = false;
            this.repositoryItemSpinEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemSpinEdit1.MaxValue = new decimal(new int[] {
            1215752192,
            23,
            0,
            0});
            this.repositoryItemSpinEdit1.Name = "repositoryItemSpinEdit1";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "По умолчанию";
            this.barButtonItem6.Id = 12;
            this.barButtonItem6.Name = "barButtonItem6";
            this.barButtonItem6.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem6_ItemClick);
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "Сохранить как исходные";
            this.barButtonItem7.Id = 13;
            this.barButtonItem7.Name = "barButtonItem7";
            this.barButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Сохранить таблицу";
            this.barButtonItem8.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.Glyph")));
            this.barButtonItem8.Id = 14;
            this.barButtonItem8.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.LargeGlyph")));
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem8_ItemClick);
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Открыть таблицу";
            this.barButtonItem9.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem9.Glyph")));
            this.barButtonItem9.Id = 15;
            this.barButtonItem9.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItem9.LargeGlyph")));
            this.barButtonItem9.Name = "barButtonItem9";
            this.barButtonItem9.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem9_ItemClick);
            // 
            // barStaticItem1
            // 
            this.barStaticItem1.Caption = "Сумма";
            this.barStaticItem1.Id = 16;
            this.barStaticItem1.Name = "barStaticItem1";
            this.barStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticItemSum
            // 
            this.barStaticItemSum.Caption = "0";
            this.barStaticItemSum.Id = 17;
            this.barStaticItemSum.Name = "barStaticItemSum";
            this.barStaticItemSum.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barEditItem2
            // 
            this.barEditItem2.Caption = "Сумма";
            this.barEditItem2.Edit = this.repositoryItemTextEdit1;
            this.barEditItem2.Id = 18;
            this.barEditItem2.ItemAppearance.Disabled.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.barEditItem2.ItemAppearance.Disabled.Options.UseFont = true;
            this.barEditItem2.Name = "barEditItem2";
            this.barEditItem2.Width = 150;
            // 
            // repositoryItemTextEdit1
            // 
            this.repositoryItemTextEdit1.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.repositoryItemTextEdit1.Appearance.Options.UseFont = true;
            this.repositoryItemTextEdit1.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.repositoryItemTextEdit1.AppearanceDisabled.Options.UseFont = true;
            this.repositoryItemTextEdit1.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.repositoryItemTextEdit1.AppearanceFocused.Options.UseFont = true;
            this.repositoryItemTextEdit1.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.repositoryItemTextEdit1.AppearanceReadOnly.Options.UseFont = true;
            this.repositoryItemTextEdit1.AutoHeight = false;
            this.repositoryItemTextEdit1.Name = "repositoryItemTextEdit1";
            this.repositoryItemTextEdit1.ReadOnly = true;
            // 
            // barEditItem3
            // 
            this.barEditItem3.Caption = "Количество";
            this.barEditItem3.Edit = this.repositoryItemTextEdit2;
            this.barEditItem3.Id = 19;
            this.barEditItem3.Name = "barEditItem3";
            // 
            // repositoryItemTextEdit2
            // 
            this.repositoryItemTextEdit2.Appearance.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.repositoryItemTextEdit2.Appearance.Options.UseFont = true;
            this.repositoryItemTextEdit2.AppearanceDisabled.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.repositoryItemTextEdit2.AppearanceDisabled.Options.UseFont = true;
            this.repositoryItemTextEdit2.AppearanceFocused.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.repositoryItemTextEdit2.AppearanceFocused.Options.UseFont = true;
            this.repositoryItemTextEdit2.AppearanceReadOnly.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.repositoryItemTextEdit2.AppearanceReadOnly.Options.UseFont = true;
            this.repositoryItemTextEdit2.AutoHeight = false;
            this.repositoryItemTextEdit2.Name = "repositoryItemTextEdit2";
            this.repositoryItemTextEdit2.ReadOnly = true;
            // 
            // barButtonItem10
            // 
            this.barButtonItem10.Caption = "barButtonItem10";
            this.barButtonItem10.Id = 20;
            this.barButtonItem10.Name = "barButtonItem10";
            this.barButtonItem10.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem10_ItemClick);
            // 
            // barButtonItem11
            // 
            this.barButtonItem11.Caption = "barButtonItem11";
            this.barButtonItem11.Id = 21;
            this.barButtonItem11.Name = "barButtonItem11";
            this.barButtonItem11.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem11_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup7,
            this.ribbonPageGroup3,
            this.ribbonPageGroup2,
            this.ribbonPageGroup4});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Файл";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem1);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem3);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem4);
            this.ribbonPageGroup1.ItemLinks.Add(this.bbiConnect);
            this.ribbonPageGroup1.ItemLinks.Add(this.barEditItem1);
            this.ribbonPageGroup1.ItemLinks.Add(this.barButtonItem11);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.bbiUsers);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            this.ribbonPageGroup3.Text = "Администрирование";
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem6);
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem7);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "Настройки";
            // 
            // ribbonPageGroup4
            // 
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem8);
            this.ribbonPageGroup4.ItemLinks.Add(this.barButtonItem9);
            this.ribbonPageGroup4.Name = "ribbonPageGroup4";
            this.ribbonPageGroup4.Text = "Таблицы";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.barEditItem2);
            this.ribbonStatusBar.ItemLinks.Add(this.barEditItem3);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 504);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1362, 31);
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.InitialImage = ((System.Drawing.Image)(resources.GetObject("repositoryItemPictureEdit1.InitialImage")));
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.SizeMode = DevExpress.XtraEditors.Controls.PictureSizeMode.StretchHorizontal;
            // 
            // gridView8
            // 
            this.gridView8.GridControl = this.grid;
            this.gridView8.Name = "gridView8";
            // 
            // grid
            // 
            this.grid.AllowDrop = true;
            this.grid.DataSource = this.bindingSource1;
            this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grid.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Flat;
            gridLevelNode2.LevelTemplate = this.gridView8;
            gridLevelNode2.RelationName = "Level1";
            this.grid.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode2});
            this.grid.Location = new System.Drawing.Point(0, 0);
            this.grid.MainView = this.gridView1;
            this.grid.MenuManager = this.ribbon;
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(1356, 333);
            this.grid.TabIndex = 2;
            this.grid.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1,
            this.gridView12,
            this.gridView14,
            this.gridView8});
            this.grid.Load += new System.EventHandler(this.grid_Load);
            this.grid.DragDrop += new System.Windows.Forms.DragEventHandler(this.grid_DragDrop);
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(TableBI.Data.View_PP);
            // 
            // gridView1
            // 
            this.gridView1.Appearance.GroupRow.BackColor = System.Drawing.Color.White;
            this.gridView1.Appearance.GroupRow.BackColor2 = System.Drawing.Color.Gainsboro;
            this.gridView1.Appearance.GroupRow.ForeColor = System.Drawing.Color.Black;
            this.gridView1.Appearance.GroupRow.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.ForwardDiagonal;
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseTextOptions = true;
            this.gridView1.Appearance.GroupRow.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPPId,
            this.colНомер,
            this.colДата,
            this.colСумма,
            this.colНДС,
            this.colПлатильщикБанк,
            this.colПлатильщикКомпания,
            this.colПлатильщикКомпанияИмя,
            this.colПлатильщикБанкИмя,
            this.colГруппа,
            this.colOwnerName1,
            this.colПлательщикСчет,
            this.colПолучательБанк,
            this.colПолучательКомпанияИмя,
            this.colПолучательБанкИмя,
            this.colПолучательКомпания,
            this.colГруппа2,
            this.colOwnerName2,
            this.colПолучательСчет,
            this.colНазначениеПлатежа1,
            this.colНаш,
            this.colColor21,
            this.gridColumn21,
            this.gridColumn22,
            this.colActionPlanDate,
            this.colActionPayDate,
            this.colActionPrecent,
            this.colActionSum,
            this.gridPlanSum,
            this.colActionDebt,
            this.colActionComment,
            this.colPercentValue1,
            this.colActionCurDebt,
            this.gridColumn23,
            this.colOwnerId2,
            this.colOwnerColor2});
            this.gridView1.GridControl = this.grid;
            this.gridView1.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Сумма", null, "|  Сумма: {0:c2}  "),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "", null, "|  Количество= {0:#.##}  "),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Долг", null, "|  Долг = {0:c2} "),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "ПланВыдача", null, "| ПланВыдача = {0:c2} ")});
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsFilter.MRUFilterListPopupCount = 10;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsFind.ClearFindOnClose = false;
            this.gridView1.OptionsFind.FindMode = DevExpress.XtraEditors.FindMode.Always;
            this.gridView1.OptionsFind.SearchInPreview = true;
            this.gridView1.OptionsLayout.StoreVisualOptions = false;
            this.gridView1.OptionsSelection.MultiSelect = true;
            this.gridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.OptionsView.GroupDrawMode = DevExpress.XtraGrid.Views.Grid.GroupDrawMode.Standard;
            this.gridView1.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            this.gridView1.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            this.gridView1.CustomUnboundColumnData += new DevExpress.XtraGrid.Views.Base.CustomColumnDataEventHandler(this.gridView1_CustomUnboundColumnData);
            this.gridView1.LayoutUpgrade += new DevExpress.Utils.LayoutUpgradeEventHandler(this.gridView1_LayoutUpgrade);
            this.gridView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            this.gridView1.DoubleClick += new System.EventHandler(this.gridView1_DoubleClick);
            // 
            // colPPId
            // 
            this.colPPId.FieldName = "PPId";
            this.colPPId.Name = "colPPId";
            // 
            // colНомер
            // 
            this.colНомер.FieldName = "Номер";
            this.colНомер.Name = "colНомер";
            this.colНомер.Width = 60;
            // 
            // colДата
            // 
            this.colДата.DisplayFormat.FormatString = "d";
            this.colДата.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colДата.FieldName = "Дата";
            this.colДата.Name = "colДата";
            this.colДата.OptionsColumn.ReadOnly = true;
            this.colДата.Visible = true;
            this.colДата.VisibleIndex = 1;
            this.colДата.Width = 79;
            // 
            // colСумма
            // 
            this.colСумма.DisplayFormat.FormatString = "c2";
            this.colСумма.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colСумма.FieldName = "Сумма";
            this.colСумма.Name = "colСумма";
            this.colСумма.OptionsColumn.ReadOnly = true;
            this.colСумма.Visible = true;
            this.colСумма.VisibleIndex = 2;
            this.colСумма.Width = 84;
            // 
            // colНДС
            // 
            this.colНДС.FieldName = "НДС";
            this.colНДС.Name = "colНДС";
            this.colНДС.OptionsColumn.ReadOnly = true;
            this.colНДС.Visible = true;
            this.colНДС.VisibleIndex = 3;
            this.colНДС.Width = 37;
            // 
            // colПлатильщикБанк
            // 
            this.colПлатильщикБанк.FieldName = "ПлатильщикБанк";
            this.colПлатильщикБанк.Name = "colПлатильщикБанк";
            this.colПлатильщикБанк.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colПлатильщикКомпания
            // 
            this.colПлатильщикКомпания.FieldName = "ПлатильщикКомпания";
            this.colПлатильщикКомпания.Name = "colПлатильщикКомпания";
            this.colПлатильщикКомпания.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colПлатильщикКомпанияИмя
            // 
            this.colПлатильщикКомпанияИмя.Caption = "Плательщик Компания";
            this.colПлатильщикКомпанияИмя.FieldName = "ПлатильщикКомпанияИмя";
            this.colПлатильщикКомпанияИмя.Name = "colПлатильщикКомпанияИмя";
            this.colПлатильщикКомпанияИмя.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colПлатильщикКомпанияИмя.Visible = true;
            this.colПлатильщикКомпанияИмя.VisibleIndex = 4;
            this.colПлатильщикКомпанияИмя.Width = 64;
            // 
            // colПлатильщикБанкИмя
            // 
            this.colПлатильщикБанкИмя.Caption = "Плательщик Банк";
            this.colПлатильщикБанкИмя.FieldName = "ПлатильщикБанкИмя";
            this.colПлатильщикБанкИмя.Name = "colПлатильщикБанкИмя";
            this.colПлатильщикБанкИмя.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colПлатильщикБанкИмя.Visible = true;
            this.colПлатильщикБанкИмя.VisibleIndex = 5;
            this.colПлатильщикБанкИмя.Width = 64;
            // 
            // colГруппа
            // 
            this.colГруппа.Caption = "Плательщик Группа";
            this.colГруппа.FieldName = "Группа";
            this.colГруппа.Name = "colГруппа";
            this.colГруппа.OptionsColumn.ReadOnly = true;
            this.colГруппа.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colГруппа.Visible = true;
            this.colГруппа.VisibleIndex = 11;
            this.colГруппа.Width = 87;
            // 
            // colOwnerName1
            // 
            this.colOwnerName1.Caption = "Плательщик Принадлежность";
            this.colOwnerName1.FieldName = "OwnerName";
            this.colOwnerName1.Name = "colOwnerName1";
            this.colOwnerName1.OptionsColumn.ReadOnly = true;
            this.colOwnerName1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOwnerName1.Visible = true;
            this.colOwnerName1.VisibleIndex = 6;
            this.colOwnerName1.Width = 64;
            // 
            // colПлательщикСчет
            // 
            this.colПлательщикСчет.FieldName = "ПлательщикСчет";
            this.colПлательщикСчет.Name = "colПлательщикСчет";
            // 
            // colПолучательБанк
            // 
            this.colПолучательБанк.FieldName = "ПолучательБанк";
            this.colПолучательБанк.Name = "colПолучательБанк";
            this.colПолучательБанк.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colПолучательКомпанияИмя
            // 
            this.colПолучательКомпанияИмя.Caption = "Получатель Компания";
            this.colПолучательКомпанияИмя.FieldName = "ПолучательКомпанияИмя";
            this.colПолучательКомпанияИмя.Name = "colПолучательКомпанияИмя";
            this.colПолучательКомпанияИмя.OptionsColumn.ReadOnly = true;
            this.colПолучательКомпанияИмя.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colПолучательКомпанияИмя.Visible = true;
            this.colПолучательКомпанияИмя.VisibleIndex = 7;
            this.colПолучательКомпанияИмя.Width = 64;
            // 
            // colПолучательБанкИмя
            // 
            this.colПолучательБанкИмя.Caption = "Получатель Банк";
            this.colПолучательБанкИмя.FieldName = "ПолучательБанкИмя";
            this.colПолучательБанкИмя.Name = "colПолучательБанкИмя";
            this.colПолучательБанкИмя.OptionsColumn.ReadOnly = true;
            this.colПолучательБанкИмя.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colПолучательБанкИмя.Visible = true;
            this.colПолучательБанкИмя.VisibleIndex = 8;
            this.colПолучательБанкИмя.Width = 76;
            // 
            // colПолучательКомпания
            // 
            this.colПолучательКомпания.FieldName = "ПолучательКомпания";
            this.colПолучательКомпания.Name = "colПолучательКомпания";
            this.colПолучательКомпания.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // colГруппа2
            // 
            this.colГруппа2.Caption = "Получатель Группа";
            this.colГруппа2.FieldName = "Группа2";
            this.colГруппа2.Name = "colГруппа2";
            this.colГруппа2.OptionsColumn.ReadOnly = true;
            this.colГруппа2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colГруппа2.Visible = true;
            this.colГруппа2.VisibleIndex = 9;
            // 
            // colOwnerName2
            // 
            this.colOwnerName2.Caption = "Получатель Принадлежность";
            this.colOwnerName2.FieldName = "OwnerName2";
            this.colOwnerName2.Name = "colOwnerName2";
            this.colOwnerName2.OptionsColumn.ReadOnly = true;
            this.colOwnerName2.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colOwnerName2.Visible = true;
            this.colOwnerName2.VisibleIndex = 10;
            this.colOwnerName2.Width = 37;
            // 
            // colПолучательСчет
            // 
            this.colПолучательСчет.FieldName = "ПолучательСчет";
            this.colПолучательСчет.Name = "colПолучательСчет";
            // 
            // colНазначениеПлатежа1
            // 
            this.colНазначениеПлатежа1.FieldName = "НазначениеПлатежа";
            this.colНазначениеПлатежа1.Name = "colНазначениеПлатежа1";
            this.colНазначениеПлатежа1.OptionsColumn.ReadOnly = true;
            this.colНазначениеПлатежа1.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colНазначениеПлатежа1.Visible = true;
            this.colНазначениеПлатежа1.VisibleIndex = 12;
            this.colНазначениеПлатежа1.Width = 101;
            // 
            // colНаш
            // 
            this.colНаш.FieldName = "Наш";
            this.colНаш.Name = "colНаш";
            // 
            // colColor21
            // 
            this.colColor21.FieldName = "Color2";
            this.colColor21.Name = "colColor21";
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "gridColumn21";
            this.gridColumn21.Name = "gridColumn21";
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "gridColumn22";
            this.gridColumn22.Name = "gridColumn22";
            // 
            // colActionPlanDate
            // 
            this.colActionPlanDate.Caption = "План Дата Выдачи";
            this.colActionPlanDate.DisplayFormat.FormatString = "d";
            this.colActionPlanDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colActionPlanDate.FieldName = "ПланДата2";
            this.colActionPlanDate.Name = "colActionPlanDate";
            // 
            // colActionPayDate
            // 
            this.colActionPayDate.Caption = "Дата Выдачи";
            this.colActionPayDate.DisplayFormat.FormatString = "d";
            this.colActionPayDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colActionPayDate.FieldName = "ActionPayDate";
            this.colActionPayDate.Name = "colActionPayDate";
            // 
            // colActionPrecent
            // 
            this.colActionPrecent.FieldName = "ActionPrecent";
            this.colActionPrecent.Name = "colActionPrecent";
            // 
            // colActionSum
            // 
            this.colActionSum.Caption = "Сумма Выдачи";
            this.colActionSum.DisplayFormat.FormatString = "c2";
            this.colActionSum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colActionSum.FieldName = "ActionSum";
            this.colActionSum.Name = "colActionSum";
            // 
            // gridPlanSum
            // 
            this.gridPlanSum.Caption = "План Сумма Выдачи";
            this.gridPlanSum.DisplayFormat.FormatString = "c2";
            this.gridPlanSum.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridPlanSum.FieldName = "ПланВыдача";
            this.gridPlanSum.Name = "gridPlanSum";
            this.gridPlanSum.OptionsColumn.ReadOnly = true;
            this.gridPlanSum.UnboundExpression = "[Сумма] - [Сумма] / 100 * [PercentValue]";
            this.gridPlanSum.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.gridPlanSum.Visible = true;
            this.gridPlanSum.VisibleIndex = 0;
            // 
            // colActionDebt
            // 
            this.colActionDebt.Caption = "Долг";
            this.colActionDebt.DisplayFormat.FormatString = "c2";
            this.colActionDebt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colActionDebt.FieldName = "Долг";
            this.colActionDebt.Name = "colActionDebt";
            this.colActionDebt.UnboundExpression = "[gridPlanSum] - [ActionSum]";
            this.colActionDebt.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            this.colActionDebt.Visible = true;
            this.colActionDebt.VisibleIndex = 13;
            // 
            // colActionComment
            // 
            this.colActionComment.Caption = "Примечание";
            this.colActionComment.FieldName = "ActionComment";
            this.colActionComment.Name = "colActionComment";
            // 
            // colPercentValue1
            // 
            this.colPercentValue1.Caption = "Процент";
            this.colPercentValue1.FieldName = "Процент";
            this.colPercentValue1.Name = "colPercentValue1";
            // 
            // colActionCurDebt
            // 
            this.colActionCurDebt.Caption = "Долг Общий";
            this.colActionCurDebt.DisplayFormat.FormatString = "c2";
            this.colActionCurDebt.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colActionCurDebt.FieldName = "ТекущийДолг";
            this.colActionCurDebt.Name = "colActionCurDebt";
            this.colActionCurDebt.UnboundType = DevExpress.Data.UnboundColumnType.Decimal;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Общий Приход";
            this.gridColumn23.DisplayFormat.FormatString = "c2";
            this.gridColumn23.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn23.FieldName = "ОбщийПриход";
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 14;
            // 
            // colOwnerId2
            // 
            this.colOwnerId2.FieldName = "OwnerId";
            this.colOwnerId2.Name = "colOwnerId2";
            // 
            // colOwnerColor2
            // 
            this.colOwnerColor2.FieldName = "OwnerColor2";
            this.colOwnerColor2.Name = "colOwnerColor2";
            // 
            // gridView12
            // 
            this.gridView12.GridControl = this.grid;
            this.gridView12.Name = "gridView12";
            // 
            // gridView14
            // 
            this.gridView14.GridControl = this.grid;
            this.gridView14.Name = "gridView14";
            // 
            // bindingSource17
            // 
            this.bindingSource17.DataSource = typeof(TableBI.Data.View_Direct);
            // 
            // bindingSource4
            // 
            this.bindingSource4.DataSource = typeof(TableBI.Data.Company);
            // 
            // bindingSource11
            // 
            this.bindingSource11.DataSource = typeof(TableBI.Data.TypeCompany);
            // 
            // bindingSource9
            // 
            this.bindingSource9.DataSource = typeof(TableBI.Data.Owner);
            // 
            // bindingSource7
            // 
            this.bindingSource7.DataSource = typeof(TableBI.Data.Group);
            // 
            // bindingSource8
            // 
            this.bindingSource8.DataSource = typeof(TableBI.Data.View_UHOD);
            // 
            // bindingSource6
            // 
            this.bindingSource6.DataSource = typeof(TableBI.Data.Bank);
            // 
            // bindingSource10
            // 
            this.bindingSource10.DataSource = typeof(TableBI.Data.Percent);
            // 
            // bindingSource5
            // 
            this.bindingSource5.DataSource = typeof(TableBI.Data.Company);
            this.bindingSource5.CurrentChanged += new System.EventHandler(this.bindingSource5_CurrentChanged);
            // 
            // bindingSource3
            // 
            this.bindingSource3.DataSource = typeof(TableBI.Data.Bank);
            // 
            // bindingSource2
            // 
            this.bindingSource2.DataSource = typeof(TableBI.Data.Company);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "txml | *.txml";
            this.saveFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog1_FileOk);
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            this.openFileDialog2.Filter = "txml | *.txml";
            this.openFileDialog2.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog2_FileOk);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.MaxItemId = 0;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(1362, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 535);
            this.barDockControlBottom.Size = new System.Drawing.Size(1362, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 535);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(1362, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 535);
            // 
            // barManager2
            // 
            this.barManager2.DockControls.Add(this.barDockControl1);
            this.barManager2.DockControls.Add(this.barDockControl2);
            this.barManager2.DockControls.Add(this.barDockControl3);
            this.barManager2.DockControls.Add(this.barDockControl4);
            this.barManager2.Form = this;
            this.barManager2.MaxItemId = 0;
            // 
            // barDockControl1
            // 
            this.barDockControl1.CausesValidation = false;
            this.barDockControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControl1.Location = new System.Drawing.Point(0, 0);
            this.barDockControl1.Size = new System.Drawing.Size(1362, 0);
            // 
            // barDockControl2
            // 
            this.barDockControl2.CausesValidation = false;
            this.barDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControl2.Location = new System.Drawing.Point(0, 535);
            this.barDockControl2.Size = new System.Drawing.Size(1362, 0);
            // 
            // barDockControl3
            // 
            this.barDockControl3.CausesValidation = false;
            this.barDockControl3.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControl3.Location = new System.Drawing.Point(0, 0);
            this.barDockControl3.Size = new System.Drawing.Size(0, 535);
            // 
            // barDockControl4
            // 
            this.barDockControl4.CausesValidation = false;
            this.barDockControl4.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControl4.Location = new System.Drawing.Point(1362, 0);
            this.barDockControl4.Size = new System.Drawing.Size(0, 535);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timerUpdateAll
            // 
            this.timerUpdateAll.Enabled = true;
            this.timerUpdateAll.Interval = 1200000;
            this.timerUpdateAll.Tick += new System.EventHandler(this.timerUpdateAll_Tick);
            // 
            // bindingSourceOstatkiPeriod
            // 
            this.bindingSourceOstatkiPeriod.DataSource = typeof(TableBI.Data.OstatokPeriod);
            // 
            // bindingSource15
            // 
            this.bindingSource15.DataSource = typeof(TableBI.Data.Account);
            // 
            // bindingSourceSection
            // 
            this.bindingSourceSection.DataSource = typeof(TableBI.Data.OstatkiSection);
            // 
            // bindingSource12
            // 
            this.bindingSource12.DataSource = typeof(TableBI.Data.Company);
            // 
            // bindingSource13
            // 
            this.bindingSource13.DataSource = typeof(TableBI.Data.Ostatki);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.xtraTabControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1356, 333);
            this.xtraTabPage2.Text = "Справочники";
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl2.Enabled = false;
            this.xtraTabControl2.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage3;
            this.xtraTabControl2.Size = new System.Drawing.Size(1356, 333);
            this.xtraTabControl2.TabIndex = 0;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage3,
            this.xtraTabPage6,
            this.xtraTabPage5,
            this.xtraTabPage8,
            this.xtraTabPage4,
            this.xtraTabPage13,
            this.xtraTabPage14});
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.gridControl1);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.Size = new System.Drawing.Size(1350, 305);
            this.xtraTabPage3.Text = "Компании";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.bindingSource4;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridViewCompany;
            this.gridControl1.MenuManager = this.ribbon;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEditCompany,
            this.repositoryItemLookUpEditOwner,
            this.repositoryItemLookUpEdit1,
            this.repositoryItemLookUpEdit2,
            this.repositoryItemCheckEdit2,
            this.repositoryItemCheckEdit3,
            this.repositoryItemCheckedComboBoxEdit1});
            this.gridControl1.Size = new System.Drawing.Size(1350, 305);
            this.gridControl1.TabIndex = 2;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewCompany,
            this.gridView9});
            // 
            // gridViewCompany
            // 
            this.gridViewCompany.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCompanyName1,
            this.colCompanyFizLizo1,
            this.colCompanyINN1,
            this.colCompanyOstatok1,
            this.colGroupId3,
            this.colOwnerId3,
            this.colTypeCompanyId3,
            this.colOstatokPeriodId1});
            this.gridViewCompany.GridControl = this.gridControl1;
            this.gridViewCompany.Name = "gridViewCompany";
            this.gridViewCompany.OptionsFind.AlwaysVisible = true;
            this.gridViewCompany.HiddenEditor += new System.EventHandler(this.gridViewCompany_HiddenEditor);
            this.gridViewCompany.ShownEditor += new System.EventHandler(this.gridViewCompany_ShownEditor);
            this.gridViewCompany.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewCompany_RowUpdated);
            this.gridViewCompany.DoubleClick += new System.EventHandler(this.gridView13_DoubleClick);
            // 
            // colCompanyName1
            // 
            this.colCompanyName1.Caption = "Наименование";
            this.colCompanyName1.FieldName = "CompanyName";
            this.colCompanyName1.Name = "colCompanyName1";
            this.colCompanyName1.Visible = true;
            this.colCompanyName1.VisibleIndex = 0;
            // 
            // colCompanyFizLizo1
            // 
            this.colCompanyFizLizo1.Caption = "Физ. лицо";
            this.colCompanyFizLizo1.ColumnEdit = this.repositoryItemCheckEdit2;
            this.colCompanyFizLizo1.FieldName = "CompanyFizLizo";
            this.colCompanyFizLizo1.Name = "colCompanyFizLizo1";
            this.colCompanyFizLizo1.Visible = true;
            this.colCompanyFizLizo1.VisibleIndex = 1;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            this.repositoryItemCheckEdit2.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // colCompanyINN1
            // 
            this.colCompanyINN1.Caption = "ИНН";
            this.colCompanyINN1.FieldName = "CompanyINN";
            this.colCompanyINN1.Name = "colCompanyINN1";
            this.colCompanyINN1.Visible = true;
            this.colCompanyINN1.VisibleIndex = 2;
            // 
            // colCompanyOstatok1
            // 
            this.colCompanyOstatok1.Caption = "Остатки по р/с";
            this.colCompanyOstatok1.ColumnEdit = this.repositoryItemCheckEdit3;
            this.colCompanyOstatok1.FieldName = "CompanyOstatok";
            this.colCompanyOstatok1.Name = "colCompanyOstatok1";
            this.colCompanyOstatok1.Visible = true;
            this.colCompanyOstatok1.VisibleIndex = 6;
            // 
            // repositoryItemCheckEdit3
            // 
            this.repositoryItemCheckEdit3.AutoHeight = false;
            this.repositoryItemCheckEdit3.Name = "repositoryItemCheckEdit3";
            this.repositoryItemCheckEdit3.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // colGroupId3
            // 
            this.colGroupId3.Caption = "Группа";
            this.colGroupId3.ColumnEdit = this.repositoryItemLookUpEditCompany;
            this.colGroupId3.FieldName = "GroupId";
            this.colGroupId3.Name = "colGroupId3";
            this.colGroupId3.Visible = true;
            this.colGroupId3.VisibleIndex = 3;
            // 
            // repositoryItemLookUpEditCompany
            // 
            this.repositoryItemLookUpEditCompany.AutoHeight = false;
            this.repositoryItemLookUpEditCompany.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo, "Наименование", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, null, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, "", null, null, true)});
            this.repositoryItemLookUpEditCompany.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GroupName", "Наименование")});
            this.repositoryItemLookUpEditCompany.DataSource = this.bindingSource7;
            this.repositoryItemLookUpEditCompany.DisplayMember = "GroupName";
            this.repositoryItemLookUpEditCompany.Name = "repositoryItemLookUpEditCompany";
            this.repositoryItemLookUpEditCompany.ValueMember = "GroupId";
            // 
            // colOwnerId3
            // 
            this.colOwnerId3.Caption = "Принадлежность";
            this.colOwnerId3.ColumnEdit = this.repositoryItemLookUpEditOwner;
            this.colOwnerId3.FieldName = "OwnerId";
            this.colOwnerId3.Name = "colOwnerId3";
            this.colOwnerId3.Visible = true;
            this.colOwnerId3.VisibleIndex = 4;
            // 
            // repositoryItemLookUpEditOwner
            // 
            this.repositoryItemLookUpEditOwner.AutoHeight = false;
            this.repositoryItemLookUpEditOwner.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEditOwner.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("OwnerName", "Наименование")});
            this.repositoryItemLookUpEditOwner.DataSource = this.bindingSource9;
            this.repositoryItemLookUpEditOwner.DisplayMember = "OwnerName";
            this.repositoryItemLookUpEditOwner.Name = "repositoryItemLookUpEditOwner";
            this.repositoryItemLookUpEditOwner.ValueMember = "OwnerId";
            // 
            // colTypeCompanyId3
            // 
            this.colTypeCompanyId3.Caption = "Тип компании";
            this.colTypeCompanyId3.ColumnEdit = this.repositoryItemLookUpEdit1;
            this.colTypeCompanyId3.FieldName = "TypeCompanyId";
            this.colTypeCompanyId3.Name = "colTypeCompanyId3";
            this.colTypeCompanyId3.Visible = true;
            this.colTypeCompanyId3.VisibleIndex = 5;
            // 
            // repositoryItemLookUpEdit1
            // 
            this.repositoryItemLookUpEdit1.AutoHeight = false;
            this.repositoryItemLookUpEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit1.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TypeCompanyName", "Наименование")});
            this.repositoryItemLookUpEdit1.DataSource = this.bindingSource11;
            this.repositoryItemLookUpEdit1.DisplayMember = "TypeCompanyName";
            this.repositoryItemLookUpEdit1.Name = "repositoryItemLookUpEdit1";
            this.repositoryItemLookUpEdit1.ValueMember = "TypeCompanyId";
            // 
            // colOstatokPeriodId1
            // 
            this.colOstatokPeriodId1.Caption = "Период остатки";
            this.colOstatokPeriodId1.ColumnEdit = this.repositoryItemCheckedComboBoxEdit1;
            this.colOstatokPeriodId1.FieldName = "BalanceDays";
            this.colOstatokPeriodId1.Name = "colOstatokPeriodId1";
            this.colOstatokPeriodId1.Visible = true;
            this.colOstatokPeriodId1.VisibleIndex = 7;
            // 
            // repositoryItemCheckedComboBoxEdit1
            // 
            this.repositoryItemCheckedComboBoxEdit1.AutoHeight = false;
            this.repositoryItemCheckedComboBoxEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemCheckedComboBoxEdit1.DataSource = this.bindingSourceOstatkiPeriod;
            this.repositoryItemCheckedComboBoxEdit1.DisplayMember = "OstatokPeriodName";
            this.repositoryItemCheckedComboBoxEdit1.Name = "repositoryItemCheckedComboBoxEdit1";
            this.repositoryItemCheckedComboBoxEdit1.SelectAllItemCaption = "Отметить все";
            this.repositoryItemCheckedComboBoxEdit1.ValueMember = "OstatokPeriodDay";
            // 
            // repositoryItemLookUpEdit2
            // 
            this.repositoryItemLookUpEdit2.AutoHeight = false;
            this.repositoryItemLookUpEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit2.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("OstatokPeriodName", "Наименование")});
            this.repositoryItemLookUpEdit2.DataSource = this.bindingSourceOstatkiPeriod;
            this.repositoryItemLookUpEdit2.DisplayMember = "OstatokPeriodName";
            this.repositoryItemLookUpEdit2.Name = "repositoryItemLookUpEdit2";
            this.repositoryItemLookUpEdit2.ValueMember = "OstatokPeriodId";
            // 
            // gridView9
            // 
            this.gridView9.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCompanyName,
            this.colCompanyFizLizo,
            this.colCompanyINN,
            this.colGroupId2,
            this.colOwnerId,
            this.colTypeCompanyId2,
            this.colCompanyOstatok,
            this.colOstatokPeriodId,
            this.colOwner1,
            this.colTypeCompany});
            this.gridView9.GridControl = this.gridControl1;
            this.gridView9.Name = "gridView9";
            // 
            // colCompanyName
            // 
            this.colCompanyName.Caption = "Наименование";
            this.colCompanyName.FieldName = "CompanyName";
            this.colCompanyName.Name = "colCompanyName";
            this.colCompanyName.Visible = true;
            this.colCompanyName.VisibleIndex = 0;
            // 
            // colCompanyFizLizo
            // 
            this.colCompanyFizLizo.Caption = "Физ. лицо";
            this.colCompanyFizLizo.FieldName = "CompanyFizLizo";
            this.colCompanyFizLizo.Name = "colCompanyFizLizo";
            this.colCompanyFizLizo.Visible = true;
            this.colCompanyFizLizo.VisibleIndex = 5;
            // 
            // colCompanyINN
            // 
            this.colCompanyINN.Caption = "ИНН";
            this.colCompanyINN.FieldName = "CompanyINN";
            this.colCompanyINN.Name = "colCompanyINN";
            this.colCompanyINN.Visible = true;
            this.colCompanyINN.VisibleIndex = 3;
            // 
            // colGroupId2
            // 
            this.colGroupId2.Caption = "Группа";
            this.colGroupId2.FieldName = "GroupId";
            this.colGroupId2.Name = "colGroupId2";
            this.colGroupId2.Visible = true;
            this.colGroupId2.VisibleIndex = 1;
            // 
            // colOwnerId
            // 
            this.colOwnerId.Caption = "Принадлежность";
            this.colOwnerId.FieldName = "OwnerId";
            this.colOwnerId.Name = "colOwnerId";
            this.colOwnerId.Visible = true;
            this.colOwnerId.VisibleIndex = 2;
            // 
            // colTypeCompanyId2
            // 
            this.colTypeCompanyId2.FieldName = "TypeCompanyId";
            this.colTypeCompanyId2.Name = "colTypeCompanyId2";
            this.colTypeCompanyId2.Visible = true;
            this.colTypeCompanyId2.VisibleIndex = 4;
            // 
            // colCompanyOstatok
            // 
            this.colCompanyOstatok.FieldName = "CompanyOstatok";
            this.colCompanyOstatok.Name = "colCompanyOstatok";
            this.colCompanyOstatok.Visible = true;
            this.colCompanyOstatok.VisibleIndex = 6;
            // 
            // colOstatokPeriodId
            // 
            this.colOstatokPeriodId.FieldName = "OstatokPeriodId";
            this.colOstatokPeriodId.Name = "colOstatokPeriodId";
            this.colOstatokPeriodId.Visible = true;
            this.colOstatokPeriodId.VisibleIndex = 7;
            // 
            // colOwner1
            // 
            this.colOwner1.FieldName = "Owner";
            this.colOwner1.Name = "colOwner1";
            this.colOwner1.Visible = true;
            this.colOwner1.VisibleIndex = 8;
            // 
            // colTypeCompany
            // 
            this.colTypeCompany.FieldName = "TypeCompany";
            this.colTypeCompany.Name = "colTypeCompany";
            this.colTypeCompany.Visible = true;
            this.colTypeCompany.VisibleIndex = 9;
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.gridControl3);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(1350, 305);
            this.xtraTabPage6.Text = "Группы";
            // 
            // gridControl3
            // 
            this.gridControl3.DataSource = this.bindingSource7;
            this.gridControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl3.Location = new System.Drawing.Point(0, 0);
            this.gridControl3.MainView = this.gridView4;
            this.gridControl3.MenuManager = this.ribbon;
            this.gridControl3.Name = "gridControl3";
            this.gridControl3.Size = new System.Drawing.Size(1350, 305);
            this.gridControl3.TabIndex = 0;
            this.gridControl3.UseEmbeddedNavigator = true;
            this.gridControl3.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView4});
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colGroupId,
            this.colGroupName});
            this.gridView4.GridControl = this.gridControl3;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridView4.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView4_RowUpdated);
            // 
            // colGroupId
            // 
            this.colGroupId.FieldName = "GroupId";
            this.colGroupId.Name = "colGroupId";
            // 
            // colGroupName
            // 
            this.colGroupName.Caption = "Группа";
            this.colGroupName.FieldName = "GroupName";
            this.colGroupName.Name = "colGroupName";
            this.colGroupName.Visible = true;
            this.colGroupName.VisibleIndex = 0;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.xtraTabControl1);
            this.xtraTabPage5.Controls.Add(this.gridControl5);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(1350, 305);
            this.xtraTabPage5.Text = "Принадлежность";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(596, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage9;
            this.xtraTabControl1.Size = new System.Drawing.Size(754, 305);
            this.xtraTabControl1.TabIndex = 2;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage9,
            this.xtraTabPage10});
            // 
            // xtraTabPage9
            // 
            this.xtraTabPage9.Controls.Add(this.gridPercent);
            this.xtraTabPage9.Name = "xtraTabPage9";
            this.xtraTabPage9.Size = new System.Drawing.Size(748, 277);
            this.xtraTabPage9.Text = "Приход";
            // 
            // gridPercent
            // 
            this.gridPercent.DataSource = this.bindingSource10;
            this.gridPercent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridPercent.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.gridPercent.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridPercent_EmbeddedNavigator_ButtonClick);
            this.gridPercent.EmbeddedNavigator.BindingContextChanged += new System.EventHandler(this.gridPercent_EmbeddedNavigator_BindingContextChanged);
            this.gridPercent.EmbeddedNavigator.Click += new System.EventHandler(this.gridPercent_EmbeddedNavigator_Click);
            this.gridPercent.EmbeddedNavigator.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridPercent_EmbeddedNavigator_MouseUp);
            this.gridPercent.Location = new System.Drawing.Point(0, 0);
            this.gridPercent.MainView = this.gridViewPercent;
            this.gridPercent.MenuManager = this.ribbon;
            this.gridPercent.Name = "gridPercent";
            this.gridPercent.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemColorEdit3,
            this.repositoryItemLookUpEdit6});
            this.gridPercent.Size = new System.Drawing.Size(748, 277);
            this.gridPercent.TabIndex = 3;
            this.gridPercent.UseEmbeddedNavigator = true;
            this.gridPercent.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPercent});
            // 
            // gridViewPercent
            // 
            this.gridViewPercent.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colPercentId,
            this.colOwnerId1,
            this.colPrecentDate,
            this.colPercentDateEnd,
            this.colPercentValue,
            this.colPercentDayInc,
            this.colOwner,
            this.colPercentNdsValue,
            this.colPercentNdsDayInc,
            this.colTypeCompanyId1,
            this.colPercentPrihod});
            this.gridViewPercent.GridControl = this.gridPercent;
            this.gridViewPercent.Name = "gridViewPercent";
            this.gridViewPercent.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridViewPercent.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewPercent.OptionsView.ShowGroupPanel = false;
            this.gridViewPercent.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridViewPercent_InitNewRow);
            this.gridViewPercent.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewPercent_ValidateRow);
            this.gridViewPercent.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewPercent_RowUpdated);
            // 
            // colPercentId
            // 
            this.colPercentId.FieldName = "PercentId";
            this.colPercentId.Name = "colPercentId";
            // 
            // colOwnerId1
            // 
            this.colOwnerId1.FieldName = "OwnerId";
            this.colOwnerId1.Name = "colOwnerId1";
            // 
            // colPrecentDate
            // 
            this.colPrecentDate.Caption = "Дата начало";
            this.colPrecentDate.FieldName = "PrecentDate";
            this.colPrecentDate.Name = "colPrecentDate";
            this.colPrecentDate.Visible = true;
            this.colPrecentDate.VisibleIndex = 0;
            // 
            // colPercentDateEnd
            // 
            this.colPercentDateEnd.Caption = "Дата конец";
            this.colPercentDateEnd.FieldName = "PercentDateEnd";
            this.colPercentDateEnd.Name = "colPercentDateEnd";
            this.colPercentDateEnd.Visible = true;
            this.colPercentDateEnd.VisibleIndex = 1;
            // 
            // colPercentValue
            // 
            this.colPercentValue.AppearanceCell.Options.UseTextOptions = true;
            this.colPercentValue.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.colPercentValue.Caption = "Процент";
            this.colPercentValue.FieldName = "PercentValue";
            this.colPercentValue.Name = "colPercentValue";
            this.colPercentValue.Visible = true;
            this.colPercentValue.VisibleIndex = 2;
            // 
            // colPercentDayInc
            // 
            this.colPercentDayInc.Caption = "Срок выдачи";
            this.colPercentDayInc.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPercentDayInc.FieldName = "PercentDayInc";
            this.colPercentDayInc.Name = "colPercentDayInc";
            this.colPercentDayInc.Visible = true;
            this.colPercentDayInc.VisibleIndex = 3;
            // 
            // colOwner
            // 
            this.colOwner.FieldName = "Owner";
            this.colOwner.Name = "colOwner";
            // 
            // colPercentNdsValue
            // 
            this.colPercentNdsValue.Caption = "Процент НДС";
            this.colPercentNdsValue.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPercentNdsValue.FieldName = "PercentNdsValue";
            this.colPercentNdsValue.Name = "colPercentNdsValue";
            this.colPercentNdsValue.Visible = true;
            this.colPercentNdsValue.VisibleIndex = 4;
            // 
            // colPercentNdsDayInc
            // 
            this.colPercentNdsDayInc.Caption = "Срок выдачи НДС";
            this.colPercentNdsDayInc.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colPercentNdsDayInc.FieldName = "PercentNdsDayInc";
            this.colPercentNdsDayInc.Name = "colPercentNdsDayInc";
            this.colPercentNdsDayInc.Visible = true;
            this.colPercentNdsDayInc.VisibleIndex = 5;
            // 
            // colTypeCompanyId1
            // 
            this.colTypeCompanyId1.Caption = "Тип компании";
            this.colTypeCompanyId1.ColumnEdit = this.repositoryItemLookUpEdit6;
            this.colTypeCompanyId1.FieldName = "TypeCompanyId";
            this.colTypeCompanyId1.Name = "colTypeCompanyId1";
            this.colTypeCompanyId1.Visible = true;
            this.colTypeCompanyId1.VisibleIndex = 6;
            // 
            // repositoryItemLookUpEdit6
            // 
            this.repositoryItemLookUpEdit6.AutoHeight = false;
            this.repositoryItemLookUpEdit6.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit6.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TypeCompanyName", "Тип")});
            this.repositoryItemLookUpEdit6.DataSource = this.bindingSource11;
            this.repositoryItemLookUpEdit6.DisplayMember = "TypeCompanyName";
            this.repositoryItemLookUpEdit6.Name = "repositoryItemLookUpEdit6";
            this.repositoryItemLookUpEdit6.ValueMember = "TypeCompanyId";
            // 
            // colPercentPrihod
            // 
            this.colPercentPrihod.FieldName = "PercentPrihod";
            this.colPercentPrihod.Name = "colPercentPrihod";
            // 
            // repositoryItemColorEdit3
            // 
            this.repositoryItemColorEdit3.AutoHeight = false;
            this.repositoryItemColorEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit3.Name = "repositoryItemColorEdit3";
            this.repositoryItemColorEdit3.StoreColorAsInteger = true;
            // 
            // xtraTabPage10
            // 
            this.xtraTabPage10.Controls.Add(this.gridControl7);
            this.xtraTabPage10.Name = "xtraTabPage10";
            this.xtraTabPage10.Size = new System.Drawing.Size(748, 277);
            this.xtraTabPage10.Text = "Уход";
            // 
            // gridControl7
            // 
            this.gridControl7.DataSource = this.bindingSource10;
            this.gridControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl7.EmbeddedNavigator.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.Office2003;
            this.gridControl7.EmbeddedNavigator.ButtonClick += new DevExpress.XtraEditors.NavigatorButtonClickEventHandler(this.gridPercent_EmbeddedNavigator_ButtonClick);
            this.gridControl7.EmbeddedNavigator.BindingContextChanged += new System.EventHandler(this.gridPercent_EmbeddedNavigator_BindingContextChanged);
            this.gridControl7.EmbeddedNavigator.Click += new System.EventHandler(this.gridPercent_EmbeddedNavigator_Click);
            this.gridControl7.EmbeddedNavigator.MouseUp += new System.Windows.Forms.MouseEventHandler(this.gridPercent_EmbeddedNavigator_MouseUp);
            this.gridControl7.Location = new System.Drawing.Point(0, 0);
            this.gridControl7.MainView = this.gridViewPercentUhod;
            this.gridControl7.MenuManager = this.ribbon;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemColorEdit4,
            this.repositoryItemLookUpEdit7});
            this.gridControl7.Size = new System.Drawing.Size(748, 277);
            this.gridControl7.TabIndex = 4;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPercentUhod,
            this.gridView2});
            // 
            // gridViewPercentUhod
            // 
            this.gridViewPercentUhod.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn24,
            this.gridColumn25,
            this.gridColumn26,
            this.gridColumn27,
            this.gridColumn28,
            this.gridColumn29,
            this.gridColumn30,
            this.gridColumn31,
            this.gridColumn32,
            this.gridColumn33,
            this.gridColumn34});
            this.gridViewPercentUhod.GridControl = this.gridControl7;
            this.gridViewPercentUhod.Name = "gridViewPercentUhod";
            this.gridViewPercentUhod.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridViewPercentUhod.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewPercentUhod.OptionsView.ShowGroupPanel = false;
            this.gridViewPercentUhod.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridViewPercent_InitNewRow);
            this.gridViewPercentUhod.RowDeleting += new DevExpress.Data.RowDeletingEventHandler(this.gridViewPercent_RowDeleting);
            this.gridViewPercentUhod.ValidateRow += new DevExpress.XtraGrid.Views.Base.ValidateRowEventHandler(this.gridViewPercent_ValidateRow);
            this.gridViewPercentUhod.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewPercent_RowUpdated);
            // 
            // gridColumn24
            // 
            this.gridColumn24.FieldName = "PercentId";
            this.gridColumn24.Name = "gridColumn24";
            // 
            // gridColumn25
            // 
            this.gridColumn25.FieldName = "OwnerId";
            this.gridColumn25.Name = "gridColumn25";
            // 
            // gridColumn26
            // 
            this.gridColumn26.Caption = "Дата начало";
            this.gridColumn26.FieldName = "PrecentDate";
            this.gridColumn26.Name = "gridColumn26";
            this.gridColumn26.Visible = true;
            this.gridColumn26.VisibleIndex = 0;
            // 
            // gridColumn27
            // 
            this.gridColumn27.Caption = "Дата конец";
            this.gridColumn27.FieldName = "PercentDateEnd";
            this.gridColumn27.Name = "gridColumn27";
            this.gridColumn27.Visible = true;
            this.gridColumn27.VisibleIndex = 1;
            // 
            // gridColumn28
            // 
            this.gridColumn28.AppearanceCell.Options.UseTextOptions = true;
            this.gridColumn28.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.gridColumn28.Caption = "Процент";
            this.gridColumn28.FieldName = "PercentValue";
            this.gridColumn28.Name = "gridColumn28";
            this.gridColumn28.Visible = true;
            this.gridColumn28.VisibleIndex = 2;
            // 
            // gridColumn29
            // 
            this.gridColumn29.Caption = "Срок выдачи";
            this.gridColumn29.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn29.FieldName = "PercentDayInc";
            this.gridColumn29.Name = "gridColumn29";
            this.gridColumn29.Visible = true;
            this.gridColumn29.VisibleIndex = 3;
            // 
            // gridColumn30
            // 
            this.gridColumn30.FieldName = "Owner";
            this.gridColumn30.Name = "gridColumn30";
            // 
            // gridColumn31
            // 
            this.gridColumn31.Caption = "Процент НДС";
            this.gridColumn31.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn31.FieldName = "PercentNdsValue";
            this.gridColumn31.Name = "gridColumn31";
            this.gridColumn31.Visible = true;
            this.gridColumn31.VisibleIndex = 4;
            // 
            // gridColumn32
            // 
            this.gridColumn32.Caption = "Срок выдачи НДС";
            this.gridColumn32.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn32.FieldName = "PercentNdsDayInc";
            this.gridColumn32.Name = "gridColumn32";
            this.gridColumn32.Visible = true;
            this.gridColumn32.VisibleIndex = 5;
            // 
            // gridColumn33
            // 
            this.gridColumn33.Caption = "Тип компании";
            this.gridColumn33.ColumnEdit = this.repositoryItemLookUpEdit7;
            this.gridColumn33.FieldName = "TypeCompanyId";
            this.gridColumn33.Name = "gridColumn33";
            this.gridColumn33.Visible = true;
            this.gridColumn33.VisibleIndex = 6;
            // 
            // repositoryItemLookUpEdit7
            // 
            this.repositoryItemLookUpEdit7.AutoHeight = false;
            this.repositoryItemLookUpEdit7.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit7.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TypeCompanyName", "Тип")});
            this.repositoryItemLookUpEdit7.DataSource = this.bindingSource11;
            this.repositoryItemLookUpEdit7.DisplayMember = "TypeCompanyName";
            this.repositoryItemLookUpEdit7.Name = "repositoryItemLookUpEdit7";
            this.repositoryItemLookUpEdit7.ValueMember = "TypeCompanyId";
            // 
            // gridColumn34
            // 
            this.gridColumn34.FieldName = "PercentPrihod";
            this.gridColumn34.Name = "gridColumn34";
            // 
            // repositoryItemColorEdit4
            // 
            this.repositoryItemColorEdit4.AutoHeight = false;
            this.repositoryItemColorEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit4.Name = "repositoryItemColorEdit4";
            this.repositoryItemColorEdit4.StoreColorAsInteger = true;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl7;
            this.gridView2.Name = "gridView2";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.bindingSource9;
            this.gridControl5.Dock = System.Windows.Forms.DockStyle.Left;
            this.gridControl5.Location = new System.Drawing.Point(0, 0);
            this.gridControl5.MainView = this.gridView6;
            this.gridControl5.MenuManager = this.ribbon;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemColorEdit2,
            this.repositoryItemLookUpEdit3});
            this.gridControl5.Size = new System.Drawing.Size(596, 305);
            this.gridControl5.TabIndex = 1;
            this.gridControl5.UseEmbeddedNavigator = true;
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView6});
            // 
            // gridView6
            // 
            this.gridView6.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOwnerName,
            this.colColor,
            this.colOwnerPlusDate,
            this.colGroupId1});
            this.gridView6.GridControl = this.gridControl5;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            this.gridView6.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridView6_InitNewRow);
            this.gridView6.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridView6_FocusedRowChanged);
            this.gridView6.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView6_RowUpdated);
            // 
            // colOwnerName
            // 
            this.colOwnerName.Caption = "Имя";
            this.colOwnerName.FieldName = "OwnerName";
            this.colOwnerName.Name = "colOwnerName";
            this.colOwnerName.Visible = true;
            this.colOwnerName.VisibleIndex = 0;
            this.colOwnerName.Width = 277;
            // 
            // colColor
            // 
            this.colColor.Caption = "Цвет";
            this.colColor.ColumnEdit = this.repositoryItemColorEdit2;
            this.colColor.FieldName = "Color";
            this.colColor.Name = "colColor";
            this.colColor.Visible = true;
            this.colColor.VisibleIndex = 1;
            this.colColor.Width = 301;
            // 
            // repositoryItemColorEdit2
            // 
            this.repositoryItemColorEdit2.AutoHeight = false;
            this.repositoryItemColorEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit2.Name = "repositoryItemColorEdit2";
            this.repositoryItemColorEdit2.StoreColorAsInteger = true;
            // 
            // colOwnerPlusDate
            // 
            this.colOwnerPlusDate.Caption = "Дни";
            this.colOwnerPlusDate.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colOwnerPlusDate.FieldName = "OwnerPlusDate";
            this.colOwnerPlusDate.Name = "colOwnerPlusDate";
            // 
            // colGroupId1
            // 
            this.colGroupId1.Caption = "Группа";
            this.colGroupId1.ColumnEdit = this.repositoryItemLookUpEdit3;
            this.colGroupId1.FieldName = "GroupId";
            this.colGroupId1.Name = "colGroupId1";
            // 
            // repositoryItemLookUpEdit3
            // 
            this.repositoryItemLookUpEdit3.AutoHeight = false;
            this.repositoryItemLookUpEdit3.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit3.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GroupName", "Группа")});
            this.repositoryItemLookUpEdit3.DataSource = this.bindingSource7;
            this.repositoryItemLookUpEdit3.DisplayMember = "GroupName";
            this.repositoryItemLookUpEdit3.Name = "repositoryItemLookUpEdit3";
            // 
            // xtraTabPage8
            // 
            this.xtraTabPage8.Controls.Add(this.gridControl6);
            this.xtraTabPage8.Name = "xtraTabPage8";
            this.xtraTabPage8.Size = new System.Drawing.Size(1350, 305);
            this.xtraTabPage8.Text = "Тип компании";
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.bindingSource11;
            this.gridControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl6.Location = new System.Drawing.Point(0, 0);
            this.gridControl6.MainView = this.gridView7;
            this.gridControl6.MenuManager = this.ribbon;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.Size = new System.Drawing.Size(1350, 305);
            this.gridControl6.TabIndex = 1;
            this.gridControl6.UseEmbeddedNavigator = true;
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView7});
            // 
            // gridView7
            // 
            this.gridView7.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colTypeCompanyId,
            this.colTypeCompanyName});
            this.gridView7.GridControl = this.gridControl6;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridView7.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView7_RowUpdated);
            // 
            // colTypeCompanyId
            // 
            this.colTypeCompanyId.FieldName = "TypeCompanyId";
            this.colTypeCompanyId.Name = "colTypeCompanyId";
            // 
            // colTypeCompanyName
            // 
            this.colTypeCompanyName.Caption = "Тип";
            this.colTypeCompanyName.FieldName = "TypeCompanyName";
            this.colTypeCompanyName.Name = "colTypeCompanyName";
            this.colTypeCompanyName.Visible = true;
            this.colTypeCompanyName.VisibleIndex = 0;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.gridControl2);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(1350, 305);
            this.xtraTabPage4.Text = "Банки";
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.bindingSource6;
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.gridView3;
            this.gridControl2.MenuManager = this.ribbon;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(1350, 305);
            this.gridControl2.TabIndex = 0;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colBankName,
            this.colBankAbr,
            this.colBankBik});
            this.gridView3.GridControl = this.gridControl2;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsFind.AlwaysVisible = true;
            this.gridView3.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView3_RowUpdated);
            // 
            // colBankName
            // 
            this.colBankName.Caption = "Банк";
            this.colBankName.FieldName = "BankName";
            this.colBankName.Name = "colBankName";
            this.colBankName.Visible = true;
            this.colBankName.VisibleIndex = 0;
            // 
            // colBankAbr
            // 
            this.colBankAbr.Caption = "Сокращение";
            this.colBankAbr.FieldName = "BankAbr";
            this.colBankAbr.Name = "colBankAbr";
            // 
            // colBankBik
            // 
            this.colBankBik.Caption = "БИК";
            this.colBankBik.FieldName = "BankBik";
            this.colBankBik.Name = "colBankBik";
            this.colBankBik.Visible = true;
            this.colBankBik.VisibleIndex = 1;
            // 
            // xtraTabPage13
            // 
            this.xtraTabPage13.Controls.Add(this.gridControlAccount);
            this.xtraTabPage13.Name = "xtraTabPage13";
            this.xtraTabPage13.Size = new System.Drawing.Size(1350, 305);
            this.xtraTabPage13.Text = "Счета";
            // 
            // gridControlAccount
            // 
            this.gridControlAccount.DataSource = this.bindingSource15;
            this.gridControlAccount.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlAccount.Location = new System.Drawing.Point(0, 0);
            this.gridControlAccount.MainView = this.gridViewAccount;
            this.gridControlAccount.MenuManager = this.ribbon;
            this.gridControlAccount.Name = "gridControlAccount";
            this.gridControlAccount.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemLookUpEdit4,
            this.repositoryItemLookUpEdit5,
            this.repositoryItemCheckEdit4,
            this.repositoryItemCheckEdit5});
            this.gridControlAccount.Size = new System.Drawing.Size(1350, 305);
            this.gridControlAccount.TabIndex = 1;
            this.gridControlAccount.UseEmbeddedNavigator = true;
            this.gridControlAccount.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewAccount});
            // 
            // gridViewAccount
            // 
            this.gridViewAccount.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colAccount1,
            this.colCompanyId,
            this.colBankId,
            this.colAccountActive,
            this.colAccountBlock});
            this.gridViewAccount.GridControl = this.gridControlAccount;
            this.gridViewAccount.Name = "gridViewAccount";
            this.gridViewAccount.OptionsFind.AlwaysVisible = true;
            // 
            // colAccount1
            // 
            this.colAccount1.Caption = "Счет";
            this.colAccount1.FieldName = "Account1";
            this.colAccount1.Name = "colAccount1";
            this.colAccount1.Visible = true;
            this.colAccount1.VisibleIndex = 0;
            this.colAccount1.Width = 266;
            // 
            // colCompanyId
            // 
            this.colCompanyId.Caption = "Компания";
            this.colCompanyId.ColumnEdit = this.repositoryItemLookUpEdit5;
            this.colCompanyId.FieldName = "CompanyId";
            this.colCompanyId.Name = "colCompanyId";
            this.colCompanyId.Visible = true;
            this.colCompanyId.VisibleIndex = 4;
            this.colCompanyId.Width = 384;
            // 
            // repositoryItemLookUpEdit5
            // 
            this.repositoryItemLookUpEdit5.AutoHeight = false;
            this.repositoryItemLookUpEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit5.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CompanyName", "Наименование"),
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CompanyINN", "ИНН")});
            this.repositoryItemLookUpEdit5.DataSource = this.bindingSource4;
            this.repositoryItemLookUpEdit5.DisplayMember = "CompanyName";
            this.repositoryItemLookUpEdit5.Name = "repositoryItemLookUpEdit5";
            this.repositoryItemLookUpEdit5.ValueMember = "CompanyId";
            // 
            // colBankId
            // 
            this.colBankId.Caption = "Банк";
            this.colBankId.ColumnEdit = this.repositoryItemLookUpEdit4;
            this.colBankId.FieldName = "BankId";
            this.colBankId.Name = "colBankId";
            this.colBankId.Visible = true;
            this.colBankId.VisibleIndex = 3;
            this.colBankId.Width = 377;
            // 
            // repositoryItemLookUpEdit4
            // 
            this.repositoryItemLookUpEdit4.AutoHeight = false;
            this.repositoryItemLookUpEdit4.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit4.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BankName", "Наименование")});
            this.repositoryItemLookUpEdit4.DataSource = this.bindingSource3;
            this.repositoryItemLookUpEdit4.DisplayMember = "BankName";
            this.repositoryItemLookUpEdit4.Name = "repositoryItemLookUpEdit4";
            this.repositoryItemLookUpEdit4.ValueMember = "BankId";
            // 
            // colAccountActive
            // 
            this.colAccountActive.Caption = "Активен";
            this.colAccountActive.ColumnEdit = this.repositoryItemCheckEdit4;
            this.colAccountActive.FieldName = "AccountActive";
            this.colAccountActive.Name = "colAccountActive";
            this.colAccountActive.Visible = true;
            this.colAccountActive.VisibleIndex = 1;
            this.colAccountActive.Width = 128;
            // 
            // repositoryItemCheckEdit4
            // 
            this.repositoryItemCheckEdit4.AutoHeight = false;
            this.repositoryItemCheckEdit4.Name = "repositoryItemCheckEdit4";
            this.repositoryItemCheckEdit4.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // colAccountBlock
            // 
            this.colAccountBlock.Caption = "Заблокирован";
            this.colAccountBlock.ColumnEdit = this.repositoryItemCheckEdit5;
            this.colAccountBlock.FieldName = "AccountBlock";
            this.colAccountBlock.Name = "colAccountBlock";
            this.colAccountBlock.Visible = true;
            this.colAccountBlock.VisibleIndex = 2;
            this.colAccountBlock.Width = 177;
            // 
            // repositoryItemCheckEdit5
            // 
            this.repositoryItemCheckEdit5.AutoHeight = false;
            this.repositoryItemCheckEdit5.Name = "repositoryItemCheckEdit5";
            this.repositoryItemCheckEdit5.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // xtraTabPage14
            // 
            this.xtraTabPage14.Controls.Add(this.gridControlOstatkiSectionDetail);
            this.xtraTabPage14.Controls.Add(this.gridControlSection);
            this.xtraTabPage14.Name = "xtraTabPage14";
            this.xtraTabPage14.Size = new System.Drawing.Size(1350, 305);
            this.xtraTabPage14.Text = "Темы";
            // 
            // gridControlOstatkiSectionDetail
            // 
            this.gridControlOstatkiSectionDetail.DataSource = this.bindingSource18;
            this.gridControlOstatkiSectionDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlOstatkiSectionDetail.Location = new System.Drawing.Point(351, 0);
            this.gridControlOstatkiSectionDetail.MainView = this.gridViewOstatkiSectionDetail;
            this.gridControlOstatkiSectionDetail.MenuManager = this.ribbon;
            this.gridControlOstatkiSectionDetail.Name = "gridControlOstatkiSectionDetail";
            this.gridControlOstatkiSectionDetail.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemColorEdit5,
            this.repositoryItemLookUpEdit13,
            this.repositoryItemLookUpEdit11,
            this.repositoryItemLookUpEdit12,
            this.repositoryItemLookUpEdit14,
            this.repositoryItemLookUpEdit15,
            this.repositoryItemLookUpEdit16});
            this.gridControlOstatkiSectionDetail.Size = new System.Drawing.Size(999, 305);
            this.gridControlOstatkiSectionDetail.TabIndex = 3;
            this.gridControlOstatkiSectionDetail.UseEmbeddedNavigator = true;
            this.gridControlOstatkiSectionDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewOstatkiSectionDetail});
            // 
            // bindingSource18
            // 
            this.bindingSource18.DataSource = typeof(TableBI.Data.OstatkiSectionDetail);
            // 
            // gridViewOstatkiSectionDetail
            // 
            this.gridViewOstatkiSectionDetail.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumnTypeCompany,
            this.gridColumnOwner,
            this.gridColumnGroup});
            this.gridViewOstatkiSectionDetail.GridControl = this.gridControlOstatkiSectionDetail;
            this.gridViewOstatkiSectionDetail.Name = "gridViewOstatkiSectionDetail";
            this.gridViewOstatkiSectionDetail.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridViewOstatkiSectionDetail.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewOstatkiSectionDetail.OptionsView.ShowGroupPanel = false;
            this.gridViewOstatkiSectionDetail.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gridViewOstatkiSectionDetail_InitNewRow);
            this.gridViewOstatkiSectionDetail.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView4_RowUpdated);
            // 
            // gridColumnTypeCompany
            // 
            this.gridColumnTypeCompany.Caption = "Тип компании";
            this.gridColumnTypeCompany.ColumnEdit = this.repositoryItemLookUpEdit15;
            this.gridColumnTypeCompany.FieldName = "TypeCompanyId";
            this.gridColumnTypeCompany.Name = "gridColumnTypeCompany";
            this.gridColumnTypeCompany.Visible = true;
            this.gridColumnTypeCompany.VisibleIndex = 0;
            // 
            // repositoryItemLookUpEdit15
            // 
            this.repositoryItemLookUpEdit15.AutoHeight = false;
            this.repositoryItemLookUpEdit15.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit15.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("TypeCompanyName", "Тип компании")});
            this.repositoryItemLookUpEdit15.DataSource = this.bindingSource11;
            this.repositoryItemLookUpEdit15.DisplayMember = "TypeCompanyName";
            this.repositoryItemLookUpEdit15.Name = "repositoryItemLookUpEdit15";
            this.repositoryItemLookUpEdit15.ValueMember = "TypeCompanyId";
            // 
            // gridColumnOwner
            // 
            this.gridColumnOwner.Caption = "Принадлежность";
            this.gridColumnOwner.ColumnEdit = this.repositoryItemLookUpEdit14;
            this.gridColumnOwner.FieldName = "OwnerId";
            this.gridColumnOwner.Name = "gridColumnOwner";
            this.gridColumnOwner.Visible = true;
            this.gridColumnOwner.VisibleIndex = 1;
            // 
            // repositoryItemLookUpEdit14
            // 
            this.repositoryItemLookUpEdit14.AutoHeight = false;
            this.repositoryItemLookUpEdit14.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit14.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("OwnerName", "Принадлежность")});
            this.repositoryItemLookUpEdit14.DataSource = this.bindingSource9;
            this.repositoryItemLookUpEdit14.DisplayMember = "OwnerName";
            this.repositoryItemLookUpEdit14.Name = "repositoryItemLookUpEdit14";
            this.repositoryItemLookUpEdit14.ValueMember = "OwnerId";
            // 
            // gridColumnGroup
            // 
            this.gridColumnGroup.Caption = "Группа";
            this.gridColumnGroup.ColumnEdit = this.repositoryItemLookUpEdit16;
            this.gridColumnGroup.FieldName = "GroupId";
            this.gridColumnGroup.Name = "gridColumnGroup";
            this.gridColumnGroup.Visible = true;
            this.gridColumnGroup.VisibleIndex = 2;
            // 
            // repositoryItemLookUpEdit16
            // 
            this.repositoryItemLookUpEdit16.AutoHeight = false;
            this.repositoryItemLookUpEdit16.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit16.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GroupName", "Группа")});
            this.repositoryItemLookUpEdit16.DataSource = this.bindingSource7;
            this.repositoryItemLookUpEdit16.DisplayMember = "GroupName";
            this.repositoryItemLookUpEdit16.Name = "repositoryItemLookUpEdit16";
            this.repositoryItemLookUpEdit16.ValueMember = "GroupId";
            // 
            // repositoryItemColorEdit5
            // 
            this.repositoryItemColorEdit5.AutoHeight = false;
            this.repositoryItemColorEdit5.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit5.Name = "repositoryItemColorEdit5";
            this.repositoryItemColorEdit5.StoreColorAsInteger = true;
            // 
            // repositoryItemLookUpEdit13
            // 
            this.repositoryItemLookUpEdit13.AutoHeight = false;
            this.repositoryItemLookUpEdit13.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit13.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GroupName", "Группа")});
            this.repositoryItemLookUpEdit13.DataSource = this.bindingSource7;
            this.repositoryItemLookUpEdit13.DisplayMember = "GroupName";
            this.repositoryItemLookUpEdit13.Name = "repositoryItemLookUpEdit13";
            // 
            // repositoryItemLookUpEdit11
            // 
            this.repositoryItemLookUpEdit11.AutoHeight = false;
            this.repositoryItemLookUpEdit11.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit11.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GroupName", "Наименование")});
            this.repositoryItemLookUpEdit11.DataSource = this.bindingSource7;
            this.repositoryItemLookUpEdit11.DisplayMember = "GroupName";
            this.repositoryItemLookUpEdit11.Name = "repositoryItemLookUpEdit11";
            this.repositoryItemLookUpEdit11.ValueMember = "GroupId";
            // 
            // repositoryItemLookUpEdit12
            // 
            this.repositoryItemLookUpEdit12.AutoHeight = false;
            this.repositoryItemLookUpEdit12.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit12.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("OwnerName", "Наименование")});
            this.repositoryItemLookUpEdit12.DataSource = this.bindingSource9;
            this.repositoryItemLookUpEdit12.DisplayMember = "OwnerName";
            this.repositoryItemLookUpEdit12.Name = "repositoryItemLookUpEdit12";
            this.repositoryItemLookUpEdit12.ValueMember = "OwnerId";
            // 
            // gridControlSection
            // 
            this.gridControlSection.DataSource = this.bindingSourceSection;
            this.gridControlSection.Dock = System.Windows.Forms.DockStyle.Left;
            this.gridControlSection.Location = new System.Drawing.Point(0, 0);
            this.gridControlSection.MainView = this.gridViewSection;
            this.gridControlSection.MenuManager = this.ribbon;
            this.gridControlSection.Name = "gridControlSection";
            this.gridControlSection.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemColorEdit1,
            this.repositoryItemLookUpEdit8,
            this.repositoryItemLookUpEdit9,
            this.repositoryItemLookUpEdit10});
            this.gridControlSection.Size = new System.Drawing.Size(351, 305);
            this.gridControlSection.TabIndex = 2;
            this.gridControlSection.UseEmbeddedNavigator = true;
            this.gridControlSection.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSection});
            // 
            // gridViewSection
            // 
            this.gridViewSection.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colOstatkiSectionName,
            this.colGroupId4,
            this.colOwnerId4});
            this.gridViewSection.GridControl = this.gridControlSection;
            this.gridViewSection.Name = "gridViewSection";
            this.gridViewSection.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridViewSection.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.gridViewSection.OptionsView.ShowGroupPanel = false;
            this.gridViewSection.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.gridViewSection_FocusedRowChanged);
            this.gridViewSection.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView4_RowUpdated);
            // 
            // colOstatkiSectionName
            // 
            this.colOstatkiSectionName.Caption = "Название темы";
            this.colOstatkiSectionName.FieldName = "OstatkiSectionName";
            this.colOstatkiSectionName.Name = "colOstatkiSectionName";
            this.colOstatkiSectionName.OptionsFilter.AllowFilter = false;
            this.colOstatkiSectionName.Visible = true;
            this.colOstatkiSectionName.VisibleIndex = 0;
            // 
            // colGroupId4
            // 
            this.colGroupId4.Caption = "Группа";
            this.colGroupId4.ColumnEdit = this.repositoryItemLookUpEdit9;
            this.colGroupId4.FieldName = "GroupId";
            this.colGroupId4.Name = "colGroupId4";
            // 
            // repositoryItemLookUpEdit9
            // 
            this.repositoryItemLookUpEdit9.AutoHeight = false;
            this.repositoryItemLookUpEdit9.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit9.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GroupName", "Наименование")});
            this.repositoryItemLookUpEdit9.DataSource = this.bindingSource7;
            this.repositoryItemLookUpEdit9.DisplayMember = "GroupName";
            this.repositoryItemLookUpEdit9.Name = "repositoryItemLookUpEdit9";
            this.repositoryItemLookUpEdit9.ValueMember = "GroupId";
            // 
            // colOwnerId4
            // 
            this.colOwnerId4.Caption = "Принадлежность";
            this.colOwnerId4.ColumnEdit = this.repositoryItemLookUpEdit10;
            this.colOwnerId4.FieldName = "OwnerId";
            this.colOwnerId4.Name = "colOwnerId4";
            // 
            // repositoryItemLookUpEdit10
            // 
            this.repositoryItemLookUpEdit10.AutoHeight = false;
            this.repositoryItemLookUpEdit10.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit10.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("OwnerName", "Наименование")});
            this.repositoryItemLookUpEdit10.DataSource = this.bindingSource9;
            this.repositoryItemLookUpEdit10.DisplayMember = "OwnerName";
            this.repositoryItemLookUpEdit10.Name = "repositoryItemLookUpEdit10";
            this.repositoryItemLookUpEdit10.ValueMember = "OwnerId";
            // 
            // repositoryItemColorEdit1
            // 
            this.repositoryItemColorEdit1.AutoHeight = false;
            this.repositoryItemColorEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemColorEdit1.Name = "repositoryItemColorEdit1";
            this.repositoryItemColorEdit1.StoreColorAsInteger = true;
            // 
            // repositoryItemLookUpEdit8
            // 
            this.repositoryItemLookUpEdit8.AutoHeight = false;
            this.repositoryItemLookUpEdit8.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemLookUpEdit8.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("GroupName", "Группа")});
            this.repositoryItemLookUpEdit8.DataSource = this.bindingSource7;
            this.repositoryItemLookUpEdit8.DisplayMember = "GroupName";
            this.repositoryItemLookUpEdit8.Name = "repositoryItemLookUpEdit8";
            // 
            // xtraTabPage12
            // 
            this.xtraTabPage12.Controls.Add(this.sbtnRefresh);
            this.xtraTabPage12.Controls.Add(this.sBtnOstatkiExcel);
            this.xtraTabPage12.Controls.Add(this.dateEditOstatki);
            this.xtraTabPage12.Controls.Add(this.lookUpEditSection);
            this.xtraTabPage12.Controls.Add(this.gridControlOstatki);
            this.xtraTabPage12.Name = "xtraTabPage12";
            this.xtraTabPage12.Size = new System.Drawing.Size(1356, 333);
            this.xtraTabPage12.Text = "Остатки";
            // 
            // sbtnRefresh
            // 
            this.sbtnRefresh.Image = ((System.Drawing.Image)(resources.GetObject("sbtnRefresh.Image")));
            this.sbtnRefresh.Location = new System.Drawing.Point(770, 12);
            this.sbtnRefresh.Name = "sbtnRefresh";
            this.sbtnRefresh.Size = new System.Drawing.Size(26, 23);
            this.sbtnRefresh.TabIndex = 20;
            this.sbtnRefresh.ToolTip = "Обновить";
            this.sbtnRefresh.Click += new System.EventHandler(this.sbtnRefresh_Click);
            // 
            // sBtnOstatkiExcel
            // 
            this.sBtnOstatkiExcel.Image = ((System.Drawing.Image)(resources.GetObject("sBtnOstatkiExcel.Image")));
            this.sBtnOstatkiExcel.Location = new System.Drawing.Point(802, 11);
            this.sBtnOstatkiExcel.Name = "sBtnOstatkiExcel";
            this.sBtnOstatkiExcel.Size = new System.Drawing.Size(26, 23);
            this.sBtnOstatkiExcel.TabIndex = 5;
            this.sBtnOstatkiExcel.ToolTip = "Экспорт в excel";
            this.sBtnOstatkiExcel.Click += new System.EventHandler(this.sBtnOstatkiExcel_Click);
            // 
            // dateEditOstatki
            // 
            this.dateEditOstatki.EditValue = null;
            this.dateEditOstatki.Location = new System.Drawing.Point(664, 14);
            this.dateEditOstatki.MenuManager = this.ribbon;
            this.dateEditOstatki.Name = "dateEditOstatki";
            this.dateEditOstatki.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditOstatki.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dateEditOstatki.Size = new System.Drawing.Size(100, 20);
            this.dateEditOstatki.TabIndex = 19;
            // 
            // lookUpEditSection
            // 
            this.lookUpEditSection.Location = new System.Drawing.Point(506, 14);
            this.lookUpEditSection.MenuManager = this.ribbon;
            this.lookUpEditSection.Name = "lookUpEditSection";
            this.lookUpEditSection.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEditSection.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("OstatkiSectionName", "Наименование")});
            this.lookUpEditSection.Properties.DataSource = this.bindingSourceSection;
            this.lookUpEditSection.Properties.DisplayMember = "OstatkiSectionName";
            this.lookUpEditSection.Properties.ValueMember = "OstatkiSectionId";
            this.lookUpEditSection.Size = new System.Drawing.Size(152, 20);
            this.lookUpEditSection.TabIndex = 18;
            this.lookUpEditSection.EditValueChanged += new System.EventHandler(this.lookUpEditSection_EditValueChanged);
            // 
            // gridView11
            // 
            this.gridView11.GridControl = this.gridControl4;
            this.gridView11.Name = "gridView11";
            // 
            // gridControl4
            // 
            this.gridControl4.AllowDrop = true;
            this.gridControl4.DataSource = this.bindingSource8;
            this.gridControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl4.Location = new System.Drawing.Point(0, 0);
            this.gridControl4.MainView = this.gridViewUhod;
            this.gridControl4.MenuManager = this.ribbon;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.Size = new System.Drawing.Size(1356, 333);
            this.gridControl4.TabIndex = 3;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewUhod,
            this.gridView13,
            this.gridView11});
            // 
            // gridViewUhod
            // 
            this.gridViewUhod.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8,
            this.gridColumn10,
            this.gridColumn9,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn14,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.colНазначениеПлатежа,
            this.colКомиссия,
            this.colОбщийПриход,
            this.colГруппа21,
            this.colГруппа22,
            this.colPercentDayInc1,
            this.colПланДата,
            this.gridColumnУходПроцент,
            this.colДатаВыполнено,
            this.colДатаСписано,
            this.gridColumn36,
            this.gridColumn37,
            this.gridColumn38,
            this.gridColumn39,
            this.colActionComment1,
            this.colActionPayDate1});
            this.gridViewUhod.GridControl = this.gridControl4;
            this.gridViewUhod.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Сумма", null, "|Сумма: {0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "Дата", null, "|Количество {0:#.##}")});
            this.gridViewUhod.Name = "gridViewUhod";
            this.gridViewUhod.OptionsFind.AlwaysVisible = true;
            this.gridViewUhod.OptionsLayout.StoreVisualOptions = false;
            this.gridViewUhod.OptionsSelection.MultiSelect = true;
            this.gridViewUhod.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridViewUhod.OptionsView.ColumnAutoWidth = false;
            this.gridViewUhod.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewUhod.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView5_CustomDrawCell);
            this.gridViewUhod.SelectionChanged += new DevExpress.Data.SelectionChangedEventHandler(this.gridView1_SelectionChanged);
            this.gridViewUhod.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridViewUhod_RowUpdated);
            this.gridViewUhod.MouseDown += new System.Windows.Forms.MouseEventHandler(this.gridView1_MouseDown);
            // 
            // gridColumn1
            // 
            this.gridColumn1.FieldName = "PPId";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Width = 50;
            // 
            // gridColumn2
            // 
            this.gridColumn2.FieldName = "Номер";
            this.gridColumn2.Name = "gridColumn2";
            // 
            // gridColumn3
            // 
            this.gridColumn3.DisplayFormat.FormatString = "d";
            this.gridColumn3.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn3.FieldName = "Дата";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "Дата", "{0:#.##}")});
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 0;
            // 
            // gridColumn4
            // 
            this.gridColumn4.DisplayFormat.FormatString = "c";
            this.gridColumn4.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn4.FieldName = "Сумма";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Сумма", "Сумма={0:c2}")});
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 1;
            this.gridColumn4.Width = 98;
            // 
            // gridColumn5
            // 
            this.gridColumn5.FieldName = "НДС";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 2;
            this.gridColumn5.Width = 37;
            // 
            // gridColumn6
            // 
            this.gridColumn6.FieldName = "ПлатильщикБанк";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn7
            // 
            this.gridColumn7.FieldName = "ПлатильщикКомпания";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Плательщик Компания";
            this.gridColumn8.FieldName = "ПлатильщикКомпанияИмя";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.OptionsColumn.ReadOnly = true;
            this.gridColumn8.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 3;
            this.gridColumn8.Width = 51;
            // 
            // gridColumn10
            // 
            this.gridColumn10.Caption = "Плательщик Банк";
            this.gridColumn10.FieldName = "ПлатильщикБанкИмя";
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.OptionsColumn.ReadOnly = true;
            this.gridColumn10.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn10.Visible = true;
            this.gridColumn10.VisibleIndex = 4;
            this.gridColumn10.Width = 52;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "Плательщик Принадлежность";
            this.gridColumn9.FieldName = "OwnerName";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.OptionsColumn.ReadOnly = true;
            this.gridColumn9.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 5;
            this.gridColumn9.Width = 33;
            // 
            // gridColumn11
            // 
            this.gridColumn11.FieldName = "ПлательщикСчет";
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn12
            // 
            this.gridColumn12.FieldName = "ПолучательБанк";
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn13
            // 
            this.gridColumn13.Caption = "Получатель Банк";
            this.gridColumn13.FieldName = "ПолучательБанкИмя";
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.OptionsColumn.ReadOnly = true;
            this.gridColumn13.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn13.Visible = true;
            this.gridColumn13.VisibleIndex = 7;
            this.gridColumn13.Width = 60;
            // 
            // gridColumn14
            // 
            this.gridColumn14.FieldName = "ПолучательКомпания";
            this.gridColumn14.Name = "gridColumn14";
            this.gridColumn14.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn15
            // 
            this.gridColumn15.Caption = "Получатель Компания";
            this.gridColumn15.FieldName = "ПолучательКомпанияИмя";
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.OptionsColumn.ReadOnly = true;
            this.gridColumn15.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn15.Visible = true;
            this.gridColumn15.VisibleIndex = 6;
            this.gridColumn15.Width = 52;
            // 
            // gridColumn16
            // 
            this.gridColumn16.Caption = "Получатель Принадлежность";
            this.gridColumn16.FieldName = "OwnerName2";
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.OptionsColumn.ReadOnly = true;
            this.gridColumn16.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn16.Visible = true;
            this.gridColumn16.VisibleIndex = 8;
            this.gridColumn16.Width = 34;
            // 
            // gridColumn17
            // 
            this.gridColumn17.FieldName = "ПолучательСчет";
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Плательщик Группа";
            this.gridColumn18.FieldName = "Группа";
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.ReadOnly = true;
            this.gridColumn18.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 9;
            this.gridColumn18.Width = 56;
            // 
            // gridColumn19
            // 
            this.gridColumn19.FieldName = "Наш";
            this.gridColumn19.Name = "gridColumn19";
            // 
            // gridColumn20
            // 
            this.gridColumn20.FieldName = "Color2";
            this.gridColumn20.Name = "gridColumn20";
            // 
            // colНазначениеПлатежа
            // 
            this.colНазначениеПлатежа.FieldName = "НазначениеПлатежа";
            this.colНазначениеПлатежа.Name = "colНазначениеПлатежа";
            this.colНазначениеПлатежа.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colНазначениеПлатежа.Visible = true;
            this.colНазначениеПлатежа.VisibleIndex = 10;
            this.colНазначениеПлатежа.Width = 79;
            // 
            // colКомиссия
            // 
            this.colКомиссия.Caption = "Комментарий";
            this.colКомиссия.FieldName = "Комиссия";
            this.colКомиссия.Name = "colКомиссия";
            this.colКомиссия.Width = 104;
            // 
            // colОбщийПриход
            // 
            this.colОбщийПриход.Caption = "Общий Уход";
            this.colОбщийПриход.DisplayFormat.FormatString = "c2";
            this.colОбщийПриход.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.colОбщийПриход.FieldName = "ОбщийПриход";
            this.colОбщийПриход.Name = "colОбщийПриход";
            this.colОбщийПриход.Visible = true;
            this.colОбщийПриход.VisibleIndex = 11;
            // 
            // colГруппа21
            // 
            this.colГруппа21.FieldName = "Группа";
            this.colГруппа21.Name = "colГруппа21";
            // 
            // colГруппа22
            // 
            this.colГруппа22.Caption = "Получатель Группа";
            this.colГруппа22.FieldName = "Группа2";
            this.colГруппа22.Name = "colГруппа22";
            this.colГруппа22.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.colГруппа22.Visible = true;
            this.colГруппа22.VisibleIndex = 12;
            // 
            // colPercentDayInc1
            // 
            this.colPercentDayInc1.FieldName = "PercentDayInc";
            this.colPercentDayInc1.Name = "colPercentDayInc1";
            this.colPercentDayInc1.Visible = true;
            this.colPercentDayInc1.VisibleIndex = 13;
            // 
            // colПланДата
            // 
            this.colПланДата.Caption = "План Дата Выдачи";
            this.colПланДата.DisplayFormat.FormatString = "d";
            this.colПланДата.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colПланДата.FieldName = "ПланДата2";
            this.colПланДата.Name = "colПланДата";
            this.colПланДата.Visible = true;
            this.colПланДата.VisibleIndex = 14;
            // 
            // gridColumnУходПроцент
            // 
            this.gridColumnУходПроцент.Caption = "Процент";
            this.gridColumnУходПроцент.FieldName = "Процент";
            this.gridColumnУходПроцент.Name = "gridColumnУходПроцент";
            this.gridColumnУходПроцент.Visible = true;
            this.gridColumnУходПроцент.VisibleIndex = 15;
            // 
            // colДатаВыполнено
            // 
            this.colДатаВыполнено.FieldName = "ДатаВыполнено";
            this.colДатаВыполнено.Name = "colДатаВыполнено";
            this.colДатаВыполнено.Visible = true;
            this.colДатаВыполнено.VisibleIndex = 16;
            // 
            // colДатаСписано
            // 
            this.colДатаСписано.FieldName = "ДатаСписано";
            this.colДатаСписано.Name = "colДатаСписано";
            this.colДатаСписано.Visible = true;
            this.colДатаСписано.VisibleIndex = 17;
            // 
            // gridColumn36
            // 
            this.gridColumn36.Caption = "План Сумма Выдачи";
            this.gridColumn36.DisplayFormat.FormatString = "c2";
            this.gridColumn36.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn36.FieldName = "ПланСумма";
            this.gridColumn36.Name = "gridColumn36";
            this.gridColumn36.OptionsColumn.ReadOnly = true;
            this.gridColumn36.Visible = true;
            this.gridColumn36.VisibleIndex = 18;
            // 
            // gridColumn37
            // 
            this.gridColumn37.Caption = "Долг";
            this.gridColumn37.DisplayFormat.FormatString = "c2";
            this.gridColumn37.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn37.FieldName = "Долг";
            this.gridColumn37.Name = "gridColumn37";
            this.gridColumn37.Visible = true;
            this.gridColumn37.VisibleIndex = 19;
            // 
            // gridColumn38
            // 
            this.gridColumn38.Caption = "Долг Общий";
            this.gridColumn38.DisplayFormat.FormatString = "c2";
            this.gridColumn38.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn38.FieldName = "ТекущийДолг";
            this.gridColumn38.Name = "gridColumn38";
            this.gridColumn38.Visible = true;
            this.gridColumn38.VisibleIndex = 20;
            // 
            // gridColumn39
            // 
            this.gridColumn39.Caption = "Сумма Выдачи";
            this.gridColumn39.DisplayFormat.FormatString = "c2";
            this.gridColumn39.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn39.FieldName = "ActionSum";
            this.gridColumn39.Name = "gridColumn39";
            this.gridColumn39.Visible = true;
            this.gridColumn39.VisibleIndex = 21;
            // 
            // colActionComment1
            // 
            this.colActionComment1.Caption = "Примечание";
            this.colActionComment1.FieldName = "ActionComment";
            this.colActionComment1.Name = "colActionComment1";
            this.colActionComment1.Visible = true;
            this.colActionComment1.VisibleIndex = 22;
            // 
            // colActionPayDate1
            // 
            this.colActionPayDate1.Caption = "Дата Выдачи";
            this.colActionPayDate1.DisplayFormat.FormatString = "d";
            this.colActionPayDate1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.colActionPayDate1.FieldName = "ActionPayDate";
            this.colActionPayDate1.Name = "colActionPayDate1";
            this.colActionPayDate1.Visible = true;
            this.colActionPayDate1.VisibleIndex = 23;
            // 
            // gridView13
            // 
            this.gridView13.GridControl = this.gridControl4;
            this.gridView13.Name = "gridView13";
            // 
            // xtraTabPage11
            // 
            this.xtraTabPage11.Controls.Add(this.sBtn);
            this.xtraTabPage11.Controls.Add(this.sBtnAddDirect);
            this.xtraTabPage11.Controls.Add(this.gridControlDirect);
            this.xtraTabPage11.Controls.Add(this.simpleButton6);
            this.xtraTabPage11.Controls.Add(this.sBtnEditDirect);
            this.xtraTabPage11.Controls.Add(this.simpleButton4);
            this.xtraTabPage11.Name = "xtraTabPage11";
            this.xtraTabPage11.Size = new System.Drawing.Size(1356, 333);
            this.xtraTabPage11.Text = "Прямые проводки";
            // 
            // sBtn
            // 
            this.sBtn.Image = ((System.Drawing.Image)(resources.GetObject("sBtn.Image")));
            this.sBtn.Location = new System.Drawing.Point(549, 3);
            this.sBtn.Name = "sBtn";
            this.sBtn.Size = new System.Drawing.Size(36, 37);
            this.sBtn.TabIndex = 15;
            this.sBtn.ToolTipTitle = "Добавить";
            this.sBtn.Click += new System.EventHandler(this.sBtn_Click);
            // 
            // sBtnAddDirect
            // 
            this.sBtnAddDirect.Image = ((System.Drawing.Image)(resources.GetObject("sBtnAddDirect.Image")));
            this.sBtnAddDirect.Location = new System.Drawing.Point(507, 3);
            this.sBtnAddDirect.Name = "sBtnAddDirect";
            this.sBtnAddDirect.Size = new System.Drawing.Size(36, 37);
            this.sBtnAddDirect.TabIndex = 14;
            this.sBtnAddDirect.ToolTipTitle = "Добавить";
            this.sBtnAddDirect.Click += new System.EventHandler(this.btnAddRowPrihod_Click);
            // 
            // gridControlDirect
            // 
            this.gridControlDirect.AllowDrop = true;
            this.gridControlDirect.DataSource = this.bindingSource17;
            this.gridControlDirect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlDirect.Location = new System.Drawing.Point(0, 0);
            this.gridControlDirect.MainView = this.gridViewDirect;
            this.gridControlDirect.MenuManager = this.ribbon;
            this.gridControlDirect.Name = "gridControlDirect";
            this.gridControlDirect.Size = new System.Drawing.Size(1356, 333);
            this.gridControlDirect.TabIndex = 13;
            this.gridControlDirect.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewDirect,
            this.gridView10,
            this.gridView16});
            // 
            // gridViewDirect
            // 
            this.gridViewDirect.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn41,
            this.gridColumn42,
            this.gridColumn43,
            this.gridColumn46,
            this.gridColumn47,
            this.gridColumn48,
            this.gridColumn51,
            this.gridColumn53,
            this.gridColumn54,
            this.gridColumn56,
            this.gridColumn59});
            this.gridViewDirect.GridControl = this.gridControlDirect;
            this.gridViewDirect.GroupSummary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Сумма", null, "|Сумма: {0:c2}"),
            new DevExpress.XtraGrid.GridGroupSummaryItem(DevExpress.Data.SummaryItemType.Count, "Дата", null, "|Количество {0:#.##}")});
            this.gridViewDirect.Name = "gridViewDirect";
            this.gridViewDirect.OptionsFind.AlwaysVisible = true;
            this.gridViewDirect.OptionsLayout.StoreVisualOptions = false;
            this.gridViewDirect.OptionsSelection.MultiSelect = true;
            this.gridViewDirect.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect;
            this.gridViewDirect.OptionsView.ColumnAutoWidth = false;
            this.gridViewDirect.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.gridViewDirect.CustomDrawCell += new DevExpress.XtraGrid.Views.Base.RowCellCustomDrawEventHandler(this.gridView1_CustomDrawCell);
            // 
            // gridColumn41
            // 
            this.gridColumn41.DisplayFormat.FormatString = "d";
            this.gridColumn41.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.gridColumn41.FieldName = "Дата";
            this.gridColumn41.Name = "gridColumn41";
            this.gridColumn41.OptionsColumn.ReadOnly = true;
            this.gridColumn41.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Count, "Дата", "{0:#.##}")});
            this.gridColumn41.Visible = true;
            this.gridColumn41.VisibleIndex = 0;
            // 
            // gridColumn42
            // 
            this.gridColumn42.DisplayFormat.FormatString = "c";
            this.gridColumn42.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.gridColumn42.FieldName = "Сумма";
            this.gridColumn42.Name = "gridColumn42";
            this.gridColumn42.OptionsColumn.ReadOnly = true;
            this.gridColumn42.Summary.AddRange(new DevExpress.XtraGrid.GridSummaryItem[] {
            new DevExpress.XtraGrid.GridColumnSummaryItem(DevExpress.Data.SummaryItemType.Sum, "Сумма", "Сумма={0:c2}")});
            this.gridColumn42.Visible = true;
            this.gridColumn42.VisibleIndex = 1;
            this.gridColumn42.Width = 98;
            // 
            // gridColumn43
            // 
            this.gridColumn43.FieldName = "НДС";
            this.gridColumn43.Name = "gridColumn43";
            this.gridColumn43.OptionsColumn.ReadOnly = true;
            this.gridColumn43.Visible = true;
            this.gridColumn43.VisibleIndex = 2;
            this.gridColumn43.Width = 37;
            // 
            // gridColumn46
            // 
            this.gridColumn46.Caption = "Плательщик Компания";
            this.gridColumn46.FieldName = "ПлатильщикКомпанияИмя";
            this.gridColumn46.Name = "gridColumn46";
            this.gridColumn46.OptionsColumn.ReadOnly = true;
            this.gridColumn46.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn46.Visible = true;
            this.gridColumn46.VisibleIndex = 3;
            this.gridColumn46.Width = 51;
            // 
            // gridColumn47
            // 
            this.gridColumn47.Caption = "Плательщик Банк";
            this.gridColumn47.FieldName = "ПлатильщикБанкИмя";
            this.gridColumn47.Name = "gridColumn47";
            this.gridColumn47.OptionsColumn.ReadOnly = true;
            this.gridColumn47.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn47.Visible = true;
            this.gridColumn47.VisibleIndex = 4;
            this.gridColumn47.Width = 52;
            // 
            // gridColumn48
            // 
            this.gridColumn48.Caption = "Плательщик Принадлежность";
            this.gridColumn48.FieldName = "OwnerName";
            this.gridColumn48.Name = "gridColumn48";
            this.gridColumn48.OptionsColumn.ReadOnly = true;
            this.gridColumn48.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn48.Visible = true;
            this.gridColumn48.VisibleIndex = 5;
            this.gridColumn48.Width = 33;
            // 
            // gridColumn51
            // 
            this.gridColumn51.Caption = "Получатель Банк";
            this.gridColumn51.FieldName = "ПолучательБанкИмя";
            this.gridColumn51.Name = "gridColumn51";
            this.gridColumn51.OptionsColumn.ReadOnly = true;
            this.gridColumn51.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn51.Visible = true;
            this.gridColumn51.VisibleIndex = 7;
            this.gridColumn51.Width = 60;
            // 
            // gridColumn53
            // 
            this.gridColumn53.Caption = "Получатель Компания";
            this.gridColumn53.FieldName = "ПолучательКомпанияИмя";
            this.gridColumn53.Name = "gridColumn53";
            this.gridColumn53.OptionsColumn.ReadOnly = true;
            this.gridColumn53.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn53.Visible = true;
            this.gridColumn53.VisibleIndex = 6;
            this.gridColumn53.Width = 52;
            // 
            // gridColumn54
            // 
            this.gridColumn54.Caption = "Получатель Принадлежность";
            this.gridColumn54.FieldName = "OwnerName2";
            this.gridColumn54.Name = "gridColumn54";
            this.gridColumn54.OptionsColumn.ReadOnly = true;
            this.gridColumn54.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn54.Visible = true;
            this.gridColumn54.VisibleIndex = 8;
            this.gridColumn54.Width = 34;
            // 
            // gridColumn56
            // 
            this.gridColumn56.Caption = "Плательщик Группа";
            this.gridColumn56.FieldName = "Группа";
            this.gridColumn56.Name = "gridColumn56";
            this.gridColumn56.OptionsColumn.ReadOnly = true;
            this.gridColumn56.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn56.Visible = true;
            this.gridColumn56.VisibleIndex = 9;
            this.gridColumn56.Width = 56;
            // 
            // gridColumn59
            // 
            this.gridColumn59.FieldName = "НазначениеПлатежа";
            this.gridColumn59.Name = "gridColumn59";
            this.gridColumn59.OptionsFilter.FilterPopupMode = DevExpress.XtraGrid.Columns.FilterPopupMode.CheckedList;
            this.gridColumn59.Visible = true;
            this.gridColumn59.VisibleIndex = 10;
            this.gridColumn59.Width = 79;
            // 
            // gridView10
            // 
            this.gridView10.GridControl = this.gridControlDirect;
            this.gridView10.Name = "gridView10";
            // 
            // gridView16
            // 
            this.gridView16.GridControl = this.gridControlDirect;
            this.gridView16.Name = "gridView16";
            // 
            // simpleButton6
            // 
            this.simpleButton6.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton6.Image")));
            this.simpleButton6.Location = new System.Drawing.Point(549, 3);
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Size = new System.Drawing.Size(39, 37);
            this.simpleButton6.TabIndex = 12;
            this.simpleButton6.ToolTipTitle = "Добавить";
            // 
            // sBtnEditDirect
            // 
            this.sBtnEditDirect.Image = ((System.Drawing.Image)(resources.GetObject("sBtnEditDirect.Image")));
            this.sBtnEditDirect.Location = new System.Drawing.Point(594, 3);
            this.sBtnEditDirect.Name = "sBtnEditDirect";
            this.sBtnEditDirect.Size = new System.Drawing.Size(36, 37);
            this.sBtnEditDirect.TabIndex = 11;
            this.sBtnEditDirect.ToolTipTitle = "Добавить";
            this.sBtnEditDirect.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton4.Image")));
            this.simpleButton4.Location = new System.Drawing.Point(507, 3);
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Size = new System.Drawing.Size(36, 37);
            this.simpleButton4.TabIndex = 10;
            this.simpleButton4.ToolTipTitle = "Добавить";
            this.simpleButton4.Click += new System.EventHandler(this.btnAddRowPrihod_Click);
            // 
            // xtraTabPage7
            // 
            this.xtraTabPage7.Controls.Add(this.spinEditOut);
            this.xtraTabPage7.Controls.Add(this.gridControl4);
            this.xtraTabPage7.Name = "xtraTabPage7";
            this.xtraTabPage7.Size = new System.Drawing.Size(1356, 333);
            this.xtraTabPage7.Text = "Уход";
            // 
            // spinEditOut
            // 
            this.spinEditOut.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditOut.Location = new System.Drawing.Point(494, 13);
            this.spinEditOut.MenuManager = this.ribbon;
            this.spinEditOut.Name = "spinEditOut";
            this.spinEditOut.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditOut.Size = new System.Drawing.Size(100, 20);
            this.spinEditOut.TabIndex = 3;
            this.spinEditOut.EditValueChanged += new System.EventHandler(this.spinEditOut_EditValueChanged);
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.spinEditPP);
            this.xtraTabPage1.Controls.Add(this.grid);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1356, 333);
            this.xtraTabPage1.Text = "Приход";
            // 
            // spinEditPP
            // 
            this.spinEditPP.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditPP.Location = new System.Drawing.Point(495, 14);
            this.spinEditPP.MenuManager = this.ribbon;
            this.spinEditPP.Name = "spinEditPP";
            this.spinEditPP.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditPP.Size = new System.Drawing.Size(100, 20);
            this.spinEditPP.TabIndex = 8;
            this.spinEditPP.EditValueChanged += new System.EventHandler(this.spinEdit1_EditValueChanged);
            // 
            // mainTab
            // 
            this.mainTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTab.Location = new System.Drawing.Point(0, 143);
            this.mainTab.Name = "mainTab";
            this.mainTab.SelectedTabPage = this.xtraTabPage1;
            this.mainTab.Size = new System.Drawing.Size(1362, 361);
            this.mainTab.TabIndex = 5;
            this.mainTab.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage7,
            this.xtraTabPage11,
            this.xtraTabPage12,
            this.xtraTabPage2});
            // 
            // bindingSource16
            // 
            this.bindingSource16.DataSource = typeof(TableBI.Data.View_Direct);
            // 
            // barWorkspaceMenuItem1
            // 
            this.barWorkspaceMenuItem1.Caption = "barWorkspaceMenuItem1";
            this.barWorkspaceMenuItem1.Id = 22;
            this.barWorkspaceMenuItem1.Name = "barWorkspaceMenuItem1";
            // 
            // workspaceManager1
            // 
            this.workspaceManager1.TargetControl = this.ribbon;
            this.workspaceManager1.TransitionType = pushTransition1;
            // 
            // ribbonPageGroup5
            // 
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItem6);
            this.ribbonPageGroup5.ItemLinks.Add(this.barButtonItem7);
            this.ribbonPageGroup5.Name = "ribbonPageGroup5";
            this.ribbonPageGroup5.Text = "Настройки";
            // 
            // ribbonPageGroup6
            // 
            this.ribbonPageGroup6.ItemLinks.Add(this.bbiUsers);
            this.ribbonPageGroup6.Name = "ribbonPageGroup6";
            this.ribbonPageGroup6.Text = "Администрирование";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "barSubItem1";
            this.barSubItem1.Id = 23;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barButtonGroup1
            // 
            this.barButtonGroup1.Caption = "barButtonGroup1";
            this.barButtonGroup1.Id = 24;
            this.barButtonGroup1.Name = "barButtonGroup1";
            // 
            // ribbonPageGroup7
            // 
            this.ribbonPageGroup7.ItemLinks.Add(this.beiBegin);
            this.ribbonPageGroup7.ItemLinks.Add(this.beiEnd);
            this.ribbonPageGroup7.Name = "ribbonPageGroup7";
            this.ribbonPageGroup7.Text = "Период";
            // 
            // beiBegin
            // 
            this.beiBegin.Caption = "C  ";
            this.beiBegin.Edit = this.repositoryItemDateEdit1;
            this.beiBegin.Id = 25;
            this.beiBegin.Name = "beiBegin";
            this.beiBegin.Width = 100;
            // 
            // repositoryItemDateEdit1
            // 
            this.repositoryItemDateEdit1.AutoHeight = false;
            this.repositoryItemDateEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit1.Name = "repositoryItemDateEdit1";
            // 
            // beiEnd
            // 
            this.beiEnd.Caption = "По";
            this.beiEnd.Edit = this.repositoryItemDateEdit2;
            this.beiEnd.Id = 26;
            this.beiEnd.Name = "beiEnd";
            this.beiEnd.Width = 100;
            // 
            // repositoryItemDateEdit2
            // 
            this.repositoryItemDateEdit2.AutoHeight = false;
            this.repositoryItemDateEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemDateEdit2.Name = "repositoryItemDateEdit2";
            // 
            // MainPanel
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1362, 535);
            this.Controls.Add(this.mainTab);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Controls.Add(this.barDockControl3);
            this.Controls.Add(this.barDockControl4);
            this.Controls.Add(this.barDockControl2);
            this.Controls.Add(this.barDockControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "MainPanel";
            this.Ribbon = this.ribbon;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "TableBI";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainPanel_FormClosing);
            this.Load += new System.EventHandler(this.MainPanel_Load);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.MainPanel_DragDrop);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainPanel_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOstatki)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOstatki)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemSpinEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceOstatkiPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceSection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource13)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditCompany)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEditOwner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckedComboBoxEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPercent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit3)).EndInit();
            this.xtraTabPage10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPercentUhod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit3)).EndInit();
            this.xtraTabPage8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            this.xtraTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            this.xtraTabPage13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit5)).EndInit();
            this.xtraTabPage14.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlOstatkiSectionDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewOstatkiSectionDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemColorEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemLookUpEdit8)).EndInit();
            this.xtraTabPage12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOstatki.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateEditOstatki.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEditSection.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUhod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).EndInit();
            this.xtraTabPage11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlDirect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewDirect)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView16)).EndInit();
            this.xtraTabPage7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spinEditOut.Properties)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spinEditPP.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mainTab)).EndInit();
            this.mainTab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemDateEdit2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.BindingSource bindingSource2;
        private System.Windows.Forms.BindingSource bindingSource3;
        private System.Windows.Forms.BindingSource bindingSource4;
        private System.Windows.Forms.BindingSource bindingSource5;
        private System.Windows.Forms.BindingSource bindingSource6;
        private System.Windows.Forms.BindingSource bindingSource7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private System.Windows.Forms.BindingSource bindingSource8;
        private System.Windows.Forms.BindingSource bindingSource9;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarDockingMenuItem barDockingMenuItem1;
        private DevExpress.XtraBars.BarButtonItem bbiUsers;
        private DevExpress.XtraBars.BarButtonItem bbiConnect;
        private DevExpress.XtraBars.Ribbon.RibbonMiniToolbar ribbonMiniToolbar1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private System.Windows.Forms.BindingSource bindingSource10;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemSpinEdit repositoryItemSpinEdit1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup4;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.BindingSource bindingSource11;
        private DevExpress.XtraBars.BarStaticItem barStaticItem1;
        private DevExpress.XtraBars.BarStaticItem barStaticItemSum;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit1;
        private DevExpress.XtraBars.BarEditItem barEditItem3;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit2;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControl3;
        private DevExpress.XtraBars.BarDockControl barDockControl4;
        private DevExpress.XtraBars.BarDockControl barDockControl2;
        private DevExpress.XtraBars.BarDockControl barDockControl1;
        private DevExpress.XtraBars.BarManager barManager2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timerUpdateAll;
        private DevExpress.XtraBars.BarButtonItem barButtonItem10;
        private DevExpress.XtraBars.BarButtonItem barButtonItem11;
        private System.Windows.Forms.BindingSource bindingSource12;
        private System.Windows.Forms.BindingSource bindingSourceOstatkiPeriod;
        private System.Windows.Forms.BindingSource bindingSource13;
        private System.Windows.Forms.BindingSource bindingSource14;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.BindingSource bindingSource15;
        private System.Windows.Forms.BindingSource bindingSourceSection;
        private DevExpress.XtraTab.XtraTabControl mainTab;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraEditors.SpinEdit spinEditPP;
        private DevExpress.XtraGrid.GridControl grid;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colPPId;
        private DevExpress.XtraGrid.Columns.GridColumn colНомер;
        private DevExpress.XtraGrid.Columns.GridColumn colДата;
        private DevExpress.XtraGrid.Columns.GridColumn colСумма;
        private DevExpress.XtraGrid.Columns.GridColumn colНДС;
        private DevExpress.XtraGrid.Columns.GridColumn colПлатильщикБанк;
        private DevExpress.XtraGrid.Columns.GridColumn colПлатильщикКомпания;
        private DevExpress.XtraGrid.Columns.GridColumn colПлатильщикКомпанияИмя;
        private DevExpress.XtraGrid.Columns.GridColumn colПлатильщикБанкИмя;
        private DevExpress.XtraGrid.Columns.GridColumn colГруппа;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerName1;
        private DevExpress.XtraGrid.Columns.GridColumn colПлательщикСчет;
        private DevExpress.XtraGrid.Columns.GridColumn colПолучательБанк;
        private DevExpress.XtraGrid.Columns.GridColumn colПолучательКомпанияИмя;
        private DevExpress.XtraGrid.Columns.GridColumn colПолучательБанкИмя;
        private DevExpress.XtraGrid.Columns.GridColumn colПолучательКомпания;
        private DevExpress.XtraGrid.Columns.GridColumn colГруппа2;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerName2;
        private DevExpress.XtraGrid.Columns.GridColumn colПолучательСчет;
        private DevExpress.XtraGrid.Columns.GridColumn colНазначениеПлатежа1;
        private DevExpress.XtraGrid.Columns.GridColumn colНаш;
        private DevExpress.XtraGrid.Columns.GridColumn colColor21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPlanDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPayDate;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPrecent;
        private DevExpress.XtraGrid.Columns.GridColumn colActionSum;
        private DevExpress.XtraGrid.Columns.GridColumn gridPlanSum;
        private DevExpress.XtraGrid.Columns.GridColumn colActionDebt;
        private DevExpress.XtraGrid.Columns.GridColumn colActionComment;
        private DevExpress.XtraGrid.Columns.GridColumn colPercentValue1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionCurDebt;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerId2;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerColor2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView12;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage7;
        private DevExpress.XtraEditors.SpinEdit spinEditOut;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewUhod;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn colНазначениеПлатежа;
        private DevExpress.XtraGrid.Columns.GridColumn colКомиссия;
        private DevExpress.XtraGrid.Columns.GridColumn colОбщийПриход;
        private DevExpress.XtraGrid.Columns.GridColumn colГруппа21;
        private DevExpress.XtraGrid.Columns.GridColumn colГруппа22;
        private DevExpress.XtraGrid.Columns.GridColumn colPercentDayInc1;
        private DevExpress.XtraGrid.Columns.GridColumn colПланДата;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnУходПроцент;
        private DevExpress.XtraGrid.Columns.GridColumn colДатаВыполнено;
        private DevExpress.XtraGrid.Columns.GridColumn colДатаСписано;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn36;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn37;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn38;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn39;
        private DevExpress.XtraGrid.Columns.GridColumn colActionComment1;
        private DevExpress.XtraGrid.Columns.GridColumn colActionPayDate1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage11;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage12;
        private DevExpress.XtraEditors.DateEdit dateEditOstatki;
        private DevExpress.XtraEditors.LookUpEdit lookUpEditSection;
        private DevExpress.XtraGrid.GridControl gridControlOstatki;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView15;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewOstatki;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewCompany;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyFizLizo1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyINN1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyOstatok1;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupId3;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEditCompany;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerId3;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEditOwner;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeCompanyId3;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn colOstatokPeriodId1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyFizLizo;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyINN;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupId2;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerId;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeCompanyId2;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyOstatok;
        private DevExpress.XtraGrid.Columns.GridColumn colOstatokPeriodId;
        private DevExpress.XtraGrid.Columns.GridColumn colOwner1;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeCompany;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage8;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeCompanyId;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeCompanyName;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Columns.GridColumn colBankName;
        private DevExpress.XtraGrid.Columns.GridColumn colBankAbr;
        private DevExpress.XtraGrid.Columns.GridColumn colBankBik;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraGrid.GridControl gridControl3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupId;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupName;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage13;
        private DevExpress.XtraGrid.GridControl gridControlAccount;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewAccount;
        private DevExpress.XtraGrid.Columns.GridColumn colAccount1;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyId;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit5;
        private DevExpress.XtraGrid.Columns.GridColumn colBankId;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit4;
        private DevExpress.XtraGrid.Columns.GridColumn colAccountActive;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage9;
        private DevExpress.XtraGrid.GridControl gridPercent;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPercent;
        private DevExpress.XtraGrid.Columns.GridColumn colPercentId;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerId1;
        private DevExpress.XtraGrid.Columns.GridColumn colPrecentDate;
        private DevExpress.XtraGrid.Columns.GridColumn colPercentDateEnd;
        private DevExpress.XtraGrid.Columns.GridColumn colPercentValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPercentDayInc;
        private DevExpress.XtraGrid.Columns.GridColumn colOwner;
        private DevExpress.XtraGrid.Columns.GridColumn colPercentNdsValue;
        private DevExpress.XtraGrid.Columns.GridColumn colPercentNdsDayInc;
        private DevExpress.XtraGrid.Columns.GridColumn colTypeCompanyId1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit6;
        private DevExpress.XtraGrid.Columns.GridColumn colPercentPrihod;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage10;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPercentUhod;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn24;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn26;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn27;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn28;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn29;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn30;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn31;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn32;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn33;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn34;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerName;
        private DevExpress.XtraGrid.Columns.GridColumn colColor;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerPlusDate;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupId1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage14;
        private DevExpress.XtraGrid.GridControl gridControlSection;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSection;
        private DevExpress.XtraGrid.Columns.GridColumn colOstatkiSectionName;
        private DevExpress.XtraGrid.Columns.GridColumn colGroupId4;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit9;
        private DevExpress.XtraGrid.Columns.GridColumn colOwnerId4;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit10;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit8;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView13;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView14;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn76;
        private DevExpress.XtraGrid.Columns.GridColumn colOstatokSelected;
        private DevExpress.XtraGrid.Columns.GridColumn colCompanyName2;
        private DevExpress.XtraGrid.Columns.GridColumn colBankName1;
        private DevExpress.XtraGrid.Columns.GridColumn colКонечныйОстаток;
        private DevExpress.XtraGrid.Columns.GridColumn colOstatokProcess;
        private DevExpress.XtraGrid.Columns.GridColumn colOstatokFrom;
        private DevExpress.XtraGrid.Columns.GridColumn colOstatokTakeoff;
        private DevExpress.XtraGrid.Columns.GridColumn colOstatokPrimichanie;
        private DevExpress.XtraGrid.Columns.GridColumn colAccountBlock;
        private DevExpress.XtraGrid.Columns.GridColumn colOstatokProcessSpisanie1;
        private DevExpress.XtraGrid.Columns.GridColumn colOstatokTo1;
        private DevExpress.XtraGrid.Columns.GridColumn colOstatokProcessSpisanie2;
        private DevExpress.XtraGrid.Columns.GridColumn colOstatokTo2;
        private DevExpress.XtraGrid.Columns.GridColumn colOstatokProcessSpisanie3;
        private DevExpress.XtraGrid.Columns.GridColumn colOstatokTo3;
        private DevExpress.XtraGrid.Columns.GridColumn colOstatokProcessSpisanie4;
        private DevExpress.XtraGrid.Columns.GridColumn colOstatokTo4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn75;
        private DevExpress.XtraEditors.SimpleButton sBtnOstatkiExcel;
        private System.Windows.Forms.BindingSource bindingSource16;
        private System.Windows.Forms.BindingSource bindingSource17;
        private DevExpress.XtraGrid.GridControl gridControlDirect;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewDirect;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn41;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn43;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn46;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn47;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn48;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn51;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn53;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn54;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn56;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn59;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView16;
        private DevExpress.XtraEditors.SimpleButton simpleButton6;
        private DevExpress.XtraEditors.SimpleButton sBtnEditDirect;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
        private DevExpress.XtraEditors.SimpleButton sBtn;
        private DevExpress.XtraEditors.SimpleButton sBtnAddDirect;
        private DevExpress.XtraGrid.GridControl gridControlOstatkiSectionDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewOstatkiSectionDetail;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnTypeCompany;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnOwner;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumnGroup;
        private DevExpress.XtraEditors.Repository.RepositoryItemColorEdit repositoryItemColorEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit13;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit11;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit12;
        private System.Windows.Forms.BindingSource bindingSource18;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit14;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit15;
        private DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit repositoryItemLookUpEdit16;
        private DevExpress.XtraEditors.SimpleButton sbtnRefresh;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit3;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit4;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit5;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckedComboBoxEdit repositoryItemCheckedComboBoxEdit1;
        private DevExpress.XtraBars.BarWorkspaceMenuItem barWorkspaceMenuItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarButtonGroup barButtonGroup1;
        private DevExpress.XtraBars.BarEditItem beiBegin;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit1;
        private DevExpress.XtraBars.BarEditItem beiEnd;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit repositoryItemDateEdit2;
        private DevExpress.XtraBars.Ribbon.RibbonMiniToolbar ribbonMiniToolbar2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup7;
        private DevExpress.Utils.WorkspaceManager workspaceManager1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup5;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup6;
    }
}