﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableBI.Data
{
    public class PP
    {
        public int Номер { get; set; }
        public DateTime Дата { get; set; }
        public double Сумма { get; set; }
        public string ПлательщикСчет { get; set; }
        
        public string Плательщик1 { get; set; }

        public string ПлательщикИНН { get; set; }
        public string ПлательщикБанк1        { get; set; }

        public string ПлательщикБИК { get; set; }
        
        public string ПолучательБИК { get; set; }

        public string ПолучательСчет { get; set; }

        public DateTime ДатаПоступило { get; set; }

        public string Получатель1 { get; set; }

        public string ПолучательИНН { get; set; }        

        public string ПолучательРасчСчет { get; set; }
        public string ПолучательБанк1 { get; set; }

        public string НазначениеПлатежа { get; set; }
        public DateTime ДатаСписано { get; set; }        
        public DateTime КвитанцияДата { get; set; }
        public  string Type { get; set; }

    }
}
