﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableBI.Data
{
    public partial class UserToOwnerMy
    {
        public int OwnerId { get; set; }
        public string OwnerName { get; set; }
        public Nullable<int> Color { get; set; }
        public Nullable<int> UserToOwnerId { get; set; }
        public Nullable<int> UserId { get; set; }
        public Nullable<int> OwnerId1 { get; set; }    

        public Nullable<int> Check { get; set; }
        
    }
    
}
