﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TableBI.BL
{

    public static class Config
    {
        public static string ConnectionString { get { return String.Format("Server={0};User={1};Password={2};Database={3};Trusted_Connection=true", Server, User, Password, Database); } }
        public static string Server { get; set; }
        public static string User { get; set; }
        public static string Password { get; set; }
        public static string Database { get; set; }
        public static string Path { get; set; }
        public static Double Val { get; set; }
        public static Double Out { get; set; }


        static Config()
        {
            try
            {
                if (!System.IO.Directory.Exists(Application.StartupPath + @"\sys\"))
                {
                    System.IO.Directory.CreateDirectory(Application.StartupPath + @"\sys\");
                    List<string> lines = new List<string>();
                    lines.Add("Server=");
                    lines.Add("User=");
                    lines.Add("Password=");
                    lines.Add("Database=");
                    lines.Add("val=");
                    lines.Add("Out=");
                    lines.Add("Path=");
                    File.WriteAllLines(Application.StartupPath + @"\sys\config.cfg", lines);
                }
            }
            catch(Exception ex)
            {
                Log.AddToLog(ex.Message);
            }

            Read();

        }

        static void Read()
        {
            try
            {
                string[] lines = System.IO.File.ReadAllLines(Application.StartupPath + @"\sys\config.cfg");
                foreach (string line in lines)
                {
                    string[] columns = line.Split('=');

                    switch (columns[0])
                    {
                        case "Server":
                            Server = columns[1];
                            break;
                        case "User":
                            User = columns[1];
                            break;
                        case "Password":
                            Password = columns[1];
                            break;
                        case "Database":
                            Database = columns[1];
                            break;
                        case "val":
                            Val = Convert.ToDouble(columns[1]);
                            break;
                        case "Out":
                            Out = Convert.ToDouble(columns[1]);
                            break;
                        case "Path":
                            Path = columns[1];
                            break;
                    }
                }

                if (string.IsNullOrEmpty(Database))
                    Database = "TableBI";
            }
            catch(Exception ex)
            {
                Log.AddToLog(ex.Message);
            }

        }
        public static void Save()
        {
            try
            {
                List<string> lines = new List<string>();
                lines.Add(String.Format("Server={0}", Server));
                lines.Add(String.Format("User={0}", User));
                lines.Add(String.Format("Password={0}", Password));
                lines.Add(String.Format("Database={0}", Database));
                lines.Add(String.Format("val={0}", Val));
                lines.Add(String.Format("Out={0}", Out));
                lines.Add(String.Format("Path={0}", Path));
                File.WriteAllLines(Application.StartupPath + @"\sys\config.cfg", lines);
            }
            catch(Exception ex)
            {
                Log.AddToLog(ex.Message);
            }

        }
    }
}
