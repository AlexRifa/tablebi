﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableBI.BL;

namespace TableBI.Data
{
   public class CompanyService
    {
        static TableBIEntities2 context;

        private static CompanyService instance;

        public CompanyService()
        {            
            context = new TableBIEntities2();
        }
        public static CompanyService GetInstance()
        {
            
            if (instance == null)
            {
                instance = new CompanyService();
            }

            return instance;
        }
        public IQueryable<Company> GetSinonim(string sinonim)
        {
            IQueryable<Company> rez;
            
            rez = context.Company.Where(c => sinonim.IndexOf(c.CompanyName) >-1);

            return rez;
        }

        public void UpdatePP(List<int> oldId, int newId)
        {
            if (oldId.Count == 0) return;
            string ids = string.Join(",", oldId.Select(x => x.ToString()).ToArray());

            string sql = String.Format(@"UPDATE Account SET [CompanyId]={0} WHERE CompanyId in ({1})", newId, ids) ;
            context.Database.ExecuteSqlCommand(sql);
        }
    }
}
