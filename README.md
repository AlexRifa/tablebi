# TableBI 

[![Build status](https://ci.appveyor.com/api/projects/status/w6s1is1741u0fj34/branch/master?svg=true)](https://ci.appveyor.com/project/AndrewKitu/tablebi/branch/master)


### Disclaimer from DevExpress
*Please note that according to DevExpress [EULA](https://www.devexpress.com/Support/EULAs/NetComponents.xml), every person working with DevExpress components should have a separate license. To properly register our components on your machine, use the DevExpress installer as described in the [How to activate my DevExpress license article](https://www.devexpress.com/Support/Center/Question/Details/KA18604). Working with DevExpress components using libraries got from NuGet without proper registration may result in licensing violation*.

---

The DevExpress components and trademark are Copyright (C) 2000-2017 Developer Express Inc. and their end-user license agreement is available at [https://www.devexpress.com/Support/EULAs/NetComponents.xml](https://www.devexpress.com/Support/EULAs/NetComponents.xml).
