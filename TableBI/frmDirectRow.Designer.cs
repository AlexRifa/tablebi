﻿namespace TableBI
{
    partial class frmDirectRow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Дата = new DevExpress.XtraEditors.DateEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.Сумма = new DevExpress.XtraEditors.SpinEdit();
            this.Плательщик = new DevExpress.XtraEditors.LookUpEdit();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.ПлательщикБанк = new DevExpress.XtraEditors.LookUpEdit();
            this.bindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.Получатель = new DevExpress.XtraEditors.LookUpEdit();
            this.bindingSource3 = new System.Windows.Forms.BindingSource(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.ПолучательБанк = new DevExpress.XtraEditors.LookUpEdit();
            this.НазначениеПлатежа = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.Дата.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Дата.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Сумма.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Плательщик.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ПлательщикБанк.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Получатель.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ПолучательБанк.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // Дата
            // 
            this.Дата.EditValue = null;
            this.Дата.Location = new System.Drawing.Point(120, 12);
            this.Дата.Name = "Дата";
            this.Дата.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Дата.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Дата.Size = new System.Drawing.Size(282, 20);
            this.Дата.TabIndex = 1;
            this.Дата.EditValueChanged += new System.EventHandler(this.dateEdit1_EditValueChanged);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 15);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(26, 13);
            this.labelControl1.TabIndex = 2;
            this.labelControl1.Text = "Дата";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 41);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(31, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Сумма";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(12, 71);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(64, 13);
            this.labelControl3.TabIndex = 4;
            this.labelControl3.Text = "Плательщик";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(12, 93);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(88, 13);
            this.labelControl4.TabIndex = 5;
            this.labelControl4.Text = "ПлательщикБанк";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(12, 119);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(61, 13);
            this.labelControl5.TabIndex = 6;
            this.labelControl5.Text = "Получатель";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(12, 145);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(85, 13);
            this.labelControl6.TabIndex = 7;
            this.labelControl6.Text = "ПолучательБанк";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(12, 172);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(60, 13);
            this.labelControl7.TabIndex = 8;
            this.labelControl7.Text = "Назначение";
            // 
            // Сумма
            // 
            this.Сумма.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.Сумма.Location = new System.Drawing.Point(120, 38);
            this.Сумма.Name = "Сумма";
            this.Сумма.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Сумма.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Сумма.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.Сумма.Size = new System.Drawing.Size(282, 20);
            this.Сумма.TabIndex = 10;
            // 
            // Плательщик
            // 
            this.Плательщик.Location = new System.Drawing.Point(120, 64);
            this.Плательщик.Name = "Плательщик";
            this.Плательщик.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Плательщик.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CompanyName", "Компания")});
            this.Плательщик.Properties.DataSource = this.bindingSource1;
            this.Плательщик.Properties.DisplayMember = "CompanyName";
            this.Плательщик.Properties.DropDownRows = 20;
            this.Плательщик.Properties.ValueMember = "CompanyId";
            this.Плательщик.Size = new System.Drawing.Size(282, 20);
            this.Плательщик.TabIndex = 11;
            this.Плательщик.EditValueChanged += new System.EventHandler(this.lookUpEdit1_EditValueChanged);
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = typeof(TableBI.Data.Company);
            // 
            // ПлательщикБанк
            // 
            this.ПлательщикБанк.Location = new System.Drawing.Point(120, 90);
            this.ПлательщикБанк.Name = "ПлательщикБанк";
            this.ПлательщикБанк.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ПлательщикБанк.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BankName", "Банк")});
            this.ПлательщикБанк.Properties.DataSource = this.bindingSource2;
            this.ПлательщикБанк.Properties.DisplayMember = "BankName";
            this.ПлательщикБанк.Properties.ValueMember = "BankId";
            this.ПлательщикБанк.Size = new System.Drawing.Size(282, 20);
            this.ПлательщикБанк.TabIndex = 12;
            // 
            // bindingSource2
            // 
            this.bindingSource2.DataSource = typeof(TableBI.Data.Bank);
            // 
            // Получатель
            // 
            this.Получатель.Location = new System.Drawing.Point(120, 116);
            this.Получатель.Name = "Получатель";
            this.Получатель.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Получатель.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("CompanyName", "Компания")});
            this.Получатель.Properties.DataSource = this.bindingSource3;
            this.Получатель.Properties.DisplayMember = "CompanyName";
            this.Получатель.Properties.ValueMember = "CompanyId";
            this.Получатель.Size = new System.Drawing.Size(282, 20);
            this.Получатель.TabIndex = 13;
            // 
            // bindingSource3
            // 
            this.bindingSource3.DataSource = typeof(TableBI.Data.Company);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(120, 255);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "Ok";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(201, 255);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 18;
            this.button2.Text = "Отмена";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ПолучательБанк
            // 
            this.ПолучательБанк.Location = new System.Drawing.Point(120, 142);
            this.ПолучательБанк.Name = "ПолучательБанк";
            this.ПолучательБанк.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ПолучательБанк.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] {
            new DevExpress.XtraEditors.Controls.LookUpColumnInfo("BankName", "Банк")});
            this.ПолучательБанк.Properties.DataSource = this.bindingSource2;
            this.ПолучательБанк.Properties.DisplayMember = "BankName";
            this.ПолучательБанк.Properties.ValueMember = "BankId";
            this.ПолучательБанк.Size = new System.Drawing.Size(282, 20);
            this.ПолучательБанк.TabIndex = 19;
            // 
            // НазначениеПлатежа
            // 
            this.НазначениеПлатежа.Location = new System.Drawing.Point(120, 169);
            this.НазначениеПлатежа.Name = "НазначениеПлатежа";
            this.НазначениеПлатежа.Size = new System.Drawing.Size(282, 68);
            this.НазначениеПлатежа.TabIndex = 20;
            this.НазначениеПлатежа.Text = "";
            // 
            // frmDirectRow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 290);
            this.Controls.Add(this.НазначениеПлатежа);
            this.Controls.Add(this.ПолучательБанк);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Получатель);
            this.Controls.Add(this.ПлательщикБанк);
            this.Controls.Add(this.Плательщик);
            this.Controls.Add(this.Сумма);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.Дата);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmDirectRow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.AddRow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Дата.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Дата.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Сумма.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Плательщик.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ПлательщикБанк.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Получатель.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ПолучательБанк.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private DevExpress.XtraEditors.DateEdit Дата;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.SpinEdit Сумма;
        private DevExpress.XtraEditors.LookUpEdit Плательщик;
        private System.Windows.Forms.BindingSource bindingSource1;
        private DevExpress.XtraEditors.LookUpEdit ПлательщикБанк;
        private System.Windows.Forms.BindingSource bindingSource2;
        private DevExpress.XtraEditors.LookUpEdit Получатель;
        private System.Windows.Forms.BindingSource bindingSource3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private DevExpress.XtraEditors.LookUpEdit ПолучательБанк;
        private System.Windows.Forms.RichTextBox НазначениеПлатежа;
    }
}