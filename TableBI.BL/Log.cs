﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TableBI.BL
{
   public static class Log
    {
        public static void AddToLog(string msg)
        {
            try
            {
                if (!System.IO.Directory.Exists(Application.StartupPath + @"\Logs\"))
                    System.IO.Directory.CreateDirectory(Application.StartupPath + @"\Logs\");
                System.IO.FileStream fs = new System.IO.FileStream(Application.StartupPath + @"\Logs\" + DateTime.Now.ToString("yyyy-MM-dd") + ".log", System.IO.FileMode.Append);
                System.IO.StreamWriter sw = new System.IO.StreamWriter(fs);
                sw.WriteLine("[" + System.DateTime.Now.ToString("HH:mm:ss") + "] " + msg);
                sw.Close();
                fs.Close();

                
                
            }
            catch
            {
                //MessageBox.Show(resources.GetString("ErrorToLog"), resources.GetString("Warning"), MessageBoxButtons.OK);
            }
        }
    }
}
